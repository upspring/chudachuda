package com.sudasuda.app.service;

/**
 * Created by ponvandu on 05/01/22.
 */

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import org.springframework.util.ResourceUtils;


@WebServlet("/oauth2callback")
public class GoogleLogin extends HttpServlet
{



    private static final long serialVersionUID = 1L;
    static String className = "com.upspring.highlight.service.google";
    InputStream input = null;
    Properties prop = new Properties();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            System.out.println("inside of oauth2 call back.......");

            String code = request.getParameter("code");

            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            prop.load(in);
            System.out.println("api key"+prop.getProperty("client_id"));

            String url = "https://accounts.google.com/o/oauth2/token";
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

            //  chudachuda credentials
            nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
            nameValuePairs.add(new BasicNameValuePair("client_id",prop.getProperty("client_id")));
            nameValuePairs.add(new BasicNameValuePair("client_secret", prop.getProperty("client_secret")));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", prop.getProperty("redirect_uri")));
            nameValuePairs.add(new BasicNameValuePair("code", code));

            //Local Credentials
         /*   nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
            nameValuePairs.add(new BasicNameValuePair("client_id","281124818594-hmeukt2p7p4hlno58n75nddrqq0ll8no.apps.googleusercontent.com"));
            nameValuePairs.add(new BasicNameValuePair("client_secret", "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"));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", "https://www.chudachuda.com/oauth2callback"));
            nameValuePairs.add(new BasicNameValuePair("code", code));*/


            HttpPost post = new HttpPost(url);
            HttpClient client = new DefaultHttpClient();

            // add header
            post.setHeader("Host", "accounts.google.com");
            post.setHeader("Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            post.setHeader("Accept-Language", "en-US,en;q=0.5");
            post.setHeader("Connection", "keep-alive");
            post.setHeader("Referer", "https://accounts.google.com/o/oauth2/token");
            post.setHeader("Content-Type", "application/x-www-form-urlencoded");

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response2 = client.execute(post);

            int responseCode = response2.getStatusLine().getStatusCode();

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response2.getEntity().getContent()));

            String result="";
            String line = "";
            while ((line = rd.readLine()) != null) {
                result+=line;
            }


            System.out.println("Input Reader--->"+result);
            //get Access Token
            JsonObject json = (JsonObject)new JsonParser().parse(result);
            String access_token = json.get("access_token").getAsString();
            System.out.println("Access Token-->"+access_token);


            GoogleCredential cr=new GoogleCredential().setAccessToken(access_token);

            System.out.println("cr"+cr);
            Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), cr).setApplicationName(
                    "Oauth2").build();
            System.out.println("userInfo<<------>>"+oauth2);
            Userinfoplus userinfo = oauth2.userinfo().get().execute();
            System.out.println("userInfo<<------>>"+userinfo);
            userinfo.toPrettyString();
            System.out.println("userInfo<<------>>"+userinfo.getEmail());
            String email=userinfo.getEmail();
            String user=userinfo.getGivenName();
            String picture=userinfo.getPicture();

            System.out.println("working......"+user);
            String userName= URLEncoder.encode(user,"UTF8");

            System.out.println("username<<------>>"+userName);
            System.out.println("userInfo<<------>>"+userinfo.getEmail());

            System.out.println("userInfo Encoded Username<<------>>"+userName);
            String encodeEmail= new String(java.util.Base64.getEncoder().encode(email.getBytes()));

            System.out.println("encodeEmail"+encodeEmail);


            //Update Tp lightuptheweb

            URL userUrl = new URL(
                    ""+prop.getProperty("url")+"/SocialLogin?email=" + email+"&userName="+userName+"&network=GL&picture="+picture
            );
          /*  URL userUrl = new URL(
                    "http://localhost:8080/SocialLogin?email=" + email+"&userName="+userName+"&network=GL&picture="+picture
            );*/

            URLConnection urlConn = userUrl.openConnection();
            String userDeatil="";
            rd = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                userDeatil += line;
            }

            System.out.println("Login user details--->"+userDeatil);
            //Checking User Already Exist
           /* JSONArray tt = new JSONArray(userDeatil);
            System.out.println("json length is-->"+tt.length());
            if(tt.length()>0) {

                for (int i = 0; i < tt.length(); i++) {

                    String color = tt.getJSONObject(i).getString("color");
                    String username = tt.getJSONObject(i).getString("username");
                    System.out.println("color-->"+color);

                    Cookie ck2=new Cookie("COLOR",color);//creating cookie object
                    ck2.setMaxAge(60*60*24*365);
                    response.addCookie(ck2);
                    Cookie ck=new Cookie("GL",encodeEmail);//creating cookie object
                    ck.setMaxAge(60*60*24*365);
                    response.addCookie(ck);//adding cookie in the response


                }
                response.sendRedirect("http://localhost:8080");
            }
            else
            {
                System.out.println("User connected with some other login like twitter and lutw");
                response.sendRedirect("http://localhost:8080/LoginAlert");
            }*/


            if(userDeatil.equals("signin"))
            {
                System.out.println("Email Already Exist.....");
                Cookie[] cookies = request.getCookies();
                System.out.println("cookid"+cookies);
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("cc") ) {
                            // emailId = cookie.getValue();
                            System.out.println("cookie value"+cookie.getValue());
                            cookie.setValue("");
                            cookie.setPath("/");
                            cookie.setMaxAge(0);
                            response.addCookie(cookie);
                        }
                    }
                }
                request.setAttribute("error", " email is already taken");
                response.sendRedirect(""+prop.getProperty("url")+"/error");
            }
            else
            {
                Cookie ck=new Cookie("cc",encodeEmail);//creating cookie object
                ck.setMaxAge(60*60*24*365);
                //   ck.setDomain("https://lightuptheweb.net");
                response.addCookie(ck);//adding cookie in the response

                response.sendRedirect(prop.getProperty("url"));
            }


            rd.close();


        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (ProtocolException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {


        System.out.println("do post");
    }



}
