package com.sudasuda.app.service;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.sudasuda.app.dao.CacheDAO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by venkksastry on 21/10/16.
 */
@Service
public class HazelcastCacheService {


    CacheDAO cacheDAO = new CacheDAO();
    private static final Map<String, Object> cache = new HashMap<String, Object>();

    public final static HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

    public static HazelcastCacheService INSTANCE = new HazelcastCacheService();

    private HazelcastCacheService() { }

    public static HazelcastCacheService getInstance() {
        System.out.println("INSTANCE"+INSTANCE);
        return INSTANCE;
    }

    public void put(String key, Object value)
    {


        IMap<String, Object> cache = hazelcastInstance.getMap("cache");
        System.out.println("hazlecast"+value);
        System.out.println("hazlecast"+key);
        if ( value == null )
        {
            cache.remove(key);
        }
        else
        {
            cache.put(key, value);
        }
    }

    public Object get(String key)
    {
        IMap<String, Object> cache = hazelcastInstance.getMap("cache");
        return cache.get(key);
    }

    public void remove(String key)
    {

    }


}
