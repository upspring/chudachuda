package com.sudasuda.app.service;




/**
 * Created by sri on 18/9/15.
 */

public class RssFeedService {

   /*  private static final Logger logger = LoggerFactory
           .getLogger(RssFeedService.class);


    LinkDAO linkDAO = new LinkDAO();
    String imageURL = null;

    public synchronized void addRssFeed() throws Exception {

        logger.info("Rss Feeds Starts");


        HazelcastCacheService.getInstance().put("newStories", null);
        HazelcastCacheService.getInstance().put("xmlFeed", null);

        List<String> rssFeedList = new ArrayList<String>();
       // rssFeedList.add("http://www.dinakaran.com/rss_news.asp?id=8");
        rssFeedList.add("http://www.dinamani.com/%E0%AE%A4%E0%AE%B1%E0%AF%8D%E0%AE%AA%E0%AF%8B%E0%AE%A4%E0%AF%88%E0%AE%AF-%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/%E0%AE%AE%E0%AF%81%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AE%BF%E0%AE%AF%E0%AE%9A%E0%AF%8D-%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/rssfeed/?id=479&getXmlFeed=true");
        rssFeedList.add("http://www.dailythanthi.com/RSS/News/TopNews");
        rssFeedList.add("http://feeds.feedburner.com/dinamalar/Front_page_news?format=xml");
            // rssFeedList.add("http://www.thehindu.com/?service=rss");
       rssFeedList.add("http://tamil.thehindu.com/india/?service=rss");
       rssFeedList.add("http://tamil.thehindu.com/tamilnadu/?service=rss");
        // rssFeedList.add("http://syndication.indianexpress.com/rss/latest-news.xml");
        // rssFeedList.add("http://www.asianage.com/rss/37");


        for (String feedLink : rssFeedList) {
            System.out.println("RSS Source:"+feedLink);
            try {
                getArticleDetails(feedLink);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }
        linkDAO.cleanUpDuplicates();
        logger.info("Rss Feeds End");
    }


    public void getArticleDetails(String rssFeedLink) throws Exception {

        //create a new FeedParser...
        FeedParser parser = FeedParserFactory.newFeedParser();

        //create a listener for handling our callbacks
        FeedParserListener listener = new DefaultFeedParserListener() {
            String channel = null;

            public void onChannel(FeedParserState state,
                                  String title,
                                  String link,
                                  String description) throws FeedParserException {
                channel = title;

            }

            public void onItem(FeedParserState state,
                               String title,
                               String url,
                               String description,
                               String permalink) throws FeedParserException {
                String imageURL = "";
                String desc="";
                String lang="Tamil";
                System.out.println("title"+title);
                if (channel.contains("DailyThanthi") || channel.contains("Dinakaran")
                        || channel.contains("தி இந்து")  || channel.contains("Dinamani")
                        || channel.contains("The Hindu - Home")|| channel.contains("Top Headlines")|| channel.contains("The Asian Age")) {
                    String responseBody = null;
                    try {
                        responseBody = getResponseBody(url);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (channel.contains("DailyThanthi")) {
                        imageURL = getDialyThanthiImgURL(responseBody);
                        desc=(description.length()>400)?description.substring(0,400).replaceAll("'",""):description.replaceAll("'","");


                    }else if (channel.contains("Dinakaran")) {
                        imageURL = getDinakaranImgURL(responseBody);

                        desc=description.substring(description.indexOf("<p>")+3,description.indexOf("</p>")-3);
                        desc=(desc.length()>400)?desc.substring(0,400).replaceAll("'",""):desc.replaceAll("'","");


                    } else if (channel.contains("தி இந்து" )|| (channel.contains("தி இந்து - தமிழகம் " )) ) {
                        imageURL = getTamilHinduImgURL(responseBody);
                        desc=description.replaceAll("'","");

                    }else if (channel.contains("The Hindu - Home" )) {
                        imageURL = getTamilHinduImgURL(responseBody);
                        lang="English";
                        desc=description.replaceAll("'","");

                    }else if (channel.contains("Top Headlines")) {
                        imageURL = getEnglishExpressImgURL(responseBody);

                        lang="English";
                        desc=description.replaceAll("'","");

                    }else if (channel.contains("The Asian Age")) {
                        imageURL = getAsianAgeImgURL(responseBody);
                        //imageURL = "http://bit.ly/1P6rRrd";
                        lang="English";
                        desc=description.replaceAll("'","");

                    }
                    /* else if (channel.contains("India Business News")) {
                        imageURL = getOneIndiaImgURL(responseBody);

                    } else if (channel.contains("Dinamani")) {
                        imageURL = getDinamaniImgURL(responseBody);
                    }
                }

                if (channel.contains("Dinamalar")) {
                    imageURL = getDinamalarImgURL(description);
                    description=description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>"));
                    desc=description.replaceAll("<div>","").replaceAll("</div>","").replaceAll("<b>","").replaceAll("</b>","").replaceAll("'","");
                    desc=(desc.length()>400)?desc.substring(0,400):desc;

                }

                String tags[] = {"News"};
                if (!linkDAO.isLinkPresent(url)) {
                  //  linkDAO.addLink(url, title, "999", lang, "News", "India", "Text/HTML", tags, imageURL, desc);

                }
               /* else if (!linkDAO.isLinkPresent(url) && (channel.contains("The Hindu - Home" ) )){
                    linkDAO.addLink(url, title, "999", "English", "News", "India", "Text/HTML", tags, imageURL, desc);
                }
                else{
                    linkDAO.addLink(url, title, "999", "Tamil", "News", "India", "Text/HTML", tags, imageURL, desc);
                }*/
       /*     }
        };

        //specify the feed we want to fetch
        String resource = rssFeedLink;
        //use the FeedParser network IO package to fetch our resource URL
        ResourceRequest request = ResourceRequestFactory.getResourceRequest(resource);
        //grab our input stream
        InputStream is = request.getInputStream();
        //start parsing our feed and have the above onItem methods called
        parser.parse(listener, is, resource);
    }

    public String getResponseBody(String url) throws IOException {
        String responseBody = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(url);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                // @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };

            responseBody = httpclient.execute(httpget, responseHandler);
        } finally {
            httpclient.close();
        }
        return responseBody;
    }


    public String getDialyThanthiImgURL(String responseBody) {
        if (responseBody != null && responseBody.contains("ContentPlaceHolder1_Image1")) {
            responseBody = responseBody.substring(responseBody.indexOf("ContentPlaceHolder1_Image1"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("/>") - 2);
            imageURL = responseBody;
        } else {
            imageURL = "http://stat.dailythanthi.com/DailyThanthiUI/Images/dailythanthilogo.PNG";
        }
        return imageURL;
    }

    public String getDinakaranImgURL(String responseBody) {
//        String responseText = responseBody.substring(responseBody.indexOf("main-news"), responseBody.indexOf("vtnEditorialPlayer"));
        if (responseBody.contains("main-img")) {
            responseBody = responseBody.substring(responseBody.indexOf("main-img")-150);
            responseBody = responseBody.substring(responseBody.indexOf("src=")+5,responseBody.indexOf("class")-2);
            imageURL = responseBody;
        } else {
            imageURL = "http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";
        }
        if(imageURL.contains("http://img.dinakaran.com/data1/DNewsimages/"))
        {
            imageURL = "http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";
        }
        return imageURL;
    }

    public String getDinamalarImgURL(String description) {
        if (description != null && description.contains("img src=")) {
            String[] image = description.split("</a>");
            String temp = image[0];
            temp = temp.substring(temp.indexOf("src='"), temp.indexOf("border"));
            temp = temp.substring(temp.indexOf("'") + 1, temp.lastIndexOf("'"));
            imageURL = temp;
        } else {
            imageURL = "http://epaper.dinamalar.com/PUBLICATIONS/DM/MADHURAI/images/big_logo.gif";
        }
        return imageURL;
    }

    public String getTamilHinduImgURL(String responseBody) {
        if (responseBody != null && responseBody.contains("hcenter")) {
            responseBody = responseBody.substring(responseBody.indexOf("hcenter"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("class") - 2);
            imageURL = responseBody;
        } else if (responseBody != null && responseBody.contains("og:image")) {
            responseBody = responseBody.substring(responseBody.indexOf("og:image\""));
            responseBody = responseBody.substring(responseBody.indexOf("content=\"") + 9, responseBody.indexOf("/>") - 2);
            System.out.println("IMAGE URL OF HINDU ==========>"+responseBody);
            imageURL = responseBody;
        } else {
            imageURL = "http://i.imgur.com/jvGPMA2.png";
        }
        return imageURL;
    }
    public String getEnglishExpressImgURL(String responseBody) {
        if (responseBody.contains("storybigpic2")) {
            responseBody = responseBody.substring(responseBody.indexOf("storybigpic2"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("width") - 2);
            imageURL = responseBody;
        } /*else if (responseBody.contains("text-embed")) {
            responseBody = responseBody.substring(responseBody.indexOf("text-embed"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("class") - 2);
            imageURL = responseBody;
     /*   }else {
            imageURL = "http://bit.ly/1QoeMNx";
        }
        return imageURL;
    }
    public String getAsianAgeImgURL(String responseBody) {
        if (responseBody.contains("image-attach-body")) {
            responseBody = responseBody.substring(responseBody.indexOf("image-attach-body"));
            responseBody = responseBody.substring(responseBody.indexOf("img src=") + 9, responseBody.indexOf("alt") - 2);

            imageURL = responseBody;
        } /*else if (responseBody.contains("text-embed")) {
            responseBody = responseBody.substring(responseBody.indexOf("text-embed"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("class") - 2);
            imageURL = responseBody;
        }else {
            imageURL = "http://bit.ly/1P6rRrd";
        }
        return imageURL;
    }
    public String getDinamaniImgURL(String responseBody) {
        if (responseBody != null && responseBody.contains("image-Zoomin")) {
            responseBody = responseBody.substring(responseBody.indexOf("image-Zoomin"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("title")-2);
            imageURL = responseBody;
        } else {
            imageURL = "http://media.dinamani.com/2015/03/06/Din-logo-main.png/article2700950.ece/binary/original/Din-logo-main.png";
        }
        return imageURL;
    }

    public String getOneIndiaImgURL(String responseBody) {
        if (responseBody != null && responseBody.contains("<strong>")) {
            responseBody = responseBody.substring(responseBody.indexOf("big_center_img"));
            responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("vspace") - 2);
            imageURL = responseBody;
        } else {
            imageURL = "https://beautifuljourney.in/Images/media//Oneindia-News_oneindia.jpg";
        }
        return imageURL;
    }
}*/
}

