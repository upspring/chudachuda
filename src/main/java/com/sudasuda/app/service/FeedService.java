package com.sudasuda.app.service;

import com.sudasuda.app.dao.LinkDAO;
import com.sudasuda.app.domain.Link;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by NARAYANA on 10/16/2015.
 */
public class FeedService {
    LinkDAO linkDAO = new LinkDAO();

    public StringBuffer getNewLinks(String timestamp) {
        System.out.println("inside the service layer");

        Link link=new Link();


        StringBuffer feedNews= new StringBuffer().append("<rss version=\"2.0\">\n" +
                "  <channel>\n" +
                "    <title>ChudaChuda.com</title>\n" +
                "    <link>http://http://www.chudachuda.com/</link>\n" +
                "    <description>Visit chudachuda.com</description>");


        List<Link> links = null;

        String xml = null;

        xml= (String) HazelcastCacheService.getInstance().get("xmlFeed");

        if (timestamp == null && xml != null) {
            System.out.println("XML From cache - "+xml);
            return new StringBuffer(xml);
        }

        if ( timestamp != null && timestamp.trim().length() > 0 ) {

            links = linkDAO.getFeedLinks(999, "Tamil", "News", "India", timestamp);
            System.out.println("rss feed link 1 "+links);

        }
        else {
            links = linkDAO.getNewLinks(999, "Tamil", "News", "India","0","30");

        }
        for(Link feed:links)
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println("Date --- > " + dateFormat.format(feed.getDateCreated()));
            System.out.println("title="+feed.getTitle()+"--->"+feed.getTitle().indexOf('<'));
            System.out.println("desc=" + feed.getDesc() + "--->" + feed.getDesc().indexOf('<'));
            feed.setTitle(feed.getTitle().replace('<', ' '));
            feed.setTitle(feed.getTitle().replace('>', ' '));
            feed.setDesc(feed.getDesc().replace('<',' '));
            feed.setDesc(feed.getDesc().replace('>',' '));
            if (feed.getImageUrl() == null || feed.getImageUrl().trim().length()==0) continue;
            feedNews.append("\n<item>\n <title>" + feed.getTitle() + "</title>\n").append("<link>" + feed.getUrl().replaceAll("&utm", "&amp;utm") + "</link>\n").append("<author>" + feed.getImageUrl() + "</author>\n").append("<description>" + feed.getDesc() + "</description>\n").append("<pubdate>" + dateFormat.format(feed.getDateCreated()) +"</pubdate>\n").append("</item>\n");

        }
        feedNews.append("\n</channel>\n").append("</rss>");

        if (timestamp == null )
            HazelcastCacheService.getInstance().put("xmlFeed", feedNews.toString());

        return feedNews;
    }

    public String getNewJsonLinks(String timestamp) {

        Link link = new Link();

        List<Link> links = null;

        JSONObject linkJSON = new JSONObject();
        JSONArray linkArray = new JSONArray();


        String json = null;

        json = (String) HazelcastCacheService.getInstance().get("jsonFeed");

        if (timestamp == null && json != null) {
            System.out.println("json From cache - " + json);
            return new String(json);
        }
        if (timestamp != null && timestamp.trim().length() > 0) {
            links = linkDAO.getFeedLinks(999, "Tamil", "News", "India", timestamp);

            System.out.println("links" + links);
        }
        else {
            links = linkDAO.getNewLinks(999, "Tamil", "News", "India","0","50");
            //System.out.println("links2"+links);
        }

        System.out.println("size---->" + links.size());

        for (int i = 0; i < links.size(); i++)
        {

            Link feed = links.get(i);
            //System.out.println("title--->" + feed.getTitle());

            JSONObject row = new JSONObject();
            row.put("title", feed.getTitle());
            row.put("url", feed.getUrl());
            row.put("category", feed.getCategory());
            row.put("image_url", feed.getImageUrl());
            row.put("desc", feed.getDesc());
            row.put("lastHour", feed.getHoursElapsed());
            if (feed.getDomain().equalsIgnoreCase("election.dinamalar.com"))
                row.put("domain", "தினமலர்");
            else if (feed.getDomain().equalsIgnoreCase("panithulishankar.com"))
                row.put("domain", "பனிதுளிசங்கர்");
            else if (feed.getDomain().equalsIgnoreCase("adarasaka.com"))
                row.put("domain", "அட்ரா சக்க");
            else if (feed.getDomain().equalsIgnoreCase("dinamani.com"))
                row.put("domain", "தினமனி");
            else if (feed.getDomain().equalsIgnoreCase("malayagam.lk"))
                row.put("domain", "மலயகம்");
            else if (feed.getDomain().equalsIgnoreCase("dinamalar.com"))
                row.put("domain", "தினமலர்");
            else if (feed.getDomain().equalsIgnoreCase("thehindhu.com"))
                row.put("domain", "திஹிந்து");
            else if (feed.getDomain().equalsIgnoreCase("feedproxy.google.com"))
                row.put("domain", "தினமலர்");
            else if (feed.getDomain().equalsIgnoreCase("vikatan.com"))

                row.put("domain", "விகடன்");
            else if (feed.getDomain().equalsIgnoreCase("dailythanthi.com"))


                row.put("domain", " தினதந்தி");

            else if (feed.getDomain().equalsIgnoreCase("viduthalai.in.com"))


                row.put("domain", "விடுதலை");
            else if (feed.getDomain().equalsIgnoreCase("samayalkurippu.com"))


                row.put("domain", "சமயல்குறிப்பு");
            else if (feed.getDomain().equalsIgnoreCase("tamil.thehindu.com"))


                row.put("domain", "தி இந்து");

            else
                row.put("domain", feed.getDomain());

            linkArray.put(row);
            if (feed.getImageUrl() == null || feed.getImageUrl().trim().length() == 0) continue;

        }

        linkJSON.put("feed", linkArray);
        if (timestamp == null)
            HazelcastCacheService.getInstance().put("jsonFeed", linkJSON.toString());
        return linkJSON.toString();

    }



}
