package com.sudasuda.app.service;

import com.sudasuda.app.dao.NotificationDAO;

import java.util.List;

/**
 * Created by venkksastry on 04/09/16.
 */
public class NotificationService {

    NotificationDAO dao = new NotificationDAO();

    public void registerDevice(String deviceid)
    {
        dao.addMobileDevice(deviceid);
    }

    public List<String> getMobileDevices()
    {
        return dao.getMobileDevices();
    }

}
