package com.sudasuda.app.service;

import com.sudasuda.app.dao.LinkDAO;
import com.sudasuda.app.domain.Link;
import com.sudasuda.app.domain.User;
import com.sudasuda.app.vo.NameCountVO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

public class LinkService {

	LinkDAO linkDAO = new LinkDAO();

	public List<Link> getLinks(int currUser, String language, String category, String country) {
		List<Link> links = linkDAO
				.getCurrentLinks(currUser, language, category, country);

		return links;
	}

	public List<Link> getNewLinks(int currUser, String language, String category, String country,String startLimit,String endLimit) {

		List<Link> links=null;

			links= linkDAO.getNewLinks(currUser, language, category, country,startLimit,endLimit);



		return links;
	}

	public List<Link> getExpiredLinks(int currUser, String language,
									  String category, String country) {
		List<Link> links = linkDAO
				.getExpiredLinks(currUser, language, category, country);

		return links;
	}

	public void updateLink(Link link) {

		linkDAO.updateLink(link);
	}


	public boolean isBookmarkedAlready(int userId, int linkId) {
		return linkDAO.isBookmarkedAlready(userId, linkId);
	}

	public void addBookmark(int linkid, int userid) {

		linkDAO.addBookmark(linkid, userid);

	}
	public void removeBookmark(int userId, int linkId) {

		linkDAO.removeBookmark(userId, linkId);

	}

	public boolean isVotedAlready(int userId, int linkId) {
		return linkDAO.isVotedAlready(userId, linkId);
	}

	public void voteUpLink(int linkid, int userid) {
		linkDAO.voteUpLink(linkid, userid);
	}

	public void addLink(String url, String title, String userId,
						String language, String category, String country, String media_type, String[] tags, String imageUrl, String desc) {
		linkDAO.addLink(url, title, userId, language, category, country, media_type, tags, imageUrl, desc);
	}
   /*public void saveBookmark(String linkId,String emailId)
   {
      linkDAO.saveBookmark(linkId,emailId);
   }*/

	public boolean isLinkPresent(String url) {
		return linkDAO.isLinkPresent(url);
	}

	public List<NameCountVO> getCategoryLinkCount(int userId, int type, String cond) {
		return linkDAO.getCategoryLinkCount(userId, type, cond);
	}

	public List<Link> getLinksSubmittedBy(int submittedBy) {
		List<Link> links = linkDAO.getSubmittedByLinks(submittedBy);

		return links;
	}

	public Link getLink(int linkId) {
		return linkDAO.getLink(linkId);
	}

	public List<Link> getNews_Detail(String linkId,int currUser) {
		List<Link> links=linkDAO.getNews_Detail(linkId,currUser);
		return links;
	}

	public List<Link> getDomainLinks(String domain) {
		return linkDAO.getDomainLinks(domain);
	}

	public List<Link> getTagLinks(String tag) {
		return linkDAO.getTagLinks(tag);
	}
	public void addSpam(int linkId, User user) {
		linkDAO.addSpam(linkId, user);
	}

	public void applyTag(String tagName, int linkId) {
		linkDAO.applyTag(tagName, linkId);
	}

	public List<Link> getMyLinks(int userId, String lang, String category, String country)
	{
		return linkDAO.getMyLinks(userId, lang, category, country);
	}

	public List<NameCountVO> getMyTags(int userId) {
		return linkDAO.getMyTags(userId);
	}

	public List<String> getRecentSubmitters() {
		return linkDAO.getRecentlySubmittedUsers();
	}

	public void updateImageUrl(int linkid, String imageUrl)
	{
		linkDAO.updateImageUrl(linkid, imageUrl);
	}

	public List<Link> getMyBookmarks(int userId, String lang, String category, String country) {

		return linkDAO.getMyBookmarks(userId, lang, category, country);
	}
	public String getCategoryNewsForMobile(String category)
	{



		Link link = new Link();

		List<Link> links = null;

		JSONObject linkJSON = new JSONObject();
		JSONArray linkArray = new JSONArray();

		String json = null;

		//json = (String) HazelcastCacheService.getInstance().get("jsonFeed");
		InputStream input = null;
		Properties prop = new Properties();
		try {
			//input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");


			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			System.out.println("json limit here"+prop.getProperty("JsonLimit"));


			String val=prop.getProperty("JsonLimit");
			String descriptionLength=prop.getProperty("DescriptionLength");
			int descLength=Integer.parseInt(descriptionLength);
			links = linkDAO.getCategoryNewsForMobile(category,val,descLength);

			for (int i = 0; i < links.size(); i++) {

				Link feed = links.get(i);

				JSONObject row = new JSONObject();
				row.put("id",feed.getLinkId());
				row.put("title", feed.getTitle());
				row.put("url", feed.getUrl());
				row.put("category", feed.getCategory());
				row.put("image_url", feed.getImageUrl());
				row.put("desc", feed.getDesc());
				row.put("likes",feed.getLikes());
				row.put("liked",feed.getLiked());
				row.put("bookmarked",feed.getBookmark());
				row.put("lastHour", feed.getHoursElapsed());

				if (feed.getDomain().equalsIgnoreCase("tamil.filmibeat.com"))
					row.put("domain", "பிலிமி பீட்");
				else if (feed.getDomain().equalsIgnoreCase("dinakaran.com"))
					row.put("domain", "தினகரன் ");
				else if (feed.getDomain().equalsIgnoreCase("cinema.dinakaran.com"))
					row.put("domain", "தினகரன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.samayam.com"))
					row.put("domain", "சமயம்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.oneindia.com"))
					row.put("domain", "ஒன் இந்தியா");
				else if(feed.getDomain().equalsIgnoreCase("bbc.com"))
					row.put("domain","பிபிசி தமிழ் ");
				else if(feed.getDomain().equalsIgnoreCase("tamil.webdunia.com"))
					row.put("domain","வெப்துனியா");
				else if(feed.getDomain().equalsIgnoreCase("zeenews.india.com"))
					row.put("domain","ஜீ நியூஸ் ");
				else if(feed.getDomain().equalsIgnoreCase("dinamalar.com"))
					row.put("domain","தினமலர்");
				else if(feed.getDomain().equalsIgnoreCase("cinema.dinamalar.com"))
					row.put("domain","தினமலர்");
				else if (feed.getDomain().equalsIgnoreCase("vikatan.com"))
					row.put("domain", "விகடன்");
				else if (feed.getDomain().equalsIgnoreCase("cinema.vikatan.com"))
					row.put("domain", "விகடன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.thehindu.com"))
					row.put("domain", "தி இந்து");
				else if (feed.getDomain().equalsIgnoreCase("hindutamil.in"))
					row.put("domain", "தி இந்து");
				else
					row.put("domain", feed.getDomain());

				linkArray.put(row);
				if (feed.getImageUrl() == null || feed.getImageUrl().trim().length() == 0) continue;

			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		linkJSON.put("feed", linkArray);

		return linkJSON.toString();


	}
	public List<Link> getCategoryNewsForWeb(String category,int currUser,String start,String end) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			//input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");


			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			int descLength=Integer.parseInt(descriptionLength);
			String limit=prop.getProperty("TotalNews");
			links= linkDAO.getCategoryNews(category,descLength,currUser,start,end);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;

		//return linkDAO.getCategoryNews(category);
	}
	public List<Link> getNews(String category,int currUser,String start,String end) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			//input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");


			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			int descLength=Integer.parseInt(descriptionLength);
			String limit=prop.getProperty("TotalNews");
			links= linkDAO.getNews(category,descLength,currUser,start,end);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;

		//return linkDAO.getCategoryNews(category);
	}
	public List<Link> getFrontPageNews(int currUser,String start,String end) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			//input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");


			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			int descLength=Integer.parseInt(descriptionLength);
			String limit=prop.getProperty("TotalNews");
			links= linkDAO.getFrontPageNews(currUser,start,end);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;

		//return linkDAO.getCategoryNews(category);
	}
	public List<Link> getCategoryForNews(String category,String domain,int currUser,String start,String end) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			//input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");


			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			int descLength=Integer.parseInt(descriptionLength);
			String limit=prop.getProperty("TotalNews");
			links= linkDAO.getCategoryForNews(category,domain,descLength,currUser,start,end);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;

		//return linkDAO.getCategoryNews(category);
	}
	public String getTopNews() {

		Link link = new Link();

		List<Link> links = null;

		JSONObject linkJSON = new JSONObject();
		JSONArray linkArray = new JSONArray();

		String json = null;


		try {



			links = linkDAO.getTopNews();

			for (int i = 0; i < links.size(); i++) {

				Link feed = links.get(i);

				JSONObject row = new JSONObject();
				row.put("id",feed.getLinkId());
				row.put("title", feed.getTitle());
				row.put("url", feed.getUrl());
				row.put("category", feed.getCategory());
				row.put("image_url", feed.getImageUrl());
				row.put("desc", feed.getDesc());
				row.put("lastHour", feed.getHoursElapsed());

				if (feed.getDomain().equalsIgnoreCase("tamil.filmibeat.com"))
					row.put("domain", "பிலிமி பீட்");
				else if (feed.getDomain().equalsIgnoreCase("dinakaran.com"))
					row.put("domain", "தினகரன் ");
				else if (feed.getDomain().equalsIgnoreCase("cinema.dinakaran.com"))
					row.put("domain", "தினகரன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.samayam.com"))
					row.put("domain", "சமயம்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.oneindia.com"))
					row.put("domain", "ஒன் இந்தியா");
				else if (feed.getDomain().equalsIgnoreCase("vikatan.com"))

					row.put("domain", "விகடன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.thehindu.com"))


					row.put("domain", "தி இந்து");
				else if (feed.getDomain().equalsIgnoreCase("hindutamil.in"))
					row.put("domain", "தி இந்து");

				else
					row.put("domain", feed.getDomain());

				linkArray.put(row);
				if (feed.getImageUrl() == null || feed.getImageUrl().trim().length() == 0) continue;

			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

		linkJSON.put("feed", linkArray);

		return linkJSON.toString();


	}

	public String getVideoFeed() {

		Link link = new Link();

		List<Link> links = null;

		JSONObject linkJSON = new JSONObject();
		JSONArray linkArray = new JSONArray();

		String json = null;


		try {



			links = linkDAO.getVideoFeed();

			for (int i = 0; i < links.size(); i++) {

				Link feed = links.get(i);

				JSONObject row = new JSONObject();
				row.put("id",feed.getLinkId());
				row.put("title", feed.getTitle());
				row.put("url", feed.getUrl());
				row.put("category", feed.getCategory());
				row.put("image_url", feed.getImageUrl());
				row.put("desc", feed.getDesc());
				row.put("type",feed.getMedia_type());
				row.put("format",feed.getVideo_format());
				row.put("likes", feed.getLikes());
				row.put("liked", feed.getLiked());
				row.put("lastHour", feed.getHoursElapsed());

				if (feed.getDomain().equalsIgnoreCase("tamil.filmibeat.com"))
					row.put("domain", "பிலிமி பீட்");
				else if (feed.getDomain().equalsIgnoreCase("dinakaran.com"))
					row.put("domain", "தினகரன் ");
				else if (feed.getDomain().equalsIgnoreCase("cinema.dinakaran.com"))
					row.put("domain", "தினகரன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.samayam.com"))
					row.put("domain", "சமயம்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.oneindia.com"))
					row.put("domain", "ஒன் இந்தியா");
				else if (feed.getDomain().equalsIgnoreCase("vikatan.com"))

					row.put("domain", "விகடன்");
				else if (feed.getDomain().equalsIgnoreCase("tamil.thehindu.com"))

					row.put("domain", "தி இந்து");
				else if (feed.getDomain().equalsIgnoreCase("hindutamil.in"))

					row.put("domain", "தி இந்து");

				else
					row.put("domain", feed.getDomain());

				linkArray.put(row);
				if (feed.getImageUrl() == null || feed.getImageUrl().trim().length() == 0) continue;

			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

		linkJSON.put("feed", linkArray);

		return linkJSON.toString();


	}


	public int updateLikes(int id,String mobile_id)
	{
		return linkDAO.updateLikes(id,mobile_id);
	}
	public ArrayList isLiked(String deviceId,String domain)
	{
		return linkDAO.isLiked(deviceId,domain);
	}

	public ArrayList isLikedForKathambam(String deviceId)
	{
		return linkDAO.isLikedForKathambam(deviceId);
	}

	public int updateBookmark(int id,String mobile_id)
	{
		return linkDAO.updateBookmark(id,mobile_id);
	}


	public ArrayList isBookmarked(String deviceId,String domain)
	{
		return linkDAO.isBookmarked(deviceId,domain);
	}

	public String getBookmarks(String deviceId) {
		new Link();
		List links = null;
		JSONObject linkJSON = new JSONObject();
		JSONArray linkArray = new JSONArray();
		Object json = null;

		try {
			links = this.linkDAO.getBookmarks(deviceId);

			for (int i = 0; i < links.size(); ++i) {
				Link feed = (Link) links.get(i);
				JSONObject row = new JSONObject();
				row.put("id", feed.getLinkId());
				row.put("title", feed.getTitle());
				row.put("url", feed.getUrl());
				row.put("image_url", feed.getImageUrl());
				row.put("desc", feed.getDesc());
				row.put("domain", feed.getDomain());
				row.put("bookmarkCreated",feed.getDateCreated());
				if (feed.getDomain().equalsIgnoreCase("tamil.filmibeat.com")) {
					row.put("domain", "பிலிமி பீட்");
				} else if (feed.getDomain().equalsIgnoreCase("dinakaran.com")) {
					row.put("domain", "தினகரன்");
				} else if (feed.getDomain().equalsIgnoreCase("cinema.dinakaran.com")) {
					row.put("domain", "தினகரன்");
				} else if (feed.getDomain().equalsIgnoreCase("tamil.samayam.com")) {
					row.put("domain", "சமயம்");
				} else if (feed.getDomain().equalsIgnoreCase("tamil.oneindia.com")) {
					row.put("domain", "ஒன் இந்தியா");
				} else if (feed.getDomain().equalsIgnoreCase("vikatan.com")) {
					row.put("domain", "விகடன்");
				} else if (feed.getDomain().equalsIgnoreCase("tamil.thehindu.com")) {
					row.put("domain", "தி இந்து");
				} else {
					row.put("domain", feed.getDomain());
				}

				linkArray.put(row);
				if (feed.getImageUrl() == null || feed.getImageUrl().trim().length() == 0) continue;

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		linkJSON.put("feed", linkArray);
		return linkJSON.toString();
	}

	public HashMap<String, String> likeArticles(String emailId, String linkId, float longitude,float latitude,String conutry)
	{
		System.out.println("inside of likeArticles");
		return linkDAO.likeArticles(emailId,linkId,longitude,latitude,conutry);
	}

	public HashMap<String, String> disLikeArticles(String emailId, String linkId,float longitude,float latitude,String country)
	{
		System.out.println("inside of likeArticles");
		return linkDAO.disLikeArticles(emailId,linkId,longitude,latitude,country);
	}
	public  void sendEmail(String emailId,String title,String imageUrl,String url,String domain)
	{

		System.out.println("email--->");
		System.out.println("emailId--->"+emailId);
		System.out.println("emailId--->"+title);
		System.out.println("emailId--->"+imageUrl);
		System.out.println("emailId--->"+url);

		try {
			String userName= "sathiya";
			String apiKey= "850c991d-fe69-44ee-bccb-94fc8edb0a1a";
			//"a526a16a-909a-48d9-bc62-91abc034999b";
			String fromName="chudachuda";
			String from="info@upspring.it";
			String subject="Welcome to chudachuda";
			String isTransactional="false";


			final String body = String.join(
					"<!DOCTYPE html>",
					"<html>",
					"<head>",
					"<title>chudachuda.com</title>",
					"</head>",
					"<body style='font-family: sans-serif; margin:30px;'>",
					"<table style='border-collapse: collapse; table-layout: fixed; border-spacing: 0;vertical-align: top; max-width: 570px; margin: 0 auto; background-color: #f6f6f6;'>",
					"                                <tr>" ,
					"                                    <td align='center'>" ,
					"                                        <div style='margin: 0 auto; padding:10px'>" ,
					"                                            <h4 style='font-size: 32px;padding: 0; margin:0'>சுடசுட</h4>",
					"                                        </div>" ,
					"                                    </td>" ,
					"                                </tr>",
					"                              <tr>" ,
					"                                  <td width='100%' height='320px' align='center' style='background-image: url("+imageUrl+"); background-repeat: no-repeat; background-size: cover; background-color:#fcd539;' border='0' cellspacing='0' cellpadding='0'>" ,

					"                                  </td>",
					"                              </tr>",
					"                              <tr>",
					"                                  <td style='padding: 15px 15px; color: black'>" ,
					"                                      <div style='padding:0; margin: 0;'>" ,
					"                                          <a href='"+url +"' style='text-decoration:none' target='_blank'><h4 style='font-size: 20px; line-height: 27px; color: green;'>"+title+"</h4></a>",
					"                                      </div>" ,
					"                                      <div style='padding:0; margin:0'>" ,
					"                                          <a href='"+domain+"' style='text-decoration: none;'><p style='color: #337ab7; font-size: 15px; font-family: sans-serif;'>"+domain+"</p></a>",
					"                                      </div>",
					"                                  </td>",
					"                              </tr>",
					"                            <tr  style='background-color: #ff6f00;'>",
					"                               <td align='center' style='padding: 10px;'>",
					"                                   <span style='color: white; font-size: 18px; font-weight: bold; font-family: sans-serif;'>For More Info</span>" ,
					"                                   <a style='display: block;color: white; font-size: 13px; letter-spacing:1px; color: white; font-family: sans-serif; text-decoration: none; padding-top: 8px;' href='https://www.chudachuda.com'>chudachuda.com</a>",
					"                               </td>",
					"                            </tr>" ,
					"                   </table>",

					"</body>",
					"</html>"

			);



			String encoding = "UTF-8";

			String data = "apikey=" + URLEncoder.encode(apiKey, encoding);
			data += "&from=" + URLEncoder.encode(from, encoding);
			data += "&fromName=" + URLEncoder.encode(fromName, encoding);
			data += "&subject=" + URLEncoder.encode(subject, encoding);
			data += "&bodyHtml=" + URLEncoder.encode(body, encoding);
			data += "&to=" + URLEncoder.encode(emailId, encoding);
			data += "&isTransactional=" + URLEncoder.encode(isTransactional, encoding);

			URL url1 = new URL("https://api.elasticemail.com/v2/email/send");
			URLConnection conn = url1.openConnection();
			conn.setDoOutput(true);

			Map<String, String> headers = new HashMap<>();


			headers.put("Content-type", "application/x-www-form-urlencoded");
			headers.put("Accept", "text/plain");

			for (String headerKey : headers.keySet()) {
				conn.setRequestProperty(headerKey, headers.get(headerKey));
			}

			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String result = rd.readLine();
			wr.close();
			rd.close();

			System.out.println("Message is Sent"+result);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}


	public List<Link> getTrendingLinks(int currUser, String language, String category, String country) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			int descLength=Integer.parseInt(descriptionLength);

			links= linkDAO.getTrendingLinks(currUser, language, category, country,descLength);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;
	}
	public void saveBookmark(String linkId,String emailId)
	{
		linkDAO.saveBookmark(linkId,emailId);
	}

	public List<Link> getReadLaterArticle( String emailId,int currUsr) {
		List<Link> links = linkDAO.getReadLaterArticle(emailId,currUsr);

		return links;
	}


	public List<Link> getRecommendationLinks(int currUser) {

		List<Link> links=null;
		InputStream input = null;
		Properties prop = new Properties();
		try {
			File file = ResourceUtils.getFile("classpath:application.properties");
			InputStream in = new FileInputStream(file);
			prop.load(in);
			String descriptionLength=prop.getProperty("DescriptionLength");

			String compareLimit=prop.getProperty("comparisonLimit");
			Integer limit= Integer.parseInt(compareLimit);

			int descLength=Integer.parseInt(descriptionLength);
			links= linkDAO.getRecommendationLinks(currUser,descLength,limit);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		return links;
	}
	public   List<HashMap<String,String>> getMapDetailsForArticle(String linkId)
	{
		List<HashMap<String,String>> t= new ArrayList<>();
		List<HashMap<String,String>> f= new ArrayList<>();
		List<HashMap<String,String>> s= new ArrayList<>();
		List<HashMap<String,String>> map= new ArrayList<>();
		t=linkDAO.getMapDetailsForArticle(linkId);




		for(int i=0;i<t.size();i++)
		{
			System.out.println(t.get(i));
			if(s.size()==0)
			{
				s.add(t.get(i));
				map.add(t.get(i));
			}



			for(int j=0;j<s.size();j++)
			{
				System.out.println("s--->j"+j+s.get(j));
				System.out.println("t--->i"+i+t.get(i));


				if(!t.get(i).get("country").equals(s.get(j).get("country")))
				{

					System.out.println(!t.get(i).get("country").equals(s.get(j).get("country")));
					System.out.println("inside first if");
					int exists=0;
					for(int k=0;k<map.size();k++)
					{
						System.out.println("1--"+map.get(k).get("country"));
						System.out.println("2---"+t.get(i).get("country"));
						if(map.get(k).get("country").contains(t.get(i).get("country"))) {
							//s.add(t.get(i));
							//map.add(t.get(i));
							//s=map;
							System.out.println("is there");
							exists=1;
						}
					}
					if(exists==0)
					{
						map.add(t.get(i));
						System.out.println("added 1");
					}

				}
				else
				{
					System.out.println("else");
					System.out.println(!s.contains(t.get(i)));
					if(!s.contains(t.get(i)))
					{
						if(s.get(j).get("count").equals(t.get(i).get("count")))
						{
							if(s.get(j).get("voted").contains("-1") && t.get(i).get("voted").contains("1"))
							{
								System.out.println("voted check");
								System.out.println("added 2");
								map.remove(s.get(j));
								map.add(t.get(i));
								//s.add(t.get(i));
								s=map;
								continue;
							}
						}
						if(Integer.parseInt(s.get(j).get("count"))<Integer.parseInt(t.get(i).get("count")))
						{
							System.out.println("count check ");
							System.out.println("added 3");

							map.remove(s.get(j));
							map.add(t.get(i));

							s=map;

						}
					}
				}


			}

		}




		return map;

	}

}