package com.sudasuda.app.domain;

import java.sql.Time;
import java.util.Date;

public class Link implements java.io.Serializable{

	private int linkId;

	private String url;
	private String title;
	private String submitedBy;
	private boolean voted;
	private boolean bookmarked;

	private long hoursElapsed;
	private int votes;
	private int noOfComments;
	private String domain;
	private String language;
	private String category;
	private String category2;
	private String country;
	private String tags;
	private int spam;
	private int activists;
	private String imageUrl;
	private String desc;
	private int position;
	private int likes;
	private int liked;
	private String deviceId;
	private String media_type;
	private String video_format;
	private int bookmark;
	private int vote_up;
	private int vote_down;
	private String userVoted;
	private String userId;
	private Date dateCreated;
	private String Domain2;
	private Time hoursElapsed2;

	public int getLinkId() {
		return linkId;
	}

	public void setLinkId(int linkId) {
		this.linkId = linkId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}

	public String getSubmitedBy() {
		return submitedBy;
	}

	public void setSubmitedBy(String submitedBy) {
		this.submitedBy = submitedBy;
	}

	public boolean isVoted() {
		return voted;
	}
	public void setVoted(boolean voted) {
		this.voted = voted;
	}


	public boolean isBookmarked()
	{
		return bookmarked;
	}

	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}





	public long getHoursElapsed() {
		return hoursElapsed;
	}

	public void setHoursElapsed(long hoursElapsed) {
		this.hoursElapsed = hoursElapsed;
	}

	public int getNoOfComments() {
		return noOfComments;
	}

	public void setNoOfComments(int noOfComments) {
		this.noOfComments = noOfComments;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getSpam() {
		return spam;
	}

	public void setSpam(int spam) {
		this.spam = spam;
	}

	public int getActivists() {
		return activists;
	}

	public void setActivists(int activists) {
		this.activists = activists;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getLiked() {
		return liked;
	}

	public void setLiked(int liked) {
		this.liked = liked;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getMedia_type() {
		return media_type;
	}

	public void setMedia_type(String media_type) {
		this.media_type = media_type;
	}

	public String getVideo_format() {
		return video_format;
	}

	public void setVideo_format(String video_format) {
		this.video_format = video_format;
	}

	public int getBookmark() {
		return bookmark;
	}

	public void setBookmark(int bookmark) {
		this.bookmark = bookmark;
	}

	public String getCategory2() {
		return category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public int getVote_up() {
		return vote_up;
	}

	public void setVote_up(int vote_up) {
		this.vote_up = vote_up;
	}

	public int getVote_down() {
		return vote_down;
	}

	public void setVote_down(int vote_down) {
		this.vote_down = vote_down;
	}

	public String getUserVoted() {
		return userVoted;
	}

	public void setUserVoted(String userVoted) {
		this.userVoted = userVoted;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDomain2() {
		return Domain2;
	}

	public void setDomain2(String domain2) {
		Domain2 = domain2;
	}

	public Time getHoursElapsed2() {
		return hoursElapsed2;
	}

	public void setHoursElapsed2(Time hoursElapsed2) {
		this.hoursElapsed2 = hoursElapsed2;
	}
}
