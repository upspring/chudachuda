package com.sudasuda.app.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomAttr;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class ImageFinder {

	public static void main(String[] args) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
//	String responseBody=getResponseBody("http://www.dinamani.com/latest-news/mukkiya-seithigal/2016/sep/23/%E0%AE%AA%E0%AE%BE%E0%AE%95%E0%AE%BF%E0%AE%B8%E0%AF%8D%E0%AE%A4%E0%AE%BE%E0%AE%A9%E0%AF%8D-%E0%AE%A4%E0%AE%A9%E0%AE%BF%E0%AE%AE%E0%AF%88%E0%AE%AA%E0%AF%8D%E0%AE%AA%E0%AE%9F%E0%AF%81%E0%AE%A4%E0%AF%8D%E0%AE%A4%E0%AE%AA%E0%AF%8D%E0%AE%AA%E0%AE%9F%E0%AF%8D%E0%AE%9F%E0%AF%81%E0%AE%B3%E0%AF%8D%E0%AE%B3%E0%AE%A4%E0%AF%81-%E0%AE%AA%E0%AE%BE%E0%AE%B8%E0%AF%8D%E0%AE%B5%E0%AE%BE%E0%AE%A9%E0%AF%8D-2569736.html");
    String responseBody=getResponseBody("http://www.dinakaran.com/News_Detail.asp?Nid=250315");

		responseBody = responseBody.substring(responseBody.indexOf("main-img")-150);
		responseBody = responseBody.substring(responseBody.indexOf("src=")+5,responseBody.indexOf("class")-2);
		System.out.println("IMAGE URL OF dinamani ==========>"+responseBody);




	}
 public static String getResponseBody(String url) throws IOException {
		String responseBody = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			HttpGet httpget = new HttpGet(url);

			// Create a custom response handler
			ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

				// @Override
				public String handleResponse(
						final HttpResponse response) throws ClientProtocolException, IOException {
					int status = response.getStatusLine().getStatusCode();
					if (status >= 200 && status < 300) {
						HttpEntity entity = response.getEntity();
						return entity != null ? EntityUtils.toString(entity) : null;
					} else {
						throw new ClientProtocolException("Unexpected response status: " + status);
					}
				}

			};

			responseBody = httpclient.execute(httpget, responseHandler);
		} finally {
			httpclient.close();
		}
		return responseBody;
	}

}
