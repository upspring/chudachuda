package com.sudasuda.app.utils;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;
import javax.swing.text.AttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class ImageExtractor {

	 
		public static String extractImage(String webUrl) throws Exception {
			String imgSrc = null;
	     //   String webUrl = "http://www.hdwallpapers.in/";
					URL url = new URL(webUrl);
	        URLConnection connection = url.openConnection();
	        InputStream is = connection.getInputStream();
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);

	        HTMLEditorKit htmlKit = new HTMLEditorKit();
	        HTMLDocument htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
	        HTMLEditorKit.Parser parser = new ParserDelegator();
	        HTMLEditorKit.ParserCallback callback = htmlDoc.getReader(0);
	        parser.parse(br, callback, true);
	        
	       /* for (HTMLDocument.Iterator iterator = htmlDoc.getIterator(HTML.Tag.IMG); iterator.isValid(); iterator.next()) {
	        	AttributeSet attributes = iterator.getAttributes();
	            // String imgSrc = (String) attributes.getAttribute(HTML.Attribute.SRC);
	            imgSrc = (String) attributes.getAttribute(HTML.Attribute.SRC);
	            System.out.println("property :"+attributes.getAttribute(HTML.Attribute.HREF));
	             	imgSrc = (String) attributes.getAttribute(HTML.Attribute.HREF);
	            System.out.println("img ="+imgSrc);
           
           }*/
	        
	        for (HTMLDocument.Iterator iterator = htmlDoc.getIterator(HTML.Tag.META); iterator.isValid(); iterator.next()) {
	            AttributeSet attributes = iterator.getAttributes();
	            // String imgSrc = (String) attributes.getAttribute(HTML.Attribute.SRC);
	            imgSrc = (String) attributes.getAttribute(HTML.Attribute.CONTENT);
	            System.out.println("url :"+webUrl+",property :"+attributes.getAttribute("property"));
	            if (attributes.getAttribute("property") != null && ((String)attributes.getAttribute("property")).toLowerCase().contains("og:image"))
	            { 	imgSrc = (String) attributes.getAttribute(HTML.Attribute.CONTENT);
	            	System.out.println("irmg ="+imgSrc);
	            	System.out.println("break");
	            	break;
	            }
	            
	            /*if (imgSrc != null && (imgSrc.contains(".jpg") || (imgSrc.contains(".png")) || (imgSrc.contains(".jpeg")) || (imgSrc.contains(".bmp")) || (imgSrc.contains(".ico")))) {
	                try {
	                	System.out.println("DOWNLOAD: "+imgSrc);
	                    return getImageURL(webUrl, imgSrc);
	                } catch (IOException ex) {
	                    System.out.println(ex.getMessage());
	                }
	            }*/
	        }
	        
	        return imgSrc;
	    }
	    private static String getImageURL(String url, String imgSrc) throws IOException {
	        BufferedImage image = null;
	        
	            if (!(imgSrc.startsWith("http"))) {
	                url = url + imgSrc;
	            } else {
	                url = imgSrc;
	            }
	            
	        /*    imgSrc = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);
	            String imageFormat = null;
	            imageFormat = imgSrc.substring(imgSrc.lastIndexOf(".") + 1);
	            String imgPath = null;
	            imgPath = "/Users/venkksastry/Downloads/imgtest/" + imgSrc + "";
	            URL imageUrl = new URL(url);
	            
	            image = ImageIO.read(imageUrl);
	            if (image != null) {
	            	System.out.println("HOST :"+imageUrl.getHost()+" img:"+imgPath);
	                File file = new File(imgPath);
	                ImageIO.write(image, imageFormat, file);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        } */

	        
	        return url;
	        
	    }
	}
	