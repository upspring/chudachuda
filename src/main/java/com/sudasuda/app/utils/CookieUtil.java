package com.sudasuda.app.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {

	public static String COOKIE_NAME = "login_user_uuid";

	public static String getCookieValue(HttpServletRequest req,
			String cookieName) {
		Cookie[] cookies = req.getCookies();
		String emailId = "";
		String popup="";
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookieName.equalsIgnoreCase(cookie.getName())) {
					return cookie.getValue();
				}
				if (cookie.getName().equals("cc")) {

					emailId = cookie.getValue();
					System.out.println("Login cookie"+emailId);
				}
				if (cookie.getName().equals("rememberMe")) {

					emailId = cookie.getValue();
					System.out.println(" remember me cookie"+emailId);
				}
				if (cookie.getName().equals("popup")) {

					popup = cookie.getValue();
					System.out.println(" remember me cookie"+popup);
				}
			}

		}
		return null;
	}

	public static void addCookie(HttpServletResponse response, String name,
			String value, int maxAge) {
		Cookie cookie = new Cookie(name, value);
		cookie.setPath("/");
		cookie.setMaxAge(maxAge);
		response.addCookie(cookie);
	}

	public static void removeCookie(HttpServletResponse response, String name) {

		System.out.println("cookie is removed ah..."+name);
		addCookie(response, name, null, 0);
	}

}
