package com.sudasuda.app.dao;

import com.sudasuda.app.db.DBConnection;
import com.sudasuda.app.domain.Link;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkksastry on 04/09/16.
 */
public class NotificationDAO {

    private static final Logger logger = LoggerFactory.getLogger(NotificationDAO.class);

    public void addMobileDevice(String deviceid) {
        DataSource ds ;
        Connection conn = null;
        Statement stmt = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "insert into mobile_devices values (0,'" + deviceid + "'," + 0
                    + ",now())";
            logger.info("SQL:" + SQL);
            stmt.executeUpdate(SQL);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public List<String> getMobileDevices() {


        DataSource ds;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<String> deviceids = new ArrayList<String>();

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select deviceid from mobile_devices where deleted=0";

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                deviceids.add(rs.getString("deviceid"));
                }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return deviceids;
    }

}
