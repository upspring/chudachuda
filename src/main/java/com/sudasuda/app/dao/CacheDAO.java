package com.sudasuda.app.dao;

import com.sudasuda.app.db.DBConnection;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 * Created by venkksastry on 21/10/16.
 */
public class CacheDAO {

    public Date getLastUpdatedCache(String cacheName) {

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        Date updateDate = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select update_date from cache where cache_name='" + cacheName + "' and deleted=0";

            System.out.println("SQL: "+SQL);
            System.out.println("print thr caache"+SQL);
            rs = stmt.executeQuery(SQL);

            if (rs.next()) {
                updateDate = rs.getTimestamp("update_date");
                System.out.println("Update Date- ----->" + rs.getTimestamp("update_date"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return updateDate;

    }

    public Date updateCache(String cacheName) {

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        Date updateDate = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "update cache set update_date=now() where cache_name='"+cacheName+"'";

            System.out.println("SQL:"+SQL);
            stmt.executeUpdate(SQL);


        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return updateDate;

    }
}