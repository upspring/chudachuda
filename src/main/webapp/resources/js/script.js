function callFunction1(e){
	e.stopImmediatePropagation();
	if($(this).find(".cat-items").hasClass('hidden')){
		$(this).find(".cat-items").removeClass('hidden');
	}
	else{
		$(this).find(".cat-items").addClass('hidden');
		$(".home-main").removeClass("disabled-div");
	}
}
function callFunction2(e){
	e.stopImmediatePropagation();
}
function fullSidebar(){
	$(".home-sidebar").css({
		"-webkit-transition":"width .5s ease-out",
		"-moz-transition":"width .5s ease-out",
		"-o-transition":"width .5s ease-out",
		"-ms-transition":"width .5s ease-out",
		"transition":"width .5s ease-out",
		width: "250px",
		overflow: "auto"
	});
	$(".logo").css({
		margin:"20px 72px 0 73px",
		"background-size": "auto"
	});
	$(".left-arrow").css({
		display: "inline-block"
	})
	$(".cat-items").css({
		display: "block"
	})
	$(".down-icon").css({
		display: "inline-block"
	})
	$(".s-topic h5").css({
		display: "inline-block"
	})
	$(".s-cat-heading").css({
		padding: "18px 0 18px 40px"
	})	
	$(".s-cat-heading h5").css({
		display: "inline-block"
	})			
	$(".s-topics").css({
		padding: "30px 0"
	})			
	$(".s-topic").css({
		padding: "5px 0 5px 40px"
	})	
	$(".s-cat").on("click",callFunction1);
	$(".cat-items").on("click",callFunction2);
}
function halfSidebar(){
	$(".home-sidebar").css({
		width: "10%",
		overflow: "hidden"
	});
	$(".logo").css({
		"background-size": "80%",
		margin: "20px 0 0 18%"
	});
	$(".left-arrow").css({
		display: "none"
	})
	$(".cat-items").css({
		display: "none"
	})
	$(".down-icon").css({
		display: "none"
	})
	$(".s-topics").css({
		padding: "0"
	})		
	$(".s-topic h5").css({
		display: "none"
	})				
	$(".s-cat-heading").css({
		padding: "18px 0 18px 40%"
	})
	$(".s-cat-heading h5").css({
		display: "none"
	})		
	$(".s-topic").css({
		padding: "18px 0 18px 40%"
	})	
	$(".s-cat").off("click",callFunction1);
	$(".cat-items").off("click",callFunction2);
}

$(".s-cat").on("click",callFunction1);
$(".cat-items").on("click",callFunction2);

/*$('.article-box-content>a>h5').each(function(){
	var val = $(this).text(); 
	val = val.substring(0,70)+'...'; 
	$(this).text(val)
});*/
if(($(window).width()>1200)){
	var fullBar = true;
	$(".side-menu").click(function(e){
		halfSidebar();
		$(".home-main").css({width: "90%"});				
		fullBar = false;
	})
	$(".home-sidebar").click(function(e){
		if(!fullBar){
			fullBar = true;
		}
		else{
			fullSidebar();
			$(".home-sidebar").css({width: "18%"});
			$(".home-main").css({width: "82%"});
			fullBar = false;
		}
	})
	// $(".home-sidebar").click(function(e){
	// 	if(fullBar){
	// 		halfSidebar();
	// 		$(".home-main").css({width: "90%"});				
	// 		fullBar = false;
	// 	}
	// 	else{
	// 		fullSidebar();
	// 		$(".home-sidebar").css({width: "18%"});
	// 		$(".home-main").css({width: "82%"});
	// 		fullBar = true;
	// 	}
	// })

}
if(($(window).width()>768) && ($(window).width()<1200)){
	var clickEnabled = false;
	$(".s-cat").off("click",callFunction1);
	$(".cat-items").off("click",callFunction2);

	$(".home-sidebar").click(function(e){
		e.preventDefault;
		if(!clickEnabled){
			fullSidebar();
			clickEnabled = true;
		}
		else{
			halfSidebar();
			clickEnabled = false;
		}
	})
}

if($(window).width()<768){
	var menuEnabled = false;
	$(".side-menu").click(function(e) {
		e.preventDefault();
		if(!menuEnabled){
			$(".home-sidebar").css({left:0, width: "100%"});
			menuEnabled = true;
		}
		else{
			$(".home-sidebar").css({left:-1000, width: "18%"});
			menuEnabled = false;
		}
	});
}
var likeEnabled = false;
$(".article-like").click(function(){
	if(!likeEnabled){
		$(this).find(".like-icon").css("background", "url('resources/images/like-filled-icon.png')")
		$(this).find("h6").css("color", "#489ad8")
		likeEnabled = true;
	}
	else{
		$(this).find(".like-icon").css("background", "url('resources/images/like-icon.png')")
		$(this).find("h6").css("color", "#2a332d")
		likeEnabled = false;
	}
})
var laterEnabled = false;
$(".article-later").click(function(){
	if(!laterEnabled){
		$(this).find(".later-icon").css("background", "url('resources/images/later-filled-icon.png')")
		$(this).find("h6").css("color", "#d8b848")
		laterEnabled = true;
	}
	else{
		$(this).find(".later-icon").css("background", "url('resources/images/later-icon.png')")
		$(this).find("h6").css("color", "#2a332d")
		laterEnabled = false;
	}
})
$("#signin, #user-signin").click(function(){
	$("#signin").addClass("active");
	$("#signup").removeClass("active");
	$("#signup-form").addClass("hide");
	$("#signin-form").removeClass("hide");
	$("#signup-social").addClass("hide");
	$("#signin-social").removeClass("hide");

})
$("#signup, #user-signup").click(function(){
	$("#signup").addClass("active");
	$("#signin").removeClass("active");
	$("#signup-form").removeClass("hide");
	$("#signin-form").addClass("hide");
	$("#signup-social").removeClass("hide");
	$("#signin-social").addClass("hide");
})