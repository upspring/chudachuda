<%@page import="org.springframework.web.servlet.i18n.SessionLocaleResolver" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.service.CountdownTimerService" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% ResourceBundle resource = ResourceBundle.getBundle("application");
    String url=resource.getString("url");
    String client_id=resource.getString("client_id");
    String redirect_uri=resource.getString("redirect_uri");
    String facebookId=resource.getString("facebookId");
    String JsonLimit=resource.getString("JsonLimit");

    String TotalNews=resource.getString("TotalNews");
    System.out.println("client"+client_id);
    System.out.println("client"+JsonLimit);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


    <%--<meta http-equiv="refresh" content="10">--%>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ChudaChuda.com">
    <meta name="keywords"
          content="ChudaChuda.com,chuda,chuda,tamil,news,positive,inspirational,wow,hot news,chudachuda news,thamizh,suda,suda suda,india, tamil nadu,kumbakonam,chennai,times,dinamalar,dinakaran,malaimalar">
    <title>சுடசுட.com - ChudaChuda.com</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/resources/css/style_1.css">
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta property="og:title" id="og-title" content="" />
    <meta property="og:description" id="og-desc" content="" />
    <meta property="og:url" id="og-url" content="">
    <meta property="og:image" id="og-image" content="" />
    <!-- <script type="text/javascript">
   var timer =from CountdownTimerService.initialTimerSeconds in jsp tag

         var clock;
         $(document).ready(function () {
             clock = $('.clock').FlipClock(timer, {
                 countdown: true,
                 clockFace: 'MinuteCounter',
                 autoStart: true,
                 callbacks: {
                     stop: function() {
                         window.location.reload();
                         clock.start();
                     }
                 }
             });
         });
     </script>-->
    <script src="https://accounts.google.com/gsi/client" async defer></script>

    <style>
        @font-face {
            font-family: 'Hind-Madurai';
            src: url('<%=request.getContextPath()%>/resources/fonts/new/HindMadurai.ttf');
        }
        @font-face {
            font-family: 'Noto-Sans-Tamil';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Noto_Sans_Tamil.ttf');
        }
        @font-face {
            font-family: 'Latha-Bold';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/LathaBold.ttf');
        }
        @font-face {
            font-family: 'Museo-Slab';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Museo_Slab.otf');
        }
        @font-face {
            font-family: 'Nambi';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Nambi.ttf');
        }
        @font-face {
            font-family: 'Mina';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Mina.ttf');
        }
        @font-face {
            font-family: 'Heena';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Heena.ttf');
        }
        @font-face {
            font-family: 'Kalaham';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Kalaham.ttf');
        }
        @font-face {
            font-family: 'Suman';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Suman.ttf');
        }
        @font-face {
            font-family: 'Alankaram';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Alankaram.ttf');
        }
        .cat-list a {
            color: #f0f3f6;
            font-family: "Lato";
            font-weight: 500;
            font-size: 16px;
            text-decoration: none;
        }
        .submit-post
        {
            padding: 20px 40px;

            text-align: center;
        }
        .android-download {
            margin-left: 10px;
            margin-top: -30px;
        }
        ios-download {
            margin-left: 0px;
        }
        .s-cat-heading{
            padding: 18px 0 18px 30px;
        }
        .s-cat-heading h5{
            margin: 0 10px 0 10px;
        }
        .s-topic{
            padding: 10px 0 5px 30px;
        }
        .s-topic h5{
            margin: 0px 10px 10px 10px;
        }
        .s-topic:first-child h5{
            margin: 0px 10px 10px 10px;
        }
        .s-topic:last-child h5{
            margin: 0px 10px 10px 10px;
        }

        .android-download-img {
            width: 200px;
        }
        .google-play-download {
            display: inline-block;
            padding-left: 10px;
            color: black;
        }
        .home-login{
            display: inline-block;
            margin: 30px 9% 20px 20px;
            float: right;
        }
        .cat-list li{
            padding: 15px 0 15px 32px;
            line-height: 11px;
        }
        .s-cat-box{
            padding: 15px 0px 10px 0px;
        }
        .s-cat-box-heading{
            padding: 10px 0px 18px 10px;
        }
        .news-paper-img{
            display: inline-block;
            height: 35px;
            width: 35px;
            border-radius: 50%;
            margin-right: 10px;
        }
        .h-user-pic{
            display: inline-block;
            vertical-align: middle;
            margin-right: 18px;
            border: 2px solid #f4f4f4;
            border-radius: 50%;
        }
        .h-user-pic img
        {
            border-radius: 50%;
            width: 35px;
            height: 35px;
        }
        .home-user{
            margin: 20px 30px 10px 10px;
        }
        .m-user-pic{
            border-radius: 50%;
            border: 2px solid #fff;
        }
        .mobile-user a {
            color: white;
            text-align: center;
        }
        .article-box:hover{
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
            transition: 0.3s linear;
        }
        .article-box img{
            height: 170px;
        }
        .articles-img{
            margin-bottom: -10px;
        }
        .article-box-content{
            padding: 0px 20px 15px 20px;
        }
        .article-box-content .article-box-title{
            overflow: hidden;
            text-overflow: ellipsis !important;
            -webkit-line-clamp: 4;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            white-space: normal;
            height: 97px;
            margin-bottom: 10px;
        }
        .article-box-content .article-box-title h4{
            font-family: 'Museo-Slab' !important;
            font-size: 17px;
            line-height: 22px;
        }
        .article-like svg {
            position: absolute;
            margin-top: 8px;
        }
        .article-like a{

        }
        .article-comment svg{
            position: absolute;
            margin-top: 8px;
        }
        .article-comment a {
            margin-left: 17px;
        }
        .article-later svg{
            position: absolute;
            margin-top: 10px !important;
        }
        .article-shadow{
            margin: 10px 0px 10px 0px;
        }
        .article-box-content h6{
            margin-top: 10px;
            margin-bottom: 15px;
            font-family: "Lato" !important;
        }
        .article-later a {
            margin-left: 17px;
        }
        .home-content{
            margin-left: 5%;
        }
        .h-manual-login-circle{
            border-radius: 50%;
            background-color: blue;
            height: 35px;
            width: 35px;
            vertical-align: middle;
            color: white !important;
            padding: 7px 0px 0px 13px;
            font-weight: bold;
            font-size: 16px;
            margin-right: 0px;
        }


        @media(min-width: 360px) and (max-width: 600px){
            .android-download-img{
                width:100px !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
            .like-count{
                margin-left: 12px !important;
            }
        }
        @media(min-width: 600px) and (max-width: 700px){
            .android-download-img{
                width:150px !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
            .like-count{
                margin-left: 12px !important;
            }
        }
        .like-count{
            position: absolute;
            margin-left: 5px;
            margin-top: 7px;
        }
        .dislike-count{
            position: absolute;
            margin-left: 2px;
            margin-top: 7px;
        }
        .loader {
            margin: 35px auto;
            font-size: 8px;
            width: 1em;
            height: 1em;
            border-radius: 50%;
            position: relative;
            text-indent: -9999em;
            -webkit-animation: load5 1.1s infinite ease;
            animation: load5 1.1s infinite ease;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        @-webkit-keyframes load5 {
            0%,
            100% {
                box-shadow: 0em -2.6em 0em 0em #5d4646, 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.5), -1.8em -1.8em 0 0em rgba(93,70,70, 0.7);
            }
            12.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.7), 1.8em -1.8em 0 0em #5d4646, 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.5);
            }
            25% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.5), 1.8em -1.8em 0 0em rgba(93,70,70, 0.7), 2.5em 0em 0 0em #5d4646, 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            37.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.5), 2.5em 0em 0 0em rgba(93,70,70, 0.7), 1.75em 1.75em 0 0em #5d4646, 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            50% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.5), 1.75em 1.75em 0 0em rgba(93,70,70, 0.7), 0em 2.5em 0 0em #5d4646, -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            62.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.5), 0em 2.5em 0 0em rgba(93,70,70, 0.7), -1.8em 1.8em 0 0em #5d4646, -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            75% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.5), -1.8em 1.8em 0 0em rgba(93,70,70, 0.7), -2.6em 0em 0 0em #5d4646, -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            87.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.5), -2.6em 0em 0 0em rgba(93,70,70, 0.7), -1.8em -1.8em 0 0em #5d4646;
            }
        }
        @keyframes load5 {
            0%,
            100% {
                box-shadow: 0em -2.6em 0em 0em #5d4646, 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.5), -1.8em -1.8em 0 0em rgba(93,70,70, 0.7);
            }
            12.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.7), 1.8em -1.8em 0 0em #5d4646, 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.5);
            }
            25% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.5), 1.8em -1.8em 0 0em rgba(93,70,70, 0.7), 2.5em 0em 0 0em #5d4646, 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            37.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.5), 2.5em 0em 0 0em rgba(93,70,70, 0.7), 1.75em 1.75em 0 0em #5d4646, 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            50% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.5), 1.75em 1.75em 0 0em rgba(93,70,70, 0.7), 0em 2.5em 0 0em #5d4646, -1.8em 1.8em 0 0em rgba(93,70,70, 0.2), -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            62.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.5), 0em 2.5em 0 0em rgba(93,70,70, 0.7), -1.8em 1.8em 0 0em #5d4646, -2.6em 0em 0 0em rgba(93,70,70, 0.2), -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            75% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.5), -1.8em 1.8em 0 0em rgba(93,70,70, 0.7), -2.6em 0em 0 0em #5d4646, -1.8em -1.8em 0 0em rgba(93,70,70, 0.2);
            }
            87.5% {
                box-shadow: 0em -2.6em 0em 0em rgba(93,70,70, 0.2), 1.8em -1.8em 0 0em rgba(93,70,70, 0.2), 2.5em 0em 0 0em rgba(93,70,70, 0.2), 1.75em 1.75em 0 0em rgba(93,70,70, 0.2), 0em 2.5em 0 0em rgba(93,70,70, 0.2), -1.8em 1.8em 0 0em rgba(93,70,70, 0.5), -2.6em 0em 0 0em rgba(93,70,70, 0.7), -1.8em -1.8em 0 0em #5d4646;
            }

        }
        .promo-container
        {
            display: none;
        }



    </style>
    <%--<link rel="stylesheet" href="../compiled/flipclock.css">--%>

    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>

    <%--<script src="../compiled/flipclock.js"></script>--%>
    <%--Specfic for tool tip--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--%>
    <%--   <style type="text/css">
          .fb_iframe_widget iframe {
             opacity: 0;
          }

          .fb_iframe_widget {
             background-image: url("<%=request.getContextPath()%>/resources/images/fb-login.png");
             background-repeat: no-repeat;
          } <script type="text/javascript">
                var clock;

                $(document).ready(function () {

                    clock = $('.clock').FlipClock(10, {
                        countdown: true,
                        clockFace: 'MinuteCounter'
                    });
                });
            </script>


       </style>--%>
    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/27dc580797c012bd9e9cde849449a638.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>
</head>
<body>
<%
    String tab = (String) request.getAttribute("tab");
    if (tab == null) tab = "current";
    if (request.getSession().getAttribute("userInfo") != null) {
        User user = (User) session.getAttribute("userInfo");
        String name=((User) session.getAttribute("userInfo")).getUserName();
        String email=((User) session.getAttribute("userInfo")).getEmail();

        System.out.println("jsp page"+ ((User) session.getAttribute("userInfo")).getUserName());
    }
%>
<%--
 <script type="text/javascript">
                var clock;

                $(document).ready(function () {

                    clock = $('.clock').FlipClock(10, {
                        countdown: true,
                        clockFace: 'MinuteCounter'
                    });
                });
            </script>
--%>

<div class="home-container">
    <div class="home-sidebar">
        <div class="logo"></div>
        <div class="left-arrow side-menu"></div>
        <div class="s-cat-box">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg width="20" height="20" style="margin-top: -13px; position: absolute;" stroke="#ffffff" fill="#ffffff" viewBox="0 0 36 36" version="1.1"  preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>new-line</title>
                                <path class="clr-i-outline clr-i-outline-path-1" d="M34.59,23l-4.08-5,4-4.9a1.82,1.82,0,0,0,.23-1.94A1.93,1.93,0,0,0,32.94,10h-31A1.91,1.91,0,0,0,0,11.88V24.13A1.91,1.91,0,0,0,1.94,26H33.05a1.93,1.93,0,0,0,1.77-1.09A1.82,1.82,0,0,0,34.59,23ZM2,24V12H32.78l-4.84,5.93L32.85,24Z"></path><polygon class="clr-i-outline clr-i-outline-path-2" points="9.39 19.35 6.13 15 5 15 5 21.18 6.13 21.18 6.13 16.84 9.39 21.18 10.51 21.18 10.51 15 9.39 15 9.39 19.35"></polygon><polygon class="clr-i-outline clr-i-outline-path-3" points="12.18 21.18 16.84 21.18 16.84 20.16 13.31 20.16 13.31 18.55 16.5 18.55 16.5 17.52 13.31 17.52 13.31 16.03 16.84 16.03 16.84 15 12.18 15 12.18 21.18"></polygon><polygon class="clr-i-outline clr-i-outline-path-4" points="24.52 19.43 23.06 15 21.84 15 20.37 19.43 19.05 15 17.82 15 19.78 21.18 20.89 21.18 22.45 16.59 24 21.18 25.13 21.18 27.08 15 25.85 15 24.52 19.43"></polygon>
                                <rect x="0" y="0" width="36" height="36" stroke="none" fill-opacity="0"/>
                            </svg>

                        </div>
                        <h5>முகப்பு</h5>
                    </div>
                </a>

                <a href="<%=request.getContextPath()%>/GetLinks?tab=trending" style="margin-top: 10px;">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("Trending")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg style="position: absolute; margin-top: -15px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.2 7.8l-7.7 7.7-4-4-5.7 5.7"/><path d="M15 7h6v6"/></svg>
                        </div>
                        <h5>டிரெண்டிங்</h5>
                    </div>
                </a>

                <% if (session.getAttribute("userInfo") != null) {
                    String save = (String) session.getAttribute("save");
                    save = (save == null ? "All" : save);
                %>


                <a href="<%=request.getContextPath()%>/Recommendation">
                    <div class='s-topic <%=(save.toLowerCase().contains("recommendation".toLowerCase())?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <img src="<%=request.getContextPath()%>/resources/images/recomendation.png" alt="recomendation-img" style="height: 22px; width: 22px; position: absolute; margin-top: -12px;" />
                        </div>
                        <h5>பரிந்துரைகள்</h5>
                        <span style="display: block; font-size: 11px; color: white; margin-left: 40px; margin-top: -5px; text-decoration: none">Experimental</span>
                    </div>
                </a>
                <%
                    }
                %>

                <!--
                <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="expired-icon"></span>
                        </div>
                        <h5>EXPIRED</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="mylinks-icon"></span>
                        </div>
                        <h5>Likes</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="later-filled-icon"></span>
                        </div>
                        <h5>Bookmark</h5>
                    </div>
                </a>-->
            </div>
        </div>

        <div class="s-cat" style="margin-top: -40px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>ஊடகங்கள்</h5><a ><span class="down-icon"></span></a>
            </div>
            <% String cat = (String) session.getAttribute("category");
                System.out.println("Domain like"+cat);
                cat = (cat == null ? "All" : cat); %>
            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-domain">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinakaran">
                        <li <%=(cat.toLowerCase().contains("Dinakaran".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinakaran-rounded.png" alt="img-newspapers" class="news-paper-img">தினகரன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=hindu">
                        <li <%=(cat.toLowerCase().contains("Hindu".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/tamilthehindu.png" alt="img-newspapers" class="news-paper-img">தமிழ் தி இந்து</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamani">
                        <li <%=(cat.toLowerCase().contains("dinamani".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamani-1.jpeg" alt="img-newspapers" class="news-paper-img">தினமணி</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamalar">
                        <li <%=(cat.toLowerCase().contains("Dinamalar".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamalar.png" alt="img-newspapers" class="news-paper-img">தினமலர்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=indianexpress">
                        <li <%=(cat.toLowerCase().contains("indianexpress".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/indian-express.jpeg" alt="img-newspapers" class="news-paper-img">இந்தியன் எக்ஸ்பிரஸ்</li>
                    </a>

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=bbc">
                        <li <%=(cat.toLowerCase().contains("bbc".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/bbc-tamil.jpg" alt="img-newspapers" class="news-paper-img">பிபிசி தமிழ் </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=oneindia">
                        <li <%=(cat.toLowerCase().contains("oneindia".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/oneindia-tamil.png" alt="img-newspapers" class="news-paper-img">ஒன் இந்தியா தமிழ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=zeenews">
                        <li <%=(cat.toLowerCase().contains("zeenews".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/zeenews.jpeg" alt="img-newspapers" class="news-paper-img">ஜீ நியூஸ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=vikatan">
                        <li <%=(cat.toLowerCase().contains("vikatan".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/vikatan.png" alt="img-newspapers" class="news-paper-img">விகடன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=filmibeat">
                        <li <%=(cat.toLowerCase().contains("filmibeat".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/filmibeat.jpeg" alt="img-newspapers" class="news-paper-img">பில்மிபீட் தமிழ்</li>
                    </a>


                </ul>
            </div>
        </div>
        <div class="s-cat" style="margin-top: -15px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>மற்ற ஊடகங்கள் </h5><a ><span class="down-icon"></span></a>
            </div>

            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-subdomain">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=samayam">
                        <li <%=(cat.toLowerCase().contains("Samayam".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/samayam-rounded.png" alt="img-newspapers" class="news-paper-img">சமயம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=puthiyathalaimurai">
                        <li <%=(cat.toLowerCase().contains("puthiyathalaimurai".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/puthiyathalaimurai.jpg" alt="img-newspapers" class="news-paper-img">புதிய தலைமுறை</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=ibctamil">
                        <li <%=(cat.toLowerCase().contains("ibctamil".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/ibc-tamil.png" alt="img-newspapers" class="news-paper-img">ஐபிசி தமிழ் </li>
                    </a>

                </ul>
            </div>
        </div>

        <div class="s-cat" style="margin-top: -5px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                <h5>தொகுப்பு</h5><a ><span class="down-icon"></span></a>
            </div>
            <% String domain = (String) session.getAttribute("domain");
                System.out.println("category like------>"+domain);
                domain = (domain == null ? "All" : domain); %>
            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-main">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetNews?category=india">
                        <li <%=(domain.equalsIgnoreCase("india") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/india-2-darkblue.png" alt="img-newspapers" class="news-paper-img">இந்தியா</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=world">
                        <li <%=(domain.equalsIgnoreCase("world") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/world-orange.png" alt="img-newspapers" class="news-paper-img">உலகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=tamilnadu">
                        <li <%=(domain.equalsIgnoreCase("tamilnadu") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/tamilnadu-3-darkblue.png" alt="img-newspapers" class="news-paper-img">தமிழகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=arasiyal">
                        <li <%=(domain.equalsIgnoreCase("arasiyal") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/politics-2-yellow.png" alt="img-newspapers" class="news-paper-img">அரசியல்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=sports">
                        <li <%=(domain.equalsIgnoreCase("sports") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/sports-1-blue.png" alt="img-newspapers" class="news-paper-img">விளையாட்டு</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=cinema">
                        <li <%=(domain.equalsIgnoreCase("cinema") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/cinema-rounded-yellow.png" alt="img-newspapers" class="news-paper-img">சினிமா </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=general">
                        <li <%=(domain.equalsIgnoreCase("general") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/news-darkblue.png" alt="img-newspapers" class="news-paper-img">பொது செய்திகள்</li>
                    </a>

                </ul>
            </div>
        </div>
        <% if (session.getAttribute("userInfo") != null) {
            String save = (String) session.getAttribute("save");
            save = (save == null ? "All" : save);
        %>

        <div class="s-cat-box" style="margin-top: -45px">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/ReadLaterArticle?userId=<%=((User) session.getAttribute("userInfo")).getUserId()%>">
                    <div class='s-topic <%=(save.toLowerCase().contains("READ".toLowerCase())?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; margin-top: -27px;"  width="23" height="23" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>

                        </div>
                        <h5>சேமிக்கப்பட்ட செய்திகள்</h5>
                    </div>
                </a>
            </div>
        </div>
        <%
            }
        %>
        <!--  <div class="s-cat">
                <div class="s-cat-heading">
                    <span class="country-icon"></span>
                    <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <% String country = (String) session.getAttribute("country");
                    if (country == null) country = "all";%>
                <div class="cat-items hidden">
                    <ul class="cat-list">
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'>
                            <li <%=(country.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'>
                            <li <%=(country.equalsIgnoreCase("India") ? "class='cat-list-active'" : "")%>>INDIA</li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'>
                            <li <%=(country.equalsIgnoreCase("United States") ? "class='cat-list-active'" : "")%>>USA
                            </li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'>
                            <li <%=(country.equalsIgnoreCase("Australia") ? "class='cat-list-active'" : "")%>>
                                AUSTRALIA
                            </li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'>
                            <li <%=(country.equalsIgnoreCase("United Kingdom") ? "class='cat-list-active'" : "")%>>
                                UNITED KINGDOM
                            </li>
                        </a>
                    </ul>
                </div>
            </div>-->
        <!--  <div class="s-cat">
                <div class="s-cat-heading">
                    <span class="lang-icon"></span>
                    <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <% String language = (String) session.getAttribute("language");
                    if (language == null) language = "All";%>

                <div class="cat-items hidden">
                    <ul class="cat-list">
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en">
                            <li <%=(language.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en">
                            <li <%=(language.equalsIgnoreCase("English") ? "class='cat-list-active'" : "")%>>ENGLISH
                            </li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta">
                            <li <%=(language.equalsIgnoreCase("Tamil") ? "class='cat-list-active'" : "")%>>தமிழ்</li>
                        </a>
                        <%--<a href="#"><li>HINDI</li></a>--%>
                        <%--<a href="#"><li>TELUGU</li></a>--%>
                        <%--<a href="#"><li>MALAYALAM</li></a>--%>
                    </ul>
                </div>
            </div>-->

        <br>

        <!--  <div class="clock"></div>-->
    </div>
</div>
<div class="home-main" id="page-content-wrapper">
    <div class="mobile-header">
        <a href="#">
            <div class="mobile-menu side-menu"></div>
        </a>
        <a href="#">
            <div class="mobile-logo"></div>
        </a>

        <div class="mobile-user">
            <div class="m-user-pic">
            </div>
            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" drop="#false">
                <span class="m-user-name"><%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserName() : "")%></span>
                <span class="down-icon-grey"></span>
            </a>
            <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel" style="background-color: #2d302e">
                <%--<a href="#"><li>Edit Profile</li></a>
                  <a href="#"><li>Change Password</li></a>--%>
                <a href="<%=request.getContextPath()%>/SignOut">
                    <li>Logout</li>
                </a>
            </ul>
        </div>
        <div class="mobile-login hide">
            <p><a href="#" id="user-signin" data-toggle="modal" data-target="#myModal">SignIn</a></p>

            <p><a href="#" id="user-signup" data-toggle="modal" data-target="#myModal">SignUp</a></p>
        </div>
    </div>
    <div class="home-header">
        <h4 class="google-play-download">சுடசுட செயலியை பெற </h4>
        <a href="http://bit.ly/2R8xz1e" class="android-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/Google-Play-Store-Logo.png"></a>
        <a href="https://apple.co/2T5zqtC" class="ios-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/app-store.png"></a>
        <div class="input-group home-search">
            <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
            <span class="input-group-btn">
                  <button class="btn home-search-btn" type="button"></button>

               </span>
        </div>

        <% if (session.getAttribute("userInfo") == null) {%>
        <div class="home-login">
            <%--   <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                  <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>--%>
            <p><a href='<%=request.getContextPath()%>/SignIn#login' id="user-signin">LOGIN</a></p>

            <p><a href="<%=request.getContextPath()%>/SignIn#signup" id="user-signup">SIGNUP</a></p>
        </div>
        <%}%>

        <% if (session.getAttribute("userInfo") != null) {

        %>

        <div class="home-user dropdown">
            <div class="h-user-pic">
                <% if (!((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its google"); %>
                <img src="<%=((User) session.getAttribute("userInfo")).getPicture()%>" alt="User Picture">
                <%
                    }
                %>
                <% if (((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its cc"); %>
                <span class="h-manual-login-circle"><%=((User) session.getAttribute("userInfo")).getUserName().toUpperCase().charAt(0)%></span>
                <%
                    }
                %>
            </div>
            <a id="dLabel1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" drop="#false">
                <span id="uname" class="h-user-name"><%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserName() : "")%></span>
                <span class="down-icon-grey"></span>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel1" style="background-color: #2d302e">
                <a href="<%=request.getContextPath()%>/SignOut">
                    <li>Logout</li>
                </a>
            </ul>
        </div>
        <%}%>

        <div class="clearfix"></div>
    </div>

    <div id="status">

    </div>

    <div class="home-content">
        <br><br>
        <div class="home-articles" id="post-list">

            <%
                System.out.println("current url is-->"+pageContext.getRequest());
                List<Link> links = (List<Link>) request.getAttribute("links");
                if (links == null || links.size() == 0) { %>
            No articles found

            <%
                }
                Integer limit= Integer.parseInt(JsonLimit);

                for (int i = 0; links != null && i < limit; i++) {

                    Link link = links.get(i);
            %>
            <div class="article-box" id="article_<%=link.getLinkId()%>">
                <div class="articles-img">
                    <img src="<%=link.getImageUrl() %>" alt="Image" id="article_image_url_<%=link.getLinkId()%>" onclick="permalink(<%=link.getLinkId()%>)">
                </div>

                <div class="article-box-content">
                    <a style="cursor:pointer" onclick="permalink(<%=link.getLinkId()%>)" target="_blank" id="article_url_<%=link.getLinkId()%>" class="article-box-title"><h4 style="color: green" id="article_title_<%=link.getLinkId()%>"><%=link.getTitle()%>
                    </h4></a>
                    <h6>
                        By <%=link.getSubmitedBy() %> <%=(link.getHoursElapsed() <= 24 ? link.getHoursElapsed() + " hours ago " : link.getHoursElapsed() / 24 + " days ago ") %>
                    </h6>
                    <span style="display: none" id="article_desc_<%=link.getLinkId()%>"><%=link.getDesc()%></span>
                    <div>
                        <a style="font-size: 13px;" href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=<%=link.getDomain2()%>" id="article_domain_<%=link.getLinkId()%>"><%=link.getDomain()%>
                            <%

                                if (session.getAttribute("domainCategory") == null) {

                            %>
                            <a style="float: right; display:inline-block; font-size: 13px;" id="cat-name_<%=i%>" href="<%=request.getContextPath()%>/GetNews?category=<%=link.getCategory2()%>"><%=link.getCategory()%></a>
                            <%}%>
                            <% if (session.getAttribute("domainCategory") != null) {

                            %>
                            <a style="float: right;" href="<%=request.getContextPath()%>/GetCategoryForNews?category=<%=link.getCategory2()%>&domain=<%=link.getDomain2()%>"><%=link.getCategory()%></a>
                            <%}%>

                        </a>
                    </div>



                    <div class="article-shadow"></div>
                    <div class="article-action">
                        <div class="article-like" title="Like" style="margin-right: 30px;" data-placement="top" data-toggle="tooltip" >
                            <svg xmlns="http://www.w3.org/2000/svg"  onclick="like(<%=link.getLinkId()%>)" id=like_<%=link.getLinkId()%> width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
                            <a ><h6></h6></a>
                            <span class="like-count"><%=link.getVote_up()%></span>

                        </div>
                        <%
                            String t =link.getUserVoted();

                            if(t.equals("1"))
                            {
                        %>
                        <script>
                            var  idLink='<%=link.getLinkId()%>';
                            document.getElementById("like_"+idLink).setAttribute("stroke","#e67e22");
                        </script>
                        <%
                            }
                        %>
                        <div class="article-later" style="margin-right: 30px;" title="Dislike" data-toggle="tooltip" data-placement="top" >
                            <svg xmlns="http://www.w3.org/2000/svg"  onclick="dislike(<%=link.getLinkId()%>)" id="dislike_<%=link.getLinkId()%>" style="margin-top: 10px;" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>
                            <a ><h6></h6></a>
                            <span class="dislike-count"><%=link.getVote_down()%></span>

                        </div>
                        <%
                            String t1 =link.getUserVoted();

                            if(t1.equals("-1"))
                            {
                        %>
                        <script>
                            var  idLink='<%=link.getLinkId()%>';
                            document.getElementById("dislike_"+idLink).setAttribute("stroke","#e67e22");
                        </script>
                        <%
                            }
                        %>
                        <div class="article-comment" title="Share" data-toggle="tooltip" data-placement="top" id=article_share_<%=link.getLinkId()%> onclick="share(<%=link.getLinkId()%>)">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>
                            <a ><h6></h6></a>
                        </div>
                        <div class="article-comment" title="Read Later"  data-toggle="tooltip" data-placement="top"  onclick="save('<%=link.getLinkId()%>')">

                            <svg xmlns="http://www.w3.org/2000/svg"  id="save-later_<%=link.getLinkId()%>" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                            <a ><h6></h6></a>
                            <%
                                if(link.getBookmark()==1)
                                {

                            %>
                            <script>
                                var emailId='<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserId() : "")%>';
                                console.log("email is"+emailId);
                                var id = '<%=link.getLinkId()%>';
                                console.log("inside of id-->" + id);
                                if(emailId === "") {
                                    console.log("not login")
                                }
                                else
                                {
                                    console.log("user id"+<%=link.getUserId()%>);
                                    var userId='<%=link.getUserId()%>';
                                    if(emailId==userId) {
                                        console.log("correct uder bookmarked");

                                        //  document.getElementById("save-later_"+id).firstElementChild.setAttribute("fill","red");

                                        document.getElementById("save-later_" + id).style = "stroke: #e67e22;cursor:none";
                                    }

                                }
                                var href=window.location.href;

                                if(href.indexOf("ReadLaterArticle")!==-1)
                                {
                                    console.log("its category page");
                                    document.getElementById("save-later_"+id).style.display="none";
                                }
                            </script>
                            <%
                                }
                            %>
                        </div>
                        <!--<div class="article-comment" title="Map" data-toggle="tooltip" data-placement="top"   onclick="showMap(<%=link.getLinkId()%>)">
                            <svg style="margin-top: 3px;" fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="22px" height="22px"><path d="M 35.5 3 C 32.316385 3 29.491595 4.5852966 27.765625 7 L 11.5 7 C 8.4802259 7 6 9.4802259 6 12.5 L 6 37.5 C 6 40.519774 8.4802259 43 11.5 43 L 36.5 43 C 39.519774 43 42 40.519774 42 37.5 L 42 19.693359 C 43.52453 17.664135 45 15.506079 45 12.476562 C 45 7.2583961 40.726284 3 35.5 3 z M 35.5 6 C 39.109716 6 42 8.8847299 42 12.476562 C 42 14.69209 40.93429 16.141555 39.326172 18.265625 C 38.057178 19.941766 36.628032 22.186244 35.5 25 C 34.371968 22.186244 32.942822 19.941766 31.673828 18.265625 C 30.06571 16.141555 29 14.69209 29 12.476562 C 29 8.8847299 31.890284 6 35.5 6 z M 11.5 10 L 26.341797 10 C 26.125724 10.791015 26 11.619076 26 12.476562 C 26 14.991244 27.017951 16.904478 28.234375 18.644531 L 9.0351562 37.84375 C 9.0199745 37.730558 9 37.617889 9 37.5 L 9 12.5 C 9 11.101774 10.101774 10 11.5 10 z M 35.5 10 C 34.119 10 33 11.119 33 12.5 C 33 13.881 34.119 15 35.5 15 C 36.881 15 38 13.881 38 12.5 C 38 11.119 36.881 10 35.5 10 z M 16.515625 12 C 13.490048 12 11.015625 14.474423 11.015625 17.5 C 11.015625 20.525577 13.490048 23 16.515625 23 C 19.333765 23 21.667751 20.852007 21.980469 18.113281 A 1.0001 1.0001 0 0 0 20.986328 17 L 17.015625 17 A 1.0001 1.0001 0 1 0 17.015625 19 L 19.419922 19 C 18.835281 20.116815 17.865351 21 16.515625 21 C 14.571202 21 13.015625 19.444423 13.015625 17.5 C 13.015625 15.555577 14.571202 14 16.515625 14 C 17.355012 14 18.115 14.291164 18.716797 14.779297 A 1.0001 1.0001 0 1 0 19.976562 13.226562 C 19.032359 12.460695 17.822238 12 16.515625 12 z M 30.033203 21.087891 C 31.402376 22.933637 32.725514 24.943963 33.513672 27.962891 L 33.515625 27.962891 C 33.552145 28.102405 33.535375 28.288143 33.714844 28.767578 C 33.804574 29.007296 33.993117 29.366839 34.357422 29.636719 C 34.72168 29.906599 35.17174 30 35.5 30 C 35.828826 30 36.280435 29.90692 36.644531 29.636719 C 37.008628 29.366518 37.19556 29.005162 37.285156 28.765625 C 37.464349 28.286551 37.448068 28.10328 37.484375 27.964844 L 37.484375 27.962891 L 37.486328 27.962891 C 37.877609 26.464145 38.405158 25.220399 39 24.113281 L 39 37.5 C 39 37.617889 38.980025 37.730558 38.964844 37.84375 L 26.121094 25 L 30.033203 21.087891 z M 24 27.121094 L 36.84375 39.964844 C 36.730558 39.980025 36.617889 40 36.5 40 L 11.5 40 C 11.382111 40 11.269442 39.980025 11.15625 39.964844 L 24 27.121094 z"/></svg>
                            <a ><h6></h6></a>
                        </div>-->
                    </div>
                </div>
                <div class="box-shadow"></div>
            </div>
            <%} %>

        </div>
        <div class="ajax-loader text-center loader" id="loader">
            Loading...
        </div>
    </div>
</div>
</div>
<!-- Share Box -->
<div class="share-container" id="share-container">
    <div class="share-box">
        <div class="share-box-header">
            <h4>Share</h4>
            <span class="close-share-box" id="share-close" title="close"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
        </div>
        <div class="share-box-content">
            <div class="share-icon" title="Share on Email" id="share-email" onclick="email()"><i class="fa fa-envelope"></i></div>
            <div class="share-icon" id="share-whatsapp" title="Share on Whatsapp" onclick="shareWhatsapp()"><i class="fa fa-whatsapp"></i></div>
            <div class="share-icon" id="share-twitter" title="Share on Twitter" onclick="shareTwitter()"><i class="fa fa-twitter"></i></div>
            <div class="share-icon" id="share-facebook" title="Share on Facebook" onclick="shareFacebook()"><i class="fa fa-facebook"></i></div>
        </div>
    </div>
</div>
<!-- End of Share Box-->


<!-- Login Box -->
<div class="login-box-container" id="login-box">
    <div class="login-box">
        <div class="login-box-header">
            <h4>You are not Logged In Please</h4>
            <span class="close-login-box" title="close"><svg xmlns="http://www.w3.org/2000/svg" id="login-box-close" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
        </div>
        <div class="login-box-content">
            <a href="<%=request.getContextPath()%>/SignIn#login" id="login" target="_blank">
                <div class="login-btn">
                    <div><i class="fa fa-lock"></i></div>
                    <span class="login-btn-txt">Login</span>
                    <p>Already a member?</p>
                </div>
            </a>
            <a href="<%=request.getContextPath()%>/SignIn#signup" id="signup" target="_blank">
                <div class="signup-btn">
                    <div><i class="fa fa-user"></i></div>
                    <span class="signup-btn-txt">Signup</span>
                    <p>Create new account</p>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- End of Login Box-->

<!-- Email Share -->
<div class="email-share-container" id="email-share-container">
    <div class="email-share-box">
        <!--<div class="email-share-header">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
        </div>-->
        <div class="email-share-content">
            <form id="signin-form">
                <div class="input-group">
                    <span class="input-group-addon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                    </span>
                    <input type="email" class="form-control" placeholder="Email Id" id="email-share">
                </div>
                <div class="submit-btn">
                    <button type="button" onclick="emailSubmit()" id="email-submit-btn">Submit</button>
                    <span class="email-notif" id="email-notif">Email Sent Successfully</span>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End of Email Share-->
<% if (session.getAttribute("userInfo") == null) {
    System.out.println("Showing Information PopUp..."+ session.getAttribute("isPopup"));
    Boolean popup = null;
    popup = (Boolean) session.getAttribute("isPopup");
    System.out.println("popup"+popup);
    if(popup!=null)
    {
        if(popup)
        {
            System.out.println("popup open"+popup);
%>
<div class="promo-container" id="promo-container" style="display:flex">
    <div class="promo-box" id="promo-panel">
        <div class="promo-box-content">
            <h2>ட்ரெண்டிங் மற்றும் பரிந்துரைக்கப்பட்ட செய்திகளை அறிய கீழே இருக்கும் Login/Signup பொத்தானை அழுத்தி  பதிவு செய்து உள்நுழையவும் .</h2>
        </div>
        <div class="promo__flaps">
            <div class="flap outer flap--left"></div>
            <a href="<%=request.getContextPath()%>/SignIn#login" target="_blank" class="flap flap__btn" href="#" id="login_promo">Login</a>
            <a href="<%=request.getContextPath()%>/SignIn#signup" target="_blank" class="flap flap__btn" href="#" id="signup_promo" style="border-left: 1px solid #f8f8f8">Signup</a>
            <div class="flap outer flap--right"></div>
        </div>
    </div>
</div>
<%
            }}}
%>
<!-- End of Promo Box-->

<%--<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>--%>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-61533941-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<!--
<script type="text/javascript">
    //<![CDATA[
    var sc_project = 9681219;
    var sc_invisible = 1;
    var sc_security = "9ea9e173";
    var scJsHost = (("https:" == document.location.protocol) ?
        "https://secure." : "http://www.");
    document.write("<sc" + "ript type='text/javascript' src='" +
        scJsHost +
        "statcounter.com/counter/counter_xhtml.js'></" + "script>");
    //]]>
</script>
<noscript>
    <div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
                                class="statcounter"><img class="statcounter"
                                                         src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                         alt="shopify analytics"/></a></div>
</noscript>-->
<!-- End of StatCounter Code for Default Guide -->
<script type="text/javascript">
    var infolinks_pid = 2467450;
    var infolinks_wsid = 0;
</script>
<script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script>
<script>
    var emailId='<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
    console.log("email is"+emailId);

    if(emailId === "") {
        console.log("its empty");
        // document.getElementById('add-topic').style="display:none";
        window.onload = function () {

            console.log("inside of onetap")
            google.accounts.id.initialize({
                client_id: '<%=client_id%>',
                callback: function (credentialResponse) {
                    var response = credentialResponse;
                    console.log(response.credential);

                    $.ajax({
                        type: 'POST',
                        url: '/OneTapLogin',

                        // contentType: 'application/JSON; charset=utf-8',
                        data: {credential: response.credential},
                        success: function(data) {
                            console.log("data"+data);
                            if(data=="signin")
                            {
                                window.location.href = "<%=url%>/error";
                            }
                            else {
                                window.location.href = "<%=url%>";
                            }
                        }
                    });
                }
            });
            google.accounts.id.prompt();
        };
    }
    else {
        // document.getElementById('add-topic').style="display:inline-block";
    }
</script>
<script>
    function save(linkId) {
        console.log("---inside of save"+linkId);

        var emailId='<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserId() : "")%>';
        console.log("email is"+emailId);
        if(emailId === "") {
            console.log("email is empty");
            document.getElementById('login-box').style="display: block";
        }
        else{
            $.ajax({
                type: 'GET',
                url: '/ReadLater?linkId='+linkId+'&emailId='+emailId,

                // contentType: 'application/JSON; charset=utf-8',

                success: function(data) {
                    console.log("data"+data);
                    // document.getElementById("save-later_"+linkId).style="cursor:none;background: url('<%=request.getContextPath()%>/resources/images/later-filled-icon.png'); height: 14px; width: 14px; background-repeat: no-repeat; display: inline-block";

                    document.getElementById("save-later_"+linkId).style="stroke: #e67e22;cursor:none";
                }
            });
        }

    }
    function permalink(linkId) {
        console.log("--->"+linkId);
        window.open("<%=url%>/News_Detail?id="+linkId)
        //window.open("<%=url%>/News_Detail?id="+linkId);

    }
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    var share_box_close = document.getElementById('share-close');
    //var article_share = document.getElementById('article-share');
    var share_email = document.getElementById('share-email');
    var email_submit_btn = document.getElementById('email-submit-btn');
    var email_share = document.getElementById('email-share');
    var share_container = document.getElementById('share-container');
    var signin_form = document.getElementById('signin-form');
    share_box_close.addEventListener('click', function(){
        document.getElementById('share-container').style="display:none";
    })
    email_submit_btn.addEventListener('click', function (e){
        e.stopPropagation();
    })
    /*article_share.addEventListener('click', function(){
     document.getElementById('share-container').style="display: block";
     })*/
    share_email.addEventListener('click', function(){
        document.getElementById('email-share-container').style="display: block";
        document.getElementById('share-container').style="display: none";
    })
    email_share.addEventListener('click', function (e){
        e.stopPropagation();
    })
    share_container.addEventListener('click', function(e){
        e.stopImmediatePropagation();
    })
    signin_form.addEventListener('click', function(e){
        e.stopImmediatePropagation();
    })

</script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : <%=facebookId%>,
            xfbml      : true,
            version    : 'v2.9'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function share(id, linkId){
        document.getElementById('share-container').style.display="block";
        var share_whatsapp = document.getElementById('share-whatsapp');
        var share_twit = document.getElementById('share-twitter');
        var share_facebook = document.getElementById('share-facebook');
        //var share_email = document.getElementById('share-email');
        var article_title = document.getElementById("article_title_"+id).innerHTML;
        var article_image_url = document.getElementById("article_image_url_"+id).src;
        var article_url = "<%=url%>/News_Detail?id="+id;
        var article_desc=document.getElementById("article_desc_"+id).innerHTML;
        //var article_url = "http://localhost:8080/News_Detail?id="+id;
        var article_domain=document.getElementById("article_domain_"+id).innerHTML;



        share_whatsapp.setAttribute('share-title', article_title);
        share_whatsapp.setAttribute('share-image-url', article_image_url);
        share_whatsapp.setAttribute('share-url', article_url);


        share_twit.setAttribute('share-title', article_title);
        share_twit.setAttribute('share-image-url', article_image_url);
        share_twit.setAttribute('share-url', article_url);

        share_facebook.setAttribute('share-title', article_title);
        share_facebook.setAttribute('share-image-url', article_image_url);
        share_facebook.setAttribute('share-url', article_url);
        share_facebook.setAttribute("share-id",id);
        share_facebook.setAttribute("share-desc",article_desc);


        share_email.setAttribute('share-title', article_title);
        share_email.setAttribute('share-image-url', article_image_url);
        share_email.setAttribute('share-url', article_url);
        share_email.setAttribute('share-domain',article_domain);
    }
    function shareWhatsapp(id){
        var title = document.getElementById('share-whatsapp').getAttribute('share-title');
        var image_url = document.getElementById('share-whatsapp').getAttribute('share-image-url');
        var url = document.getElementById('share-whatsapp').getAttribute('share-url');
        document.getElementById('og-title').setAttribute('content', title);
        document.getElementById('og-url').setAttribute('content', url);
        document.getElementById('og-image').setAttribute('content', image_url);
        var message = "சுடசுட(chudachuda.com) " + "%0a" + "%0a" + encodeURIComponent(title) + " - " + "%0a" + "%0a" +encodeURIComponent(url);
        //window.open('https://api.whatsapp.com/send?text=' + message);

        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            window.open('https://api.whatsapp.com/send?text=' + message);
        }
        else{
            window.open('https://web.whatsapp.com/send?text=' + message);
        }

        document.getElementById('share-container').style.display="none";
        const myTimeout = setTimeout(resetHeader, 10000);
        function resetHeader(){
            document.getElementById('og-desc').setAttribute('content', 'ChudaChuda Tamil News');
            document.getElementById('og-image').setAttribute('content', '<%=url%>/resources/images/logo0.png');
        }
    }
    function shareTwitter(){
        var title = document.getElementById('share-twitter').getAttribute('share-title');
        var image_url = document.getElementById('share-twitter').getAttribute('share-image-url');
        var url = document.getElementById('share-twitter').getAttribute('share-url');
        //var message_twit = encodeURIComponent(title)+encodeURIComponent(url)+encodeURIComponent(image_url);
        window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'\n&text='+'சுடசுட(chudachuda.com)'+'%0a'+encodeURIComponent(title), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
        document.getElementById('og-title').setAttribute('content', title);
        document.getElementById('og-url').setAttribute('content', url);
        document.getElementById('og-image').setAttribute('content', image_url);
        const myTimeout = setTimeout(resetHeader, 10000);
        function resetHeader(){
            document.getElementById('og-desc').setAttribute('content', 'ChudaChuda Tamil News');
            document.getElementById('og-image').setAttribute('content', '<%=url%>/resources/images/logo0.png');
        }
    }

    function shareFacebook(){
        var title = document.getElementById('share-facebook').getAttribute('share-title');
        var image_url = document.getElementById('share-facebook').getAttribute('share-image-url');
        var desc=document.getElementById('share-facebook').getAttribute('share-desc');

        var url =document.getElementById('share-facebook').getAttribute('share-url') ;
        console.log("desc"+desc);
        console.log("url"+url);
        document.getElementById('og-title').setAttribute('content', title);
        document.getElementById('og-url').setAttribute('content', url);
        document.getElementById('og-image').setAttribute('content', image_url);
        FB.init({
            appId      : <%=facebookId%>,
            xfbml      : true,
            version    : 'v2.9'
        });

        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            action_properties: JSON.stringify({
                object : {
                    'og:url':url,
                    'og:title': title,
                    'og:image':image_url,
                    'og.description':desc

                }
            })

        });


        const myTimeout = setTimeout(resetHeader, 10000);
        function resetHeader(){
            document.getElementById('og-desc').setAttribute('content', 'ChudaChuda Tamil News');
            document.getElementById('og-image').setAttribute('content', '<%=url%>/resources/images/logo0.png');
        }
    }

    function email(){
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);

        document.getElementById('email-share-container').style = "display: block";
        document.getElementById('share-container').style.display = "block";

    }
    function  emailSubmit()
    {
        console.log(document.getElementById("email-share").value);

        var emailId=document.getElementById("email-share").value;
        var title = document.getElementById('share-email').getAttribute('share-title');
        var image_url = document.getElementById('share-email').getAttribute('share-image-url');
        var url = document.getElementById('share-email').getAttribute('share-url');
        var domain=document.getElementById('share-email').getAttribute("share-domain");


        $.ajax({
            type: "GET",
            url: "/ShareEmail?emailId=" + emailId+"&title="+encodeURIComponent(title)+"&image_url="+encodeURIComponent(image_url)+"&url="+encodeURIComponent(url)+"&domain="+encodeURIComponent(domain),
            success: function (result) {
                document.getElementById("share-container").style.display="none";
                //document.getElementById('email-share-container').style = "display: none";
                document.getElementById('email-notif').style="display: block";
                const myTimeout = setTimeout(resetHeader, 6000);
                function resetHeader(){
                    document.getElementById('email-share-container').style="display: none";
                    document.getElementById('email-notif').style="display:none";
                    document.getElementById('email-share').value="";
                }
            }
        });

    }

    function  like(id) {
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);
        //  emailId="sathiyak@upspring.it"
        if(emailId=="")
        {
            document.getElementById('login-box').style="display: block";

        }
        else
        {
            document.getElementById("like_"+id).setAttribute("style","pointer-events:none")
            document.getElementById("dislike_"+id).setAttribute("style","pointer-events:none;")
            document.getElementById("article_share_"+id).setAttribute("style","margin-left:0px");

            $.ajax({
                type: "GET",
                url: "/LikeArticles?id=" + id + "&emailId=" + emailId,
                success: function (result) {

                    console.log(result);
                    console.log(result.vote_up);
                    console.log(result.vote_down);
                    document.getElementById("like_"+id).parentElement.lastElementChild.innerHTML=result.vote_up;
                    document.getElementById("dislike_"+id).parentElement.lastElementChild.innerHTML=result.vote_down;
                    //document.getElementById("dislike_"+id).style.marginLeft="30px";
                    document.getElementById("article_share_"+id).style.marginLeft="30px";
                    console.log("its set ah...")

                    var likeFill=document.getElementById("like_"+id).getAttribute("stroke");
                    var dislikeFill=document.getElementById("dislike_"+id).getAttribute("stroke");
                    console.log(likeFill);
                    if(likeFill=="#000000")
                    {
                        if(dislikeFill=="#e67e22")
                        {
                            document.getElementById("dislike_"+id). setAttribute("stroke","#000000");
                        }
                        document.getElementById("like_"+id). setAttribute("stroke","#e67e22");

                    }
                    if(likeFill=="#e67e22")
                    {
                        document.getElementById("like_"+id) .setAttribute("stroke","#000000");
                    }
                    document.getElementById("like_"+id).setAttribute("style","pointer-events:cursor");
                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor")

                    document.getElementById("article_share_"+id).setAttribute("style","margin-left:0px")

                }
            });

        }

    }

    function dislike(id)
    {
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);
        //emailId="sathiyak@upspring.it"
        if(emailId=="")
        {
            document.getElementById('login-box').style="display: block";
        }
        else
        {
            document.getElementById("like_"+id).setAttribute("style","pointer-events:none")
            document.getElementById("dislike_"+id).setAttribute("style","pointer-events:none;");
            document.getElementById("article_share_"+id).setAttribute("style","margin-left:0px")


            $.ajax({
                type: "GET",
                url: "/DisLikeArticles?id=" + id + "&emailId=" + emailId,
                success: function (result) {

                    console.log(result);
                    console.log(result.vote_up);
                    console.log(result.vote_down);
                    document.getElementById("like_"+id).parentElement.lastElementChild.innerHTML=result.vote_up;
                    document.getElementById("dislike_"+id).parentElement.lastElementChild.innerHTML=result.vote_down;


                    var likeFill=document.getElementById("like_"+id).getAttribute("stroke");
                    var dislikeFill=document.getElementById("dislike_"+id).getAttribute("stroke");
                    console.log(likeFill);
                    if(dislikeFill=="#000000")
                    {
                        if(likeFill=="#e67e22")
                        {
                            document.getElementById("like_"+id).setAttribute("stroke","#000000");
                        }
                        document.getElementById("dislike_"+id).setAttribute("stroke","#e67e22");

                    }
                    if(dislikeFill=="#e67e22")
                    {
                        document.getElementById("dislike_"+id).setAttribute("stroke","#000000");
                    }
                    document.getElementById("like_"+id).setAttribute("style","pointer-events:cursor")
                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor")

                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor;")

                    document.getElementById("article_share_"+id).setAttribute("style","margin-left:0px")


                }
            });

        }
    }
</script>

<script>
    var login_box = document.getElementById('login-box');
    var login_box_close = document.getElementById('login-box-close');
    login_box_close.addEventListener('click', function(){
        document.getElementById('login-box').style="display: none";
    })
</script>

<script>

    $('#dLabel1').click(function() {
        if ($(this).attr('drop') === '#false') {
            $(this).attr('drop', '#true');
        }
        else {
            $(this).attr('drop', '#false');
        }
        if($(this).attr('drop') === '#true'){
            $('.dropdown-menu').css('display', 'block');
        }
        else{
            $('.dropdown-menu').css('display', 'none');
        }
    });
    $('#dLabel').click(function() {
        if ($(this).attr('drop') === '#false') {
            $(this).attr('drop', '#true');
        }
        else {
            $(this).attr('drop', '#false');
        }
        if($(this).attr('drop') === '#true'){
            $('.dropdown-menu').css('display', 'block');
        }
        else{
            $('.dropdown-menu').css('display', 'none');
        }
    });
</script>
<script>

    $("body").on('click',function(){
        var promo = document.getElementById("promo-panel");
        $('#email-share-container').fadeOut(300);
        //promo.classList.remove("is--open");
        if(document.getElementById("promo-panel")) {
            promo.classList.add("is--open");
        }
        //document.getElementById('promo-container').style="display: none !important";
        if(document.getElementById('promo-container')) {
            document.getElementById('promo-container').style = "display: none !important";
        }

        $.ajax({
            url: '/isPopupActive',
            type: "get",

            success: function (data) {
                console.log("data"+data);

            }})
        var storage="opened";
        document.cookie="popup="+storage+";expires=Wed, 1 Dec 2030 12:00 UTC";

    });
</script>
<script type="text/javascript">
    var start=0;
    var end=<%=JsonLimit%>;
    var totalNews=<%=TotalNews%>;
    $(document).ready(function(){


        windowOnScroll();
        // }
    });
    function windowOnScroll() {
        $(window).scroll( function(e){
            console.log("scroll top position"+$(window).scrollTop());
            console.log('document height'+$(document).height());
            console.log('window height'+ $(window).height());

            if ($(window).scrollTop() == $(document).height() - $(window).height()){
                console.log('inside of scroll view desktop');
                //if($(".article-box").length < $("#total_count").val()) {

                start=end+1;
                console.log("updated start value"+start);
                end=start+30;

                //var lastId = $(".article-box:last").attr("id");
                console.log("last id is"+start);
                console.log("lend"+end);
                if(totalNews<end)
                {
                    console.log("all News are loaded");
                }
                else
                {
                    getMoreData(start,end);
                }

                // }
            }
        });
    }
    console.log("last id is"+start);
    console.log("lend"+end);
    function getMoreData(start,end) {
        $(window).unbind('scroll');
        console.log("start is"+start);
        console.log("lend"+end);
        var href=window.location.href;
        var category="";
        var domain="";
        if(href.indexOf("GetLinks?tab=trending")!==-1)
        {
            console.log("Trending Page");
            document.getElementById("loader").style.display="none";
            end='<%=request.getAttribute("count")%>'-1;
            $('.ajax-loader').hide()
        }

        if(href.indexOf("GetCategoryNewsForWeb?category")!==-1)
        {
            console.log("its new source")
            category='<%=request.getAttribute("category")%>';
            console.log("cat"+category);
        }
        if(href.indexOf("GetCategoryForNews?category")!==-1)
        {
            console.log("its new source and category")
            category='<%=request.getAttribute("category")%>';
            domain='<%=request.getAttribute("domain")%>';
            console.log("cat"+category);
        }
        if(href.indexOf("GetNews?category")!==-1)
        {
            console.log("its category");
            category='<%=request.getAttribute("category")%>';
            console.log("cat"+category);
        }
        console.log("total end -->"+end);
        console.log("category"+category);
        console.log("domain"+domain);
        $.ajax({
            url: '/InfiniteScrollAjax?start=' + start+"&end="+end+"&url="+href+"&category="+category+"&domain="+domain,
            type: "get",
            beforeSend: function ()
            {
                $('.ajax-loader').show();
            },
            success: function (data) {
                console.log("data"+data.length);
                if(data.length==0)
                {
                    $('.ajax-loader').hide();
                }
                setTimeout(function() {
                    $('.ajax-loader').hide();
                    if(data.length>0)
                    {
                        for (var i=0;i<data.length;i++)
                        {


                            var hours="";
                            if(data[i].hoursElapsed<=24)
                            {
                                hours=data[i].hoursElapsed+" hours ago ";
                            }
                            else
                            {

                                hours= Math.floor(data[i].hoursElapsed /24)+ " days ago ";
                            }
                            var html='<div class="article-box" id="article_'+data[i].linkId+'">'+
                                '<div class="articles-img">'+
                                '<img src="'+data[i].imageUrl+'" alt="Image" id="article_image_url_'+data[i].linkId+'" onclick="permalink('+data[i].linkId+')">'+
                                '</div>'+

                                '<div class="article-box-content">'+
                                '<a style="cursor:pointer" onclick="permalink('+data[i].linkId+')" target="_blank" id="article_url_'+data[i].linkId+'" class="article-box-title">' +
                                '<h4 style="color: green" id="article_title_'+data[i].linkId+'">'+data[i].title+''+
                                '</h4></a>'+
                                '<h6>By '+data[i].submitedBy +" "+hours+''+

                                '</h6>'+
                                '<span style="display: none" id="article_desc_'+data[i].linkId+'">'+data[i].desc+'</span>'+
                                '<div>'+
                                '<a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category='+data[i].domain2+'" id="article_domain_'+data[i].linkId+'">'+data[i].domain+''+
                                <%

                                    if (session.getAttribute("domainCategory") == null) {

                                %>
                                '<a style="float: right"  href="<%=request.getContextPath()%>/GetNews?category='+data[i].category2+'">'+data[i].category+'</a>'+
                                <%}%>
                                <% if (session.getAttribute("domainCategory") != null) {

                                %>
                                '<a style="float: right;" href="<%=request.getContextPath()%>/GetCategoryForNews?category='+data[i].category2+'&domain='+data[i].domain2+'">'+data[i].category+'</a>'+
                                <%}%>
                                ' </a>'+
                                '</div>'+

                                '<div class="article-shadow"></div>'+

                                '<div class="article-action">'+
                                '<div class="article-like" title="Like" style="margin-right: 30px;" data-placement="top" data-toggle="tooltip"  >'+
                                '<svg xmlns="http://www.w3.org/2000/svg" onclick="like('+data[i].linkId+')" id=like_'+data[i].linkId+' width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>'+
                                '<a ><h6></h6></a>'+
                                '<span class="like-count">'+data[i].vote_up+'</span>'+
                                '</div>'+

                                '<div class="article-later"  title="Dislike" style="margin-right: 30px;" data-toggle="tooltip" data-placement="top">'+
                                '<svg xmlns="http://www.w3.org/2000/svg"  onclick="dislike('+data[i].linkId+')" id="dislike_'+data[i].linkId+'" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>'+
                                '<a ><h6></h6></a>'+
                                '<span class="dislike-count">'+data[i].vote_down+'</span>'+

                                '</div>'+

                                '<div class="article-comment" title="Share" data-toggle="tooltip" data-placement="top" id=article_share_'+data[i].linkId+' onclick="share('+data[i].linkId+')">'+
                                '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>'+
                                '<a ><h6></h6></a>'+
                                '</div>'+
                                '<div class="article-comment" title="Read Later"  data-toggle="tooltip" data-placement="top"  onclick="save('+data[i].linkId+')">'+

                                '<svg xmlns="http://www.w3.org/2000/svg"  id="save-later_'+data[i].linkId+'" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>'+
                                '<a ><h6></h6></a>'+

                                '</div> '+
                                '</div>'+
                                '</div>'+
                                '<div class="box-shadow"></div>'+
                                '</div>';
                            $("#post-list").append(html);
                            var t =data[i].userVoted;

                            if(t==1)
                            {
                                document.getElementById("like_"+data[i].linkId).firstElementChild.setAttribute("stroke","#e67e22");

                            }
                            var t1 =data[i].userVoted;

                            if(t1==-1)
                            {
                                document.getElementById("dislike_"+data[i].linkId).firstElementChild.setAttribute("stroke","#e67e22");

                            }
                            if(data[i].bookmark==1)
                            {

                                var emailId='<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserId() : "")%>';

                                var id = data[i].linkId;
                                console.log("inside of id-->" + id);
                                if(emailId === "") {
                                    console.log("not login")
                                }
                                else
                                {

                                    var userId=data[i].userId;
                                    console.log("userId"+userId);
                                    if(emailId==userId) {
                                        console.log("correct uder bookmarked");

                                        //  document.getElementById("save-later_"+id).firstElementChild.setAttribute("fill","red");

                                        document.getElementById("save-later_" + id).style = "stroke: #e67e22;cursor:none";
                                    }

                                }
                                var href=window.location.href;

                                if(href.indexOf("ReadLaterArticle")!==-1)
                                {
                                    console.log("its category page");
                                    document.getElementById("save-later_"+id).style.display="none";
                                }
                            }
                        }
                    }
                    else
                    {
                        console.log("data not present")
                        document.getElementById("loader").style.display="none";
                    }

                    windowOnScroll();
                }, 1000);
            }
        });
    }
</script>
<script>




</script>
<script type="text/javascript">
    $(window).on('beforeunload',function(e) {
        e = e || window.event;
        var storage="closed";
        var href=window.location.href;
        if(href.indexOf("GetCategoryNewsForWeb")!==-1 || href.indexOf("GetNews?category")!==-1|| href.indexOf("GetCategoryForNews?category")!==-1 || href.indexOf("GetLinks?tab=trending")!==-1 || href.indexOf("ReadLaterArticle")!==-1)
        {
            var storage2="opened";
            document.cookie = "popup=" + storage2 + ";expires=Wed, 1 Dec 2030 12:00 UTC";
        }
        else {

            var localStorageTime = localStorage.getItem('popupStatus');
            if (localStorageTime != null && localStorageTime != undefined) {

                localStorage.setItem('popup', true);

                document.cookie = "popup=" + storage + ";expires=Wed, 1 Dec 2030 12:00 UTC";
            } else {

                localStorage.setItem('popup', true);
                document.cookie = "popup=" + storage + ";expires=Wed, 1 Dec 2030 12:00 UTC";
            }
        }
    })
</script>

<script>
    (function() {
        //var promo_container = document.getElementById("promo-container");
        var promo     = document.getElementById("promo-panel");
        var btns      = document.querySelectorAll(".flap__btn");
        var href=window.location.href;


        var init = function(){
            //promo.classList.add("is--open");
            if(document.getElementById("promo-panel")) {
                promo.classList.add("is--open");
            }
            const myTimeout = setTimeout(resetHeader, 20000);
            function resetHeader(){
                hidePanel();
            }
        }
        var hidePanel = function(){
            //promo.classList.remove("is--open");
            if(document.getElementById("promo-panel")) {
                promo.classList.remove("is--open");
            }
            //document.getElementById('promo-container').style="display: none !important";
            if(document.getElementById('promo-container')) {
                document.getElementById('promo-container').style = "display: none !important";
            }
            $.ajax({
                url: '/isPopupActive',
                type: "get",

                success: function (data) {
                    console.log("data"+data);

                }})
        }

        if(href.indexOf("GetCategoryNewsForWeb")!==-1 || href.indexOf("GetNews?category")!==-1|| href.indexOf("GetCategoryForNews?category")!==-1 || href.indexOf("GetLinks?tab=trending")!==-1 || href.indexOf("ReadLaterArticle")!==-1)
        {
            console.log("other page");
            var storage="opened";
            document.cookie="popup="+storage+";expires=Wed, 1 Dec 2030 12:00 UTC";

            //document.getElementById('promo-container').style="display: none !important";
            if(document.getElementById('promo-container')) {
                document.getElementById('promo-container').style = "display: flex !important";
            }

        }
        else
        {
            console.log("landing page..");
            var storage="closed";
            document.cookie="popup="+storage+";expires=Wed, 1 Dec 2030 12:00 UTC";


            init();
            //document.getElementById('promo-container').style="display: flex";
            if(document.getElementById('promo-container')) {
                document.getElementById('promo-container').style = "display: flex !important";
            }

        }
        window.onresize = displayWindowSize;
        window.onload = displayWindowSize;

        function displayWindowSize() {
            var myWidth = window.innerWidth;
            var myHeight = window.innerHeight;
// your size calculation code here
            if (myWidth <= 450) {

                windowOnScroll2();

            }
            else {

            }
        };




    })();

    function windowOnScroll2() {
        $(window).scroll( function(e){

            console.log("---->More Data......"+($(window).scrollTop() + $(window).height() > $(document).height() - 300))
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 300){

                //if($(".article-box").length < $("#total_count").val()) {

                start=end+1;

                end=start+32;

                //var lastId = $(".article-box:last").attr("id");
                console.log("last id is"+start);
                console.log("lend"+end);
                if(totalNews<end)
                {
                    console.log("all News are loaded");
                }
                else
                {
                    getMoreData(start,end);
                }

                // }
            }
            else
            {
                console.log("not statisfiled");
            }
        });
    }

</script>

<script>
    var url = window.location.href;
  //  var hash = url.substring(url.indexOf('#')+1);
    if(url.indexOf("GetCategoryNewsForWeb")!==-1){
        if(url.indexOf("samayam")!==-1 || url.indexOf("puthiyathalaimurai")!==-1|| url.indexOf("ibctamil")!==-1)
        {
            $('#cat-items-subdomain').removeClass('hidden');
            $('#cat-items-subdomain').addClass('show');
        }
        else {
            $('#cat-items-domain').removeClass('hidden');
            $('#cat-items-domain').addClass('show');
        }
    }
    if(url.indexOf("GetNews") !==-1 ){
        $('#cat-items-main').removeClass('hidden');
        $('#cat-items-main').addClass('show');
    }
    if(url.indexOf("GetCategoryForNews")!==-1)
    {
        $('#cat-items-domain').removeClass('hidden');
        $('#cat-items-domain').addClass('show');
        $('#cat-items-main').removeClass('hidden');
        $('#cat-items-main').addClass('show');
    }
    function showMap(linkId) {

        console.log("---inside of save" + linkId);
        window.open("/chudachudaMap?id=" + linkId);
    }

</script>

</body>
</html>
