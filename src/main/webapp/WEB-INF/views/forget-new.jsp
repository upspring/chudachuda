<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Chuda Chuda.com">
    <meta name="keywords" content="ChudaChuda.com,chuda,chuda,tamil,news,hot news,chuda chuda news,thamizh,suda,suda suda,india, tamil nadu,kumbakonam,chennai,times,dinamalar,dinakaran,malaimalar">
    <title>சுடசுட.com - ChudaChuda.com</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        /* Forgot Password Form */
        .cat-list li{
            padding: 15px 0 15px 50px;
            line-height: 11px;
        }
        .s-cat-box{
            padding: 15px 0px 10px 0px;
        }
        .s-cat-box-heading{
            padding: 10px 0px 18px 10px;
        }
        .news-paper-img{
            display: inline-block;
            height: 30px;
            width: 30px;
            border-radius: 50%;
            margin-right: 10px;
        }
        .h-user-pic{
            display: inline-block;
            vertical-align: middle;
            margin-right: 18px;
            border: 2px solid #666;
            border-radius: 50%;
        }
        .h-user-pic img
        {
            border-radius: 50%;
            width: 35px;
            height: 35px;
        }
        .home-user{
            margin: 20px 30px 10px 10px;
        }
        .m-user-pic{
            border-radius: 50%;
            border: 2px solid #fff;
        }
        .mobile-user a {
            color: white;
            text-align: center;
        }
        .article-box:hover{
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
            transition: 0.3s linear;
        }
        .article-box img{
            height: 170px;
        }
        .articles-img{
            margin-bottom: -10px;
        }
        .article-box-content{
            padding: 0px 20px 15px 20px;
        }
        .article-box-content .article-box-title{
            overflow: hidden;
            text-overflow: ellipsis !important;
            -webkit-line-clamp: 3;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            white-space: normal;
            height: 75px;
            margin-bottom: 10px;
        }
        .article-box-content .article-box-title h4{
            font-family: 'Noto-Sans-Tamil' !important;
            font-size: 16px;
            line-height: 22px;
        }
        .article-like svg {
            position: absolute;
            margin-top: 8px;
        }
        .article-like a{
            margin-left: 17px;
        }
        .article-comment svg{
            position: absolute;
            margin-top: 8px;
        }
        .article-comment a {
            margin-left: 17px;
        }
        .article-later svg{
            position: absolute;
            margin-top: 8px;
        }
        .article-shadow{
            margin: 10px 0px 10px 0px;
        }
        .article-box-content h6{
            margin-top: 10px;
            margin-bottom: 15px;
        }
        .article-later a {
            margin-left: 17px;
        }
        .home-content{
            margin-left: 5%;
        }
        .h-manual-login-circle{
            border-radius: 50%;
            background-color: blue;
            height: 35px;
            width: 35px;
            vertical-align: middle;
            color: white !important;
            padding: 7px 0px 0px 14px;
            font-weight: bold;
            font-size: 16px;
            margin-right: 0px;
        }


        @media(min-width: 360px) and (max-width: 600px){
            .android-download-img{
                width:100px !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
            .wrapper{
                width: 340px !important;
            }
            .login-box-icon{
                margin-left: 110px !important;
            }

        }
        @media(min-width: 600px) and (max-width: 700px){
            .android-download-img{
                width:150px !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
        }

        .input-group i{
            position: absolute;
            margin-left: -22px;
            z-index: 88888888;
            margin-top: 12px;
            color: #666;
            font-size: 14px;
        }
        .forgot-password-form {
            text-align: center;
        }
        .forgot-password-form form{
            width: 100%;
        }
        .forgot-password-form h4{
            font-size: 19px;
            letter-spacing: 1px;
        }
        .submitBtn{
            text-align: right;
        }
        .btn-submit{
            color: #fff;
            letter-spacing: 0.8px;
            padding: 5px 70px;
            border-radius: 2px;
            font-family: 'Lato';
            font-weight: bold;
            margin-bottom: 5px;
            margin-top: 10px;
        }
        .btn-submit:hover, .btn-submit:focus{
            color: #fff;
            outline: 0;
            box-shadow: none;
        }
        .submit-item-cus{
            width: 70%;
            margin-top: 3%;
            margin: 0 auto !important;
            min-height: 300px !important;
            padding: 25px !important;
            background-color: #ffffff !important;
        }

        /* Forget Box */

        .wrapper-container{
            display: grid;
            height: 100%;
            width: 100%;
            place-items: center;
            background: #f5f5f5;
            margin-top: 150px;
        }

        .wrapper{
            overflow: hidden;
            width: 390px;
            background: #fff;
            padding: 30px 30px 30px 30px;
            border-radius: 5px;
            box-shadow: 0px 15px 20px rgba(0,0,0,0.1);
        }
        .wrapper .title-text{
            display: flex;
            width: 200%;
        }
        .wrapper .title{
            width: 50%;
            font-size: 30px;
            font-weight: 600;
            text-align: center;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
        }
        .wrapper .slide-controls{
            position: relative;
            display: flex;
            height: 50px;
            width: 100%;
            overflow: hidden;
            margin: 30px 0 10px 0;
            justify-content: space-between;
            border: 1px solid lightgrey;
            border-radius: 5px;
        }
        .slide-controls .slide{
            height: 100%;
            width: 100%;
            color: #fff;
            font-size: 18px;
            font-weight: 500;
            text-align: center;
            line-height: 48px;
            cursor: pointer;
            z-index: 1;
            transition: all 0.6s ease;
        }
        .slide-controls label.signup{
            color: #000;
        }
        .slide-controls .slider-tab{
            position: absolute;
            height: 100%;
            width: 50%;
            left: 0;
            z-index: 0;
            border-radius: 5px;
            background: -webkit-linear-gradient(left, #ec9f05, #ff6f00);
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
        }
        input[type="radio"]{
            display: none;
        }
        #signup:checked ~ .slider-tab{
            left: 50%;
        }
        #signup:checked ~ label.signup{
            color: #fff;
            cursor: default;
            user-select: none;
        }
        #signup:checked ~ label.login{
            color: #000;
        }
        #login:checked ~ label.signup{
            color: #000;
        }
        #login:checked ~ label.login{
            cursor: default;
            user-select: none;
        }
        .wrapper .form-container{
            width: 100%;
            overflow: hidden;
        }
        .form-container .form-inner{
            display: flex;
            width: 200%;
        }
        .form-container .form-inner form{
            width: 50%;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
        }
        .form-inner form .field{
            height: 50px;
            width: 100%;
            margin-top: 20px;
        }
        .form-inner form .field input{
            height: 100%;
            width: 100%;
            outline: none;
            padding-left: 15px;
            border-radius: 5px;
            border: 1px solid lightgrey;
            border-bottom-width: 2px;
            font-size: 17px;
            transition: all 0.3s ease;
        }
        .form-inner-btn {
            background: -webkit-linear-gradient(right, #f7b42c, #ff6f00, #f7b42c, #ff6f00);
            border: none;
            width: 100%;
            height: 45px;
            border-radius: 5px;
            margin-top: 20px;
            color: white;
            font-size: 17px;
            transition: all 0.4s ease;
            font-weight: bold;
        }
        .form-inner-btn:hover{
            transition: all 0.4s ease;
        }
        .form-inner form .field input:focus{
            border-color: #e67e22;;
            /* box-shadow: inset 0 0 3px #fb6aae; */
        }
        .form-inner form .field input::placeholder{
            color: #999;
            transition: all 0.3s ease;
        }
        form .field input:focus::placeholder{
            color: #b3b3b3;
        }
        .form-inner form .pass-link{
            margin-top: 15px;
            display: inline-block;
            float: right;
        }
        .form-inner form .remember-pwd{
            display: inline-block;
            margin-top: 15px;
        }
        input[type="checked"]{
            background: red !important;
            color: white;
        }
        .form-inner form .signup-link{
            text-align: center;
            margin-top: 30px;
        }
        .form-inner form .pass-link a,
        .form-inner form .signup-link a{
            color: #e67e22;;
            text-decoration: none;
        }
        .form-inner form .pass-link a:hover,
        .form-inner form .signup-link a:hover{
            text-decoration: underline;
        }
        form .btn{
            height: 50px;
            width: 100%;
            border-radius: 5px;
            position: relative;
            overflow: hidden;
        }
        form .btn .btn-layer{
            height: 100%;
            width: 300%;
            position: absolute;
            left: -100%;
            background: -webkit-linear-gradient(right, #f7b42c, #ff6f00, #f7b42c, #ff6f00);
            border-radius: 5px;
            transition: all 0.4s ease;;
        }
        form .btn:hover .btn-layer{
            left: 0;
        }
        form .btn input[type="submit"]{
            height: 100%;
            width: 100%;
            z-index: 1;
            position: relative;
            background: none;
            border: none;
            color: #fff;
            padding-left: 0;
            border-radius: 5px;
            font-size: 20px;
            font-weight: 500;
            cursor: pointer;
        }
        .login-box-icon{
            height: 65px;
            width: 65px;
            position: absolute;
            border-radius: 50%;
            box-shadow: 0px 15px 10px rgba(0,0,0,0.06);
            margin-left: 130px;
            margin-top: -70px;
            background: -webkit-linear-gradient(left, #ec9f05, #ff6f00);
            padding: 21px 18px;
        }
        .login-box-icon .login-box-icon-login{
            display: inline-block;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
            visibility: visible;
            overflow: hidden;
        }
        .login-box-icon .login-box-icon-signup{
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
            visibility: hidden;
            overflow: hidden;
        }
        #login-visible {
            position: absolute;
            margin-top: 15px;
            margin-left: -25px;
            cursor: pointer;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
        }
        #signup-visible {
            position: absolute;
            margin-top: 15px;
            margin-left: -0px;
            cursor: pointer;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
            visibility: hidden;
        }
        #signup-visible-confirm {
            position: absolute;
            margin-top: 15px;
            margin-left: 0px;
            cursor: pointer;
            transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
            visibility: hidden;
        }
        .social-login{
            margin-top: 10px;
            margin-left: 40px;
        }
        .social-login .border-l{
            content:'';
            width: 100px;
            border-top: 1px solid #222;
            display: inline-block;
        }
        .social-login .border-r{
            content:'';
            width: 100px;
            border-top: 1px solid #222;
            display: inline-block;
        }
        .social-login .r-text{
            margin-left: 10px;
            margin-right: 10px;
            padding-top: 0px;

        }
        .social-login button{
            display: block;
        }
        .social-login button{
            border: 1px solid #ff6f00;
            border-radius: 5px;
            padding: 10px;
            width: 220px;
            background-color: white;
            cursor: pointer;
            margin-left: 20px;
            margin-top: 20px;
        }
        .social-login button span svg{
            position: absolute;
            margin-right: 10px;
            margin-top: 3px;
        }
        .social-login button .login-text{
            margin-left: 30px;
            font-size: 15px;
        }




        @media(min-width: 360px) and (max-width: 600px){
            .submit-item-cus{
                width: 90% !important;
                margin-top: 70px !important;
                margin-left: 20px !important;
            }
            .social-login-link{
                width: 220px !important;
                margin: 0 auto;
            }
            .social-login-icon{
                margin-left: -110px !important;
            }
            .social-login-text{
                margin-left: 0px !important;
            }
            .wrapper-container{
                margin-top: 70px !important;
            }
            .wrapper{
                width: 340px !important;
            }
            .login-box-icon{
                margin-left: 110px !important;
            }

        }
        @media(min-width: 600px) and (max-width: 700px){
            .submit-item-cus{
                width: 70% !important;
                margin-top: 100px !important;
                margin-left: 20px !important;
            }
            .social-login-link{
                width: 220px !important;
                margin: 0 auto;
            }
            .social-login-icon{
                margin-left: -110px !important;
            }
            .social-login-text{
                margin-left: 0px !important;
            }
            .wrapper-container{
                margin-top: 70px !important;
            }
            .wrapper{
                width: 400px !important;
            }
        }
        @media(min-width: 700px) and (max-width: 800px){
            .submit-item-cus{
                width: 70% !important;
                margin-top: 100px !important;
            }
            .social-login-link{
                width: 220px !important;
                margin: 0 auto;
            }
            .social-login-icon{
                margin-left: -110px !important;
            }
            .social-login-text{
                margin-left: 0px !important;
            }
            .wrapper{
                width: 400px !important;
            }
        }
        @media(min-width: 800px) and (max-width: 900px){
            .submit-item-cus{
                width: 60% !important;
            }
            .submit-item-login{
                width: 80% !important;
            }
            .social-login-link{
                width: 200px !important;
                margin-left: -25px !important;
            }
        }
        @media(min-width: 900px) and (max-width: 950px){
            .submit-item-cus{
                width: 55% !important;
            }
            .submit-item-login{
                width: 70% !important;
            }
            .social-login-link{
                width: 200px !important;
                margin-left: -25px !important;
            }
        }
        @media(min-width: 900px) and (max-width: 1100px){
            .submit-item-cus{
                width: 55% !important;
            }
            .submit-item-login{
                width: 80% !important;
            }
            .social-login-link{
                width: 200px !important;
                margin-left: -10px !important;
            }
        }
    </style>
</head>
<body>
<div class="home-container">
    <div class="home-sidebar">
        <div class="logo"></div>
        <div class="left-arrow side-menu"></div>
        <div class="s-cat-box">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
                    <div class="s-topic">
                        <div class="s-topic-icon">
                            <svg width="20" height="20" style="margin-top: -13px; position: absolute;" stroke="#ffffff" fill="#ffffff" viewBox="0 0 36 36" version="1.1"  preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>new-line</title>
                                <path class="clr-i-outline clr-i-outline-path-1" d="M34.59,23l-4.08-5,4-4.9a1.82,1.82,0,0,0,.23-1.94A1.93,1.93,0,0,0,32.94,10h-31A1.91,1.91,0,0,0,0,11.88V24.13A1.91,1.91,0,0,0,1.94,26H33.05a1.93,1.93,0,0,0,1.77-1.09A1.82,1.82,0,0,0,34.59,23ZM2,24V12H32.78l-4.84,5.93L32.85,24Z"></path><polygon class="clr-i-outline clr-i-outline-path-2" points="9.39 19.35 6.13 15 5 15 5 21.18 6.13 21.18 6.13 16.84 9.39 21.18 10.51 21.18 10.51 15 9.39 15 9.39 19.35"></polygon><polygon class="clr-i-outline clr-i-outline-path-3" points="12.18 21.18 16.84 21.18 16.84 20.16 13.31 20.16 13.31 18.55 16.5 18.55 16.5 17.52 13.31 17.52 13.31 16.03 16.84 16.03 16.84 15 12.18 15 12.18 21.18"></polygon><polygon class="clr-i-outline clr-i-outline-path-4" points="24.52 19.43 23.06 15 21.84 15 20.37 19.43 19.05 15 17.82 15 19.78 21.18 20.89 21.18 22.45 16.59 24 21.18 25.13 21.18 27.08 15 25.85 15 24.52 19.43"></polygon>
                                <rect x="0" y="0" width="36" height="36" stroke="none" fill-opacity="0"/>
                            </svg>
                        </div>
                        <h5>முகப்பு</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="s-cat" style="margin-top: -40px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>ஊடகங்கள்</h5><a ><span class="down-icon"></span></a>
            </div>
            <% String cat = (String) session.getAttribute("category");
                System.out.println("Domain like"+cat);
                cat = (cat == null ? "All" : cat); %>
            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-domain">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinakaran">
                        <li <%=(cat.toLowerCase().contains("Dinakaran".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinakaran-rounded.png" alt="img-newspapers" class="news-paper-img">தினகரன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=hindu">
                        <li <%=(cat.toLowerCase().contains("Hindu".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/tamilthehindu.png" alt="img-newspapers" class="news-paper-img">தமிழ் தி இந்து</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamani">
                        <li <%=(cat.toLowerCase().contains("dinamani".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamani-1.jpeg" alt="img-newspapers" class="news-paper-img">தினமணி</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamalar">
                        <li <%=(cat.toLowerCase().contains("Dinamalar".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamalar.png" alt="img-newspapers" class="news-paper-img">தினமலர்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=indianexpress">
                        <li <%=(cat.toLowerCase().contains("indianexpress".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/indian-express.jpeg" alt="img-newspapers" class="news-paper-img">இந்தியன் எக்ஸ்பிரஸ்</li>
                    </a>

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=bbc">
                        <li <%=(cat.toLowerCase().contains("bbc".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/bbc-tamil.jpg" alt="img-newspapers" class="news-paper-img">பிபிசி தமிழ் </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=oneindia">
                        <li <%=(cat.toLowerCase().contains("oneindia".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/oneindia-tamil.png" alt="img-newspapers" class="news-paper-img">ஒன் இந்தியா தமிழ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=zeenews">
                        <li <%=(cat.toLowerCase().contains("zeenews".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/zeenews.jpeg" alt="img-newspapers" class="news-paper-img">ஜீ நியூஸ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=vikatan">
                        <li <%=(cat.toLowerCase().contains("vikatan".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/vikatan.png" alt="img-newspapers" class="news-paper-img">விகடன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=filmibeat">
                        <li <%=(cat.toLowerCase().contains("filmibeat".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/filmibeat.jpeg" alt="img-newspapers" class="news-paper-img">பில்மிபீட் தமிழ்</li>
                    </a>


                </ul>
            </div>
        </div>
        <div class="s-cat" style="margin-top: -15px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>மற்ற ஊடகங்கள் </h5><a ><span class="down-icon"></span></a>
            </div>

            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-subdomain">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=samayam">
                        <li <%=(cat.toLowerCase().contains("Samayam".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/samayam-rounded.png" alt="img-newspapers" class="news-paper-img">சமயம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=puthiyathalaimurai">
                        <li <%=(cat.toLowerCase().contains("puthiyathalaimurai".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/puthiyathalaimurai.jpg" alt="img-newspapers" class="news-paper-img">புதிய தலைமுறை</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=ibctamil">
                        <li <%=(cat.toLowerCase().contains("ibctamil".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/ibc-tamil.png" alt="img-newspapers" class="news-paper-img">ஐபிசி தமிழ் </li>
                    </a>

                </ul>
            </div>
        </div>

        <div class="s-cat" style="margin-top: -5px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                <h5>தொகுப்பு</h5><a ><span class="down-icon"></span></a>
            </div>
            <% String domain = (String) session.getAttribute("domain");
                System.out.println("category like------>"+domain);
                domain = (domain == null ? "All" : domain); %>
            <div class="cat-items hidden" style="margin-top: -20px" id="cat-items-main">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetNews?category=india">
                        <li <%=(domain.equalsIgnoreCase("india") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/india-2-darkblue.png" alt="img-newspapers" class="news-paper-img">இந்தியா</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=world">
                        <li <%=(domain.equalsIgnoreCase("world") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/world-orange.png" alt="img-newspapers" class="news-paper-img">உலகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=tamilnadu">
                        <li <%=(domain.equalsIgnoreCase("tamilnadu") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/tamilnadu-3-darkblue.png" alt="img-newspapers" class="news-paper-img">தமிழகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=arasiyal">
                        <li <%=(domain.equalsIgnoreCase("arasiyal") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/politics-2-yellow.png" alt="img-newspapers" class="news-paper-img">அரசியல்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=sports">
                        <li <%=(domain.equalsIgnoreCase("sports") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/sports-1-blue.png" alt="img-newspapers" class="news-paper-img">விளையாட்டு</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=cinema">
                        <li <%=(domain.equalsIgnoreCase("cinema") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/cinema-rounded-yellow.png" alt="img-newspapers" class="news-paper-img">சினிமா </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=general">
                        <li <%=(domain.equalsIgnoreCase("general") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/news-darkblue.png" alt="img-newspapers" class="news-paper-img">பொது செய்திகள்</li>
                    </a>

                </ul>
            </div>
        </div>
    </div>
    <div class="home-main" id="page-content-wrapper">
        <div class="mobile-header">
            <a href="#"><div class="mobile-menu side-menu"></div></a>
            <a href="#">
                <div class="mobile-logo"></div>
            </a>
            <div class="mobile-user hide">
                <div class="m-user-pic">
                    <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
                </div>
                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="m-user-name">Hi Hari</span>
                    <span class="down-icon-grey"></span>
                </a>
                <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <a href="#"><li>Edit Profile</li></a>
                    <a href="#"><li>Change Password</li></a>
                    <a href="#"><li>Logout</li></a>
                </ul>
            </div>
            <div class="mobile-login hide">
                <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
            </div>
        </div>
        <div class="home-header signin-header">
            <div class="input-group home-search">
                <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
                <span class="input-group-btn">
                  <button class="btn home-search-btn" type="button"></button>
               </span>
            </div>
            <div class="home-login hide">
                <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
            </div>
            <div class="home-user dropdown hide">
                <div class="h-user-pic">
                    <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
                </div>
                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="h-user-name">Hi</span>
                    <span class="down-icon-grey"></span>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <a href="#"><li>Edit Profile</li></a>
                    <a href="#"><li>Change Password</li></a>
                    <a href="#"><li>Logout</li></a>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="wrapper-container">
            <div class="wrapper">
                <div class="login-box-icon">
                    <svg class="login-box-icon-login" id="login-box-icon-login" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                    <svg class="login-box-icon-signup" id="login-box-icon-signup" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                </div>
                <div class="title-text">
                    <div class="title login">Forgot Password</div>
                </div>
                <div class="form-container">
                    <div class="form-inner">
                        <form class="login" id="signin-form">
                            <div class="field">
                                <input type="text" placeholder="Email Address" id="email" required>
                            </div>
                            <span class="" style="display: none" id="loginEmail-text"></span>
                            <div class="field">
                                <input type="password" placeholder="Password" id="pass" required>
                                <svg onclick="myFunction()" id="login-visible" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#666666" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                            </div>
                            <div class="field">
                                <input type="password" placeholder="Confirm Password" id="confirm" required>
                                <svg onclick="myFunction2()" id="login-visible" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#666666" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                            </div>
                            <span class="" style="display: none" id="loginPassword-text"></span>
                            <span id="invalid" style="display: none"></span>
                            <!--<div class="field btn">
                                <div class="btn-layer"></div>
                                <input type="button" value="Submit" onclick="validatePassword()">
                                <button type="button" onclick="validatePassword()">Submit</button>
                            </div>-->
                            <button type="button" class="form-inner-btn" onclick="validatePassword()">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("#email").on('change keydown paste input', function(){
        document.getElementById('loginEmail-text').style.display="none";
        document.getElementById('invalid').style.display="none";

    });
    $("#pass").on('change keydown paste input', function(){
        document.getElementById('loginPassword-text').style.display="none";
    });

</script>
<script>

    function myFunction() {
        console.log("toggle")

        var x = document.getElementById("pass");
        console.log("toggle-->"+x)
        console.log("toggle-->"+x.type)
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    function myFunction2() {
        console.log("toggle")

        var x = document.getElementById("confirm");
        console.log("toggle-->"+x)
        console.log("toggle-->"+x.type)
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }



    function validatePassword()
    {
        var email=document.getElementById(("email")).value;
        var password=document.getElementById(("pass")).value;
        var confirmPassword=document.getElementById(("confirm")).value;

        //var enc_password=window.btoa(password);
        var passStatus=false;
        var  emailStauts=false;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var passformat=  /^(?=.*[a-z])(?!.*\s).{6,15}/;
        console.log("password"+password);
        console.log("confirm",confirmPassword);

        var email_message = document.getElementById('loginEmail-text');
        var password_valid = document.getElementById('loginPassword-text');
        if(confirmPassword===password&&password!==""&&confirmPassword!=="")
        {
            if(password.match(passformat))
            {
                passStatus=true;
            }
            else
            {
                password_valid.style = "display: block; color: #e67e22; float: right; margin-top: -10px; margin-left: 20px; margin-bottom: 10px;";
                password_valid.innerHTML = "Password between 6 to 15 characters ";

                passStatus=false;
            }
        }
        else
        {
            console.log("mismatching password");
            password_valid.style = "display: block; color: #e67e22; float: right; margin-top: 10px; margin-right: 05px;";
            password_valid.innerHTML = "Mismatching Password";

        }
        if(password==""&&confirmPassword==""){
            password_valid.style = "display: block; color: #e67e22; float: right; margin-top: 10px; margin-right: 05px;";
            password_valid.innerHTML = "Please Enter Password";
        }
        if(email==="") {
            console.log("email is empty");
            email_message.style = "display: block; color: #e67e22; float: right; margin-bottom: 3px; margin-right: 05px; padding-top: 5px;"
            email_message.innerHTML = "Please Enter Email Id";
            emailStauts=false;
        }
        else {
            emailStauts= true;
            if(email.match(mailformat)){
                emailStauts=true;
            }
            else {
                email_message.style = "display: block; color: #e67e22; float: right; margin-bottom: 3px; margin-right: 05px padding-top: 5px;"
                email_message.innerHTML = "Please Enter a valid Email Id";
                emailStauts=false;
            }

        }
        console.log("status-->",emailStauts,passStatus);
        if(emailStauts==true&&passStatus==true)
        {
            if(emailStauts){
                $.ajax({
                    type: "GET",
                    url: "/CheckUserExists?email=" + email+"&network=CC",
                    success: function (data) {
                        console.log("data", data);
                        console.log("data", data.email);

                        if (data.email==null) {
                            document.getElementById("invalid").style="display:block;color:red; margin-left: 20px;";
                            document.getElementById("invalid").innerHTML="Invalid Email Address"
                            //window.location.href = "https://localhost:8443/SignInSend?username=" + uname + "&email=" + email + "&password=" + password;
                        }
                        else {
                            console.log("correct email"+data.email);
                            $.ajax({
                                type: "GET",
                                url: "/UpdatePassword?email=" + email+"&password="+password,
                                success: function (data) {
                                    console.log("data", data);


                                    window.location.href="http://www.chudachuda.com/SignIn";
                                   // window.location.href = "http://http://100.25.102.248//SignIn";

                                }
                            })
                        }
                    }
                });
            }
        }

    }
</script>

<script src="<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>

</body>
</html>
