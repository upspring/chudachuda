<%--
  Created by IntelliJ IDEA.
  User: venkksastry
  Date: 18/05/15
  Time: 11:53 AM

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List,com.sudasuda.app.domain.Comment,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.vo.NameCountVO" %>
<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="ChudaChuda.com">
  <meta name="keywords" content="ChudaChuda.com">
  <title>சுடசுட.com - ChudaChuda.com</title>
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript">
    var timer = <%=CountdownTimerService.initialTimerSeconds%>;
    var clock;
    $(document).ready(function () {
      clock = $('.clock').FlipClock(timer, {
        countdown: true,
        clockFace: 'MinuteCounter',
        autoStart: true,
        callbacks: {
          stop: function() {
            window.location.reload();
            clock.start();
          }
        }
      });
    });
  </script>
</head>
<body>
<%String tab = (String) request.getAttribute("tab");
  if (tab==null) tab="current";
  Link link=null;
  User user=null;
  if ( request.getSession().getAttribute("userInfo") != null ) {  user = (User) session.getAttribute("userInfo"); }%>
<% if ( request.getAttribute("link") != null ) {  link = (Link) request.getAttribute("link"); } %>
<div class="home-container">
  <div class="home-sidebar">
    <div class="logo"></div>
    <div class="left-arrow side-menu"></div>
    <div class="s-cat-box">
      <div class="s-topics">
        <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
          <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="new-icon"></span>
            </div>
            <h5>NEW</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=current">
          <div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="trending-icon"></span>
            </div>
            <h5>TRENDING</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
          <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="expired-icon"></span>
            </div>
            <h5>EXPIRED</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="mylinks-icon"></span>
            </div>
            <h5>Likes</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="later-filled-icon"></span>
            </div>
            <h5>Bookmark</h5>
          </div>
        </a>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="cat-icon"></span>
          <h5>CATEGORIES</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String cat = (String)session.getAttribute("category");
          cat = (cat==null?"All":cat); %>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=All"><li <%=(cat.equalsIgnoreCase("all")?"class='cat-list-active'":"")%>>All</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Health"><li <%=(cat.equalsIgnoreCase("health")?"class='cat-list-active'":"")%>>HEALTH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Society"><li <%=(cat.equalsIgnoreCase("society")?"class='cat-list-active'":"")%>>SOCIETY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=World"><li <%=(cat.equalsIgnoreCase("world")?"class='cat-list-active'":"")%>>WORLD</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Politics"><li <%=(cat.equalsIgnoreCase("politics")?"class='cat-list-active'":"")%>>POLITICS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Sports"><li <%=(cat.equalsIgnoreCase("sports")?"class='cat-list-active'":"")%>>SPORTS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Spirituality"><li <%=(cat.equalsIgnoreCase("spirituality")?"class='cat-list-active'":"")%>>SPIRITUALITY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Recipie"><li <%=(cat.equalsIgnoreCase("recipie")?"class='cat-list-active'":"")%>>RECIPIE</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Technology"><li <%=(cat.equalsIgnoreCase("technology")?"class='cat-list-active'":"")%>>TECHNOLOGY</li></a>
          </ul>
        </div>
      </div>
      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="country-icon"></span>
          <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String country=(String) session.getAttribute("country");
          if ( country == null ) country="all";%>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'><li <%=(country.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'><li <%=(country.equalsIgnoreCase("India")?"class='cat-list-active'":"")%>>INDIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'><li <%=(country.equalsIgnoreCase("United States")?"class='cat-list-active'":"")%>>USA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'><li <%=(country.equalsIgnoreCase("Australia")?"class='cat-list-active'":"")%>>AUSTRALIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'><li <%=(country.equalsIgnoreCase("United Kingdom")?"class='cat-list-active'":"")%>>UNITED KINGDOM</li></a>
          </ul>
        </div>
      </div>
      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="lang-icon"></span>
          <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String language=(String) session.getAttribute("language");
          if ( language == null ) language="All";%>

        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en"><li <%=(language.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en"><li <%=(language.equalsIgnoreCase("English")?"class='cat-list-active'":"")%>>ENGLISH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta"><li <%=(language.equalsIgnoreCase("Tamil")?"class='cat-list-active'":"")%>>தமிழ்</li></a>
          </ul>
        </div>
      </div>
      <div class="clock"></div>
    </div>
  </div>
  <div class="home-main" id="page-content-wrapper">
    <div class="mobile-header">
      <a href="#"><div class="mobile-menu side-menu"></div></a>
      <a href="#">
        <div class="mobile-logo"></div>
      </a>
      <div class="mobile-user">
        <div class="m-user-pic">
          <%-- <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
          --%>
        </div>
        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="m-user-name"><%=(session.getAttribute("userInfo")!=null? ((User) session.getAttribute("userInfo")).getUserName():"")%></span>
          <span class="down-icon-grey"></span>
        </a>
        <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
          <%--<a href="#"><li>Edit Profile</li></a>
          <a href="#"><li>Change Password</li></a>--%>
          <a href="<%=request.getContextPath()%>/SignOut"><li>Logout</li></a>
        </ul>
      </div>
      <div class="mobile-login hide">
        <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">SignIn</a></p>
        <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SignUp</a></p>
      </div>
    </div>
    <div class="home-header">
      <div class="input-group home-search">
        <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
					<span class="input-group-btn">
						<button class="btn home-search-btn" type="button"></button>
					</span>
      </div>

      <% if ( session.getAttribute("userInfo") == null ) {%>
      <div class="home-login">
        <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
        <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
      </div>
      <%}%>

      <% if (session.getAttribute("userInfo") != null) { %>

      <div class="home-user dropdown">
        <div class="h-user-pic">
          <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
        </div>
        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="h-user-name"><%=(session.getAttribute("userInfo")!=null? ((User) session.getAttribute("userInfo")).getUserName():"")%></span>
          <span class="down-icon-grey"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <%-- <a href="#"><li>Edit Profile</li></a>
          <a href="#"><li>Change Password</li></a>
          --%>
          <a href="<%=request.getContextPath()%>/SignOut"><li>Logout</li></a>
        </ul>
      </div>
      <%}%>
      <div class="submit-post">
        <a href="<%=request.getContextPath()%>/AddLink"><span class="submit-text">Submit an item</span></a>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="comments-content">
      <%--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog signup-dialog">
          <div class="modal-content signup-content">
            <div class="modal-body signup-body">

              <div class="submit-content pad0">
                <div class="submit-item">
                  <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <div class="sign-tab">
                    <ul>
                      <li class="active" id="signin">SIGNIN</li>
                      <li id="signup">SIGNUP</li>
                    </ul>
                  </div>
                  <div class="sign-body">
                    <div class="login-left">
                      <form id="signin-form">
                        <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                          <input type="email" class="form-control" placeholder="Email Id">
                        </div>
                        <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                          <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="pwd-options">
                          <div class="input-group remember-pwd">
                            <label>
                              <input type="checkbox"> Remember Password
                            </label>
                          </div>
                          <div class="input-group fgt-pwd">
                            <a href="#"><span>Forgot Password?</span></a>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="signupBtn">
                          <button class="btn btn-signup">SIGN IN</button>
                        </div>
                      </form>
                      <form id="signup-form" class="hide">
                        <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                          <input type="email" class="form-control" placeholder="Email Id">
                        </div>
                        <div class="input-group">
														<span class="input-group-addon">
															<img src="images/fn-icon.png" alt="N">
														</span>
                          <input type="text" class="form-control" placeholder="User Name">
                        </div>
                        <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                          <input type="password" class="form-control" placeholder="Password">
                        </div>

                        <div class="signupBtn">
                          <button class="btn btn-signup">SIGN UP</button>
                        </div>
                      </form>
                    </div>
                    <div class="login-center hr-signin">
                      <hr>
                      <span>OR</span>
                    </div>
                    <div class="login-right">
                      <a href="">
                        <img src="images/fb-btn.png" alt="facebook">
                      </a>
                      <a href="">
                        <img src="images/gp-btn.png" alt="Google+">
                      </a>
                    </div>
                  </div>
                </div>
                <!-- <div class="submit-item-shadow"></div> -->
              </div>
            </div>
          </div>
        </div>
      </div>--%>
      <div class="comments-navigation">
       <%-- <ol class="breadcrumb comments-breadcrumb">
          <li><a href="#">Home</a></li>
          <li><a href="#">Sports</a></li>
        </ol>
        <a href="#"><span class="comments-back">&#60; Go Back</span></a>--%>
      </div>
      <div class="article-content">
        <div class="col-lg-4 col-sm-5"><img src="<%=link.getImageUrl()%>" height="180" width="310" /></div>
        <a href="<%=link.getUrl()%>"target="_blank"><h3><%= link.getTitle()%></h3><%--<span class="pop-out"></span>--%></a>
        <br><br><br><br><br><br>
        <%-- <a href="<%=link.getUrl()%>" title="Open original url"><span class="pop-out"></span></a> --%>
        <% if (link.getDesc() != null && link.getDesc().length() > 10 ) {%>
        <p><b>Description :</b>  <%=link.getDesc()%></p>
        <%}%>
      </div>
        <br><br>
      <div class="article-comments">
        <div class="comments-header">
          <h4>COMMENTS (<%=link.getNoOfComments()%>)</h4>
          <div class='dropdown comments-filter'>
       <%--     <select class="form-control comments-select">
              <option>Sort by:  <b>Latest</b></option>
              <option>Sort by: <b>Popular</b></option>
              <option>Sort by: <b>Likes</b></option>
            </select> --%>

            <div class="clearfix"></div>
          </div>
        </div>
        <div class="comments-body">
          <form action="<%=request.getContextPath()%>/AddComment?linkId=<%=request.getAttribute("linkId")%>" method="post">
            <!-- 							<div class="col-md-1 col-sm-2 col-xs-3 comments-input-area">
                                            <div class="comments-avatar">
                                                <img src="images/comments-avatar.png" alt="User Picture">
                                            </div>
                                        </div> -->
            <div class="col-xs-12 comments-input-text">
              <div class="form-group">
                <textarea  class="form-control" name="comment" placeholder="Type your comment here ..."row="3"></textarea>
                <div class="comment-submit">
                  <input type="submit" class="btn btn-comment-submit" value="Post">
                </div>
              </div>

            </div>
            <div class="clearfix"></div>
          </form>
<%
    List<Comment> comments = (List<Comment>) request.getAttribute("comments"); %>

    <div class="comments-list">

      <% for (int i = 0; comments != null &&  i < comments.size(); i++) {
        Comment comment = comments.get(i);
      %>
            <div class="comment-item">
              <!-- 								<div class="col-md-1 col-sm-2 col-xs-3  comments-user">
                                                  <div class="comments-avatar">
                                                      <a href="#"><img src="images/user-avatar.png" alt="User Picture"></a>
                                                  </div>
                                              </div> -->
              <div class="col-xs-12  comments-info">
                <a href="#"><h4><%=comment.getByUser()%></h4></a>
                <span class="comment-time"><%=comment.getDate_created()%></span>
                <p><%=comment.getText()%></p>
                <a href="#">
                  <span class='<%=(!comment.isVoted()?"like-filled-icon":"comment-like")%>' style="height: 14px; width: 15px;"></span>
                  <span class="comment-count"><%=comment.getVotes()%></span>
                </a>
              </div>
              <div class="clearfix"></div>
            </div>
      <%}%>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
  <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
  <script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>
  <script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#Tags").tagEditor(
              {
                items: ["First tag", "Second tag"],
                confirmRemoval: false
              });
    });
  </script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-61533941-1', 'auto');
    ga('send', 'pageview');

  </script>
  <!-- Start of StatCounter Code for Default Guide -->
  <script type="text/javascript">
    //<![CDATA[
    var sc_project=9681219;
    var sc_invisible=1;
    var sc_security="9ea9e173";
    var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
    document.write("<sc"+"ript type='text/javascript' src='" +
            scJsHost+
            "statcounter.com/counter/counter_xhtml.js'></"+"script>");
    //]]>
  </script>
  <noscript><div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
                                        class="statcounter"><img class="statcounter"
                                                                 src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                                 alt="shopify analytics" /></a></div></noscript>
  <!-- End of StatCounter Code for Default Guide -->
  <script type="text/javascript">
    var infolinks_pid = 2467450;
    var infolinks_wsid = 0;
  </script>
  <script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script>
</body>
</html>