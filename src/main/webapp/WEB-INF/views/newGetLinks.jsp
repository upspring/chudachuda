
<%@page import="org.springframework.web.servlet.i18n.SessionLocaleResolver" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.service.CountdownTimerService" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ChudaChuda.com">

    <title>News Cards - CSS only</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'>

    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/newlinks.css">
    <link rel="stylesheet" href="css/flipclock.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">

    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">

    <link href="https://pushcrew.com/http-v4/css/httpFront-v4.css?v=2" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="http://www.statcounter.com/counter/counter_xhtml.js"></script>
    <script src="http://resources.infolinks.com/js/infolinks_main.js"></script>
    <script src="https://cdn.pushcrew.com/js/27dc580797c012bd9e9cde849449a638.js"></script>
    <script src="http://resources.infolinks.com/js/1626.015-2.021.ab.1624.009-2.021/ice.js"></script>
    <script src="http://router.infolinks.com/gsd?evt=afterGSD&pid=2467450&wsid=0&pdom=chudachuda.com&purl=http%3A%2F%2Fchudachuda.com%2FGetLinks%3Ftab%3Dnew&jsv=1626.015-2.021.ab.1624.009-2.021&ref=chudachuda.com%2FGetLinks%3Bjsessionid%3D48FD270481468E4153DAF5EE9F620D9A&_cb=15372521256700"></script>
    <!--<script src="js/bootstrap.min.js"></script>-->



    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        var timer = 3600;
        var clock;
        $(document).ready(function () {
            clock = $('.clock').FlipClock(timer, {
                countdown: true,
                clockFace: 'MinuteCounter',
                autoStart: true,
                callbacks: {
                    stop: function() {
                        window.location.reload();
                        clock.start();
                    }
                }
            });
        });
    </script>




    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/27dc580797c012bd9e9cde849449a638.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>
    <style>
        .text-right {
            text-align: center;
        }


        .active{
            color:white;
            background-color:dodgerblue;
            outline:0;
        }


        .active:active{
            color:white;
            background-color:dodgerblue;
            outline:0;
        }
        .active:focus{
            color:white;
            background-color:dodgerblue;
            outline:0;
        }



    </style>

</head>

<body>


<div class="home-container">
    <div class="home-sidebar">
        <div class="logo"></div>
        <div class="left-arrow side-menu"></div>
        <div class="s-cat-box">
            <div class="s-topics">
                <a href="/GetLinks?tab=new">
                    <div class='s-topic s-topic-active'>
                        <div class="s-topic-icon">
                            <span class="new-icon"></span>
                        </div>
                        <h5>NEW</h5>
                    </div>
                </a>
                <a href="/GetLinks?tab=current">
                    <div class='s-topic '>
                        <div class="s-topic-icon">
                            <span class="trending-icon"></span>
                        </div>
                        <h5>TRENDING</h5>
                    </div>
                </a>
                <a href="/GetLinks?tab=expired">
                    <div class='s-topic '>
                        <div class="s-topic-icon">
                            <span class="expired-icon"></span>
                        </div>
                        <h5>EXPIRED</h5>
                    </div>
                </a>
                <a href="/GetLinks?tab=mylinks">
                    <div class='s-topic '>
                        <div class="s-topic-icon">
                            <span class="mylinks-icon"></span>
                        </div>
                        <h5>Likes</h5>
                    </div>
                </a>
                <a href="/GetLinks?tab=mybookmarks">
                    <div class='s-topic '>
                        <div class="s-topic-icon">
                            <span class="later-filled-icon"></span>
                        </div>
                        <h5>Bookmark</h5>
                    </div>
                </a>
            </div>
        </div>

        <div class="s-cat">
            <div class="s-cat-heading">
                <span class="cat-icon"></span>
                <h5>NEWS SOURCES</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String cat = (String) session.getAttribute("category");
                cat = (cat == null ? "All" : cat); %>
            <div class="cat-items hidden">
                <ul class="cat-list">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinakaran.com">
                        <li <%=(cat.equalsIgnoreCase("Dinakaran") ? "class='cat-list-active'" : "")%>>Dinakaran</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=tamil.thehindu.com">
                        <li <%=(cat.equalsIgnoreCase("Tamil TheHindu") ? "class='cat-list-active'" : "")%>>Tamil TheHindu</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=tamil.samayam.com">
                        <li <%=(cat.equalsIgnoreCase("Samayam") ? "class='cat-list-active'" : "")%>>Samayam</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=tamil.oneindia.com">
                        <li <%=(cat.equalsIgnoreCase("OneIndia") ? "class='cat-list-active'" : "")%>>OneIndia</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=tamil.filmibeat.com">
                        <li <%=(cat.equalsIgnoreCase("FilmiBeat") ? "class='cat-list-active'" : "")%>>FilmiBeat</li>
                    </a>

                </ul>
            </div>
        </div>
        <div class="s-cat">
            <div class="s-cat-heading">
                <span class="country-icon"></span>
                <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String country = (String) session.getAttribute("country");
                if (country == null) country = "all";%>
            <div class="cat-items hidden">
                <ul class="cat-list">
                    <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'>
                        <li <%=(country.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                    </a>
                    <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'>
                        <li <%=(country.equalsIgnoreCase("India") ? "class='cat-list-active'" : "")%>>INDIA</li>
                    </a>
                    <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'>
                        <li <%=(country.equalsIgnoreCase("United States") ? "class='cat-list-active'" : "")%>>USA
                        </li>
                    </a>
                    <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'>
                        <li <%=(country.equalsIgnoreCase("Australia") ? "class='cat-list-active'" : "")%>>
                            AUSTRALIA
                        </li>
                    </a>
                    <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'>
                        <li <%=(country.equalsIgnoreCase("United Kingdom") ? "class='cat-list-active'" : "")%>>
                            UNITED KINGDOM
                        </li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="s-cat">
            <div class="s-cat-heading">
                <span class="lang-icon"></span>
                <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String language = (String) session.getAttribute("language");
                if (language == null) language = "All";%>

            <div class="cat-items hidden">
                <ul class="cat-list">
                    <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en">
                        <li <%=(language.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en">
                        <li <%=(language.equalsIgnoreCase("English") ? "class='cat-list-active'" : "")%>>ENGLISH
                        </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta">
                        <li <%=(language.equalsIgnoreCase("Tamil") ? "class='cat-list-active'" : "")%>>தமிழ்</li>
                    </a>
                    <%--<a href="#"><li>HINDI</li></a>--%>
                    <%--<a href="#"><li>TELUGU</li></a>--%>
                    <%--<a href="#"><li>MALAYALAM</li></a>--%>
                </ul>
            </div>
        </div>
        <div class="s-cat">
            <div class="s-cat-heading">
                <span class="country-icon"></span>
                <h5>ANALYTICS</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <div class="cat-items hidden">
                <ul class="cat-list">
                    <a href='/Analytics?type=1'>
                        <li class='cat-list'>Source</li>
                    </a>
                    <a href='/Analytics'>
                        <li class='cat-list'>Category</li>
                    </a>
                </ul>
            </div>
        </div>
        <br>
        <div class="clock"></div>
    </div>
</div>
<div class="home-main" id="page-content-wrapper">
    <div class="mobile-header">
        <a href="#">
            <div class="mobile-menu side-menu"></div>
        </a>
        <a href="#">
            <div class="mobile-logo"></div>
        </a>

        <div class="mobile-user">
            <div class="m-user-pic">

            </div>
            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="m-user-name"></span>
                <span class="down-icon-grey"></span>
            </a>
            <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">

                <a href="/SignOut">
                    <li>Logout</li>
                </a>
            </ul>
        </div>
        <div class="mobile-login hide">
            <p><a href="#" id="user-signin" data-toggle="modal" data-target="#myModal">SignIn</a></p>

            <p><a href="#" id="user-signup" data-toggle="modal" data-target="#myModal">SignUp</a></p>
        </div>
    </div>
    <div class="home-header">
        <div class="input-group home-search">
            <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
            <span class="input-group-btn">
                      <button class="btn home-search-btn" type="button"></button>
                   </span>
        </div>


        <!--<div class="home-login">

            <p><a href='/SignIn' id="user-signin">LOGIN</a></p>

            <p><a href="/SignIn" id="user-signup">SIGNUP</a></p>
        </div>-->



        <div class="submit-post">
            <a href="/AddLink"><span class="submit-text">Submit an item</span></a>
        </div>
        <div class="clearfix"></div>
    </div>

    <div id="status">


    </div>
<!--
    <div class="fixed-btn">
        <button class="btn btn-size btn-circle">+</button>
    </div>-->
    <!--<div>
        <a class="  fixed-btn btn btn-size btn-circle" title="Add Item" href="#" data-toggle="modal" data-target="#AddItemModal" role="button">+</a></p>
    </div>-->
    <div class="home-content">

        <!-- Modal -->

        <div class=" home-articles">


            <%
                List<Link> links = (List<Link>) request.getAttribute("links");
                if (links == null || links.size() == 0) { %>
            No articles found

            <%
                }

                for (int i = 0; links != null && i < links.size(); i++) {

                    Link link = links.get(i);
            %>

            <div class="example-1 card article-box " id=<%=link.getLinkId()%>>
                <div class="wrapper " >

                    <img src="<%=link.getImageUrl()%>" >

                    <div class="data">
                        <div class="content">
                            <div class="callout-box">

                                <h4 class="title" ><a href="<%=link.getUrl()%>"><%=link.getTitle()%></a></h4><br>
                                <p><%=link.getDomain()%></p>

                                <div class="text callout bottom  scroll">
                                    <div class="spacing">
                                        <span class="fa-icon-size">
                                            <button title="Publish" class="btn new bind" id=bind_<%=link.getLinkId()%> onclick="publish('<%=link.getLinkId()%>')">
                                                <i class="fa fa-share"></i>
                                            </button><button title="Unpubilsh" class=" btn new unbind" id=unbind1_<%=link.getLinkId()%> onclick="unPublish('<%=link.getLinkId()%>')" disabled >
                                                <i class="fa fa-close"></i>
                                            </button>
                                            <button title="Edit" class="btn new" data-toggle="modal" onclick="editItem('<%=link.getLinkId()%>')">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button title ="Delete"class="btn new"  onclick="deleteItem('<%=link.getLinkId()%>')">
                                                <i class="fa fa-trash-o"></i></button>
                                            <span class="buttonactive">
                                                <button title ="Advertisement"class="btn">
                                                    <i class="fa fa-bullhorn"></i>
                                                </button>
                                            </span>
                                        </span>
                                    </div>
                                    <p class="padding-size"> <%=link.getDesc()%></p>

                                    <p><%=link.getDomain()%></p>
                                </div>
                            </div>
                            <p class="size">By ChudaChuda 0 hours ago</p>
                            <a href="feedproxy.google.com"><%=link.getDomain()%></a>
                        </div>

                    </div>
                </div>
            </div>


            <%
                if(link.getVotes()==1)
                {
                    System.out.println("inside jsp publish");
            %>
            <script language="javascript">

                var id='<%=link.getLinkId()%>';
                console.log("inside of like javascript"+id);

                document.getElementById("bind_"+id).style="pointer-events: none;color:grey";
                document.getElementById("unbind1_"+id).removeAttribute("disabled");

            </script>

            <%
                }
            %>

            <%
                }
            %>



            <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close sr-only-click" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only ">Close</span></button>
                            <h3 class="modal-title" id="lineModalLabel">EDIT ITEM</h3>
                        </div>
                        <div class="modal-body">

                            <!-- content goes here -->
                            <form>
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" id="title" >
                                </div>
                                <div class="form-group">
                                    <label for="url">URL</label>
                                    <input type="text" class="form-control" id="url">
                                </div>
                                <div class="form-group">
                                    <label for="imageUrl">Image URL</label>
                                    <input type="text" class="form-control" id="imageUrl">
                                </div>
                                <div class="form-group">
                                    <label for="desc">Description</label>
                                    <textarea type="text" class="form-control scroll" id="desc"></textarea>
                                </div>

                                <!-- <div class="checkbox">
                                   <label>
                                     <input type="checkbox"> Check me out
                                   </label>
                                 </div>-->

                            </form>

                            <div class="modal-footer ">
                                <div class="modal-footer-margin">
                                    <button type="button" class="btn btn-submit btn-secondary" id="itemSubmit">submit</button>
                                    <button type="button" class="btn btn-close btn-secondary" data-dismiss="modal" >close</button>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>





                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>





<script>
    $(document).ready(function(){

        $( ".bind" ).click(function() {

            var id = $(this).attr('id');


            $(this).attr("disabled", "disabled");
            $("#unbind1").removeAttr("disabled");

        });
        $( "#unbind1" ).click(function() {
            $(this).attr("disabled", "disabled");
            $("#bind1").removeAttr("disabled");
        });
    });

</script>




<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>

    function editItem(linkId)
    {
        $('document').ready(function() {
            $('#EditModal').modal('show');


            document.getElementById("title").value = document.getElementById(linkId).querySelector("h4").textContent;
            document.getElementById("url").value = document.getElementById(linkId).querySelector("h4").firstElementChild.getAttribute("href");
            document.getElementById("url").setAttribute("readonly", "true");
            document.getElementById("imageUrl").value = document.getElementById(linkId).querySelector("img").getAttribute("src");
            document.getElementById("desc").value = document.getElementById(linkId).querySelector("p[class='padding-size']").innerHTML;

            $("#itemSubmit").click(function () {


                $.ajax({
                    url: '/SaveEditLink',
                    data: {
                        linkId: linkId,
                        title: document.getElementById("title").value,
                        imageUrl: document.getElementById("imageUrl").value,
                        desc: document.getElementById("desc").value
                    },
                    type: 'GET',
                    success: function (success) {

                        console.log(success);
                        document.getElementById(linkId).querySelector("img").setAttribute("src", document.getElementById("imageUrl").value);
                        document.getElementById(linkId).querySelector("h4").firstElementChild.textContent = "";
                        document.getElementById(linkId).querySelector("h4").firstElementChild.textContent = document.getElementById("title").value;
                        document.getElementById(linkId).querySelector("p[class='padding-size']").textContent = document.getElementById("desc").value;
                        $("#EditModal").modal('hide');

                    }

                })


            });
        });

    }


    function deleteItem(linkId)
    {

        $.ajax({
            url:'/DeleteLink',
            data:{linkId:linkId},
            type:'GET',
            success: function (success) {
                console.log(success);
                document.getElementById(linkId).remove();

            }

        })




    }

</script>




<script>
    function publish(id) {
        console.log("inside of publish"+id);
        var value=1;
        $(this).attr("disabled", "disabled");
        document.getElementById("unbind1_"+id).removeAttribute("disabled");
        document.getElementById("unbind1_"+id).style="pointer-events:cursor";

        $.ajax({type:"GET",url: "/UpdatePublish?id="+id+"&value="+value, success: function(result){
            console.log("result"+result);

         //   $(this).attr("disabled", "disabled");
         //   document.getElementById("unbind1_"+id).removeAttribute("disabled");


        }});


    }
</script>
<script>
    function unPublish(id) {
        console.log("inise of unpublish",id);

        var value=0;
        $(this).attr("disabled", "disabled");

        document.getElementById("unbind1_"+id).style="pointer-events:none;color:grey";
        document.getElementById("bind_"+id).removeAttribute("disabled");

        $.ajax({type:"GET",url: "/UpdatePublish?id="+id+"&value="+value, success: function(result){
           // console.log("result"+result);





        }});

    }
</script>

<script>
    $('span.buttonactive button').click(function(){


        console.log("inside of active");
        if(!$(this).hasClass('active'))
            $(this).addClass('active');
    });
</script>





<!--
<script>
    $(document).ready(function(){
        $('.callout').hide();
        $(" .card").hover(function(){

            $(this).find('div').show();

        }, function(){
            $(this).find('.title').next().hide();
        });

    });
</script>-->
<!--
<script>
    $( document ).ready(function() {
        $('.mainr').click(function() {

            $('#btn1').css("background-color", "DodgerBlue");
            $('#btn1').css("color", "white");
        });
    });
</script>-->

<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>



<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-61533941-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
    //<![CDATA[
    var sc_project = 9681219;
    var sc_invisible = 1;
    var sc_security = "9ea9e173";
    var scJsHost = (("https:" == document.location.protocol) ?
        "https://secure." : "http://www.");
    document.write("<sc" + "ript type='text/javascript' src='" +
        scJsHost +
        "statcounter.com/counter/counter_xhtml.js'></" + "script>");
    //]]>
</script>
<noscript>
    <div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
                                class="statcounter"><img class="statcounter"
                                                         src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                         alt="shopify analytics"/></a></div>
</noscript>
<!-- End of StatCounter Code for Default Guide -->
<script type="text/javascript">
    var infolinks_pid = 2467450;
    var infolinks_wsid = 0;
</script>

<script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script>



</body>
</html>
