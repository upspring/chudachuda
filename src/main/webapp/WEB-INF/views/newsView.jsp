<%@page import="org.springframework.web.servlet.i18n.SessionLocaleResolver" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.service.CountdownTimerService" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% ResourceBundle resource = ResourceBundle.getBundle("application");
    String url=resource.getString("url");
    String client_id=resource.getString("client_id");
    String redirect_uri=resource.getString("redirect_uri");
    String facebookId=resource.getString("facebookId");
    System.out.println("client"+client_id);
    System.out.println("client"+url);
    System.out.println("facebook id"+facebookId);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


    <%--<meta http-equiv="refresh" content="10">--%>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ChudaChuda.com">
    <meta name="keywords"
          content="ChudaChuda.com,chuda,chuda,tamil,news,positive,inspirational,wow,hot news,chudachuda news,thamizh,suda,suda suda,india, tamil nadu,kumbakonam,chennai,times,dinamalar,dinakaran,malaimalar">
    <title>சுடசுட.com - ChudaChuda.com</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/resources/css/style_1.css">
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta property="og:title" name="title" id="og-title" content="<%=request.getAttribute("title")%>" />
    <meta property="og:description" name="description" id="og-desc" content="<%=request.getAttribute("description")%>" />
    <meta property="og:url" name="url" id="og-url" content="<%=url%>/News_Detail?id=<%=request.getAttribute("id")%>">
    <meta property="og:image" name="image" id="og-image" content="<%=request.getAttribute("imageUrl")%>" />

    <!-- <script type="text/javascript">
   var timer =from CountdownTimerService.initialTimerSeconds in jsp tag

         var clock;
         $(document).ready(function () {
             clock = $('.clock').FlipClock(timer, {
                 countdown: true,
                 clockFace: 'MinuteCounter',
                 autoStart: true,
                 callbacks: {
                     stop: function() {
                         window.location.reload();
                         clock.start();
                     }
                 }
             });
         });
     </script>-->
    <script src="https://accounts.google.com/gsi/client" async defer></script>

    <style>
        @font-face {
            font-family: 'Hind-Madurai';
            src: url('<%=request.getContextPath()%>/resources/fonts/new/HindMadurai.ttf');
        }
        @font-face {
            font-family: 'Noto-Sans-Tamil';
            src: url('.<%=request.getContextPath()%>/resources/fonts/new/Noto_Sans_Tamil.ttf');
        }
        .cat-list a {
            color: #f0f3f6;
            font-family: "Lato";
            font-weight: 500;
            font-size: 16px;
            text-decoration: none;
        }
        .submit-post
        {
            padding: 20px 40px;

            text-align: center;
        }
        .android-download {
            margin-left: 10px;
            margin-top: -30px;
        }
        ios-download {
            margin-left: 0px;
        }

        .s-cat-heading{
            padding: 18px 0 18px 30px;
        }
        .s-cat-heading h5{
            margin: 0 10px 0 10px;
        }
        .s-topic{
            padding: 10px 0 5px 30px;
        }
        .s-topic h5{
            margin: 0px 10px 10px 10px;
        }
        .s-topic:first-child h5{
            margin: 0px 10px 10px 10px;
        }
        .s-topic:last-child h5{
            margin: 0px 10px 10px 10px;
        }

        .android-download-img {
            width: 200px;
        }
        .google-play-download {
            display: inline-block;
            padding-left: 10px;
            color: black;
        }
        .home-login{
            display: inline-block;
            margin: 30px 9% 20px 20px;
            float: right;
        }
        .cat-list li{
            padding: 15px 0 15px 50px;
            line-height: 11px;
        }
        .s-cat-box{
            padding: 15px 0px 10px 0px;
        }
        .s-cat-box-heading{
            padding: 10px 0px 18px 10px;
        }
        .news-paper-img{
            display: inline-block;
            height: 35px;
            width: 35px;
            border-radius: 50%;
            margin-right: 10px;
        }
        .h-user-pic{
            display: inline-block;
            vertical-align: middle;
            margin-right: 18px;
            border: 2px solid #666;
            border-radius: 50%;
        }
        .h-user-pic img
        {
            border-radius: 50%;
            width: 35px;
            height: 35px;
        }
        .home-user{
            margin: 20px 30px 10px 10px;
        }
        .m-user-pic{
            border-radius: 50%;
            border: 2px solid #fff;
        }
        .mobile-user a {
            color: white;
            text-align: center;
        }
        .article-box:hover{
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
            transition: 0.3s linear;
        }
        .article-box img{
            height: 170px;
        }
        .articles-img{
            margin-bottom: -10px;
        }
        .article-box-content{
            padding: 0px 20px 15px 20px;
        }
        .article-box-content .article-box-title{
            overflow: hidden;
            text-overflow: ellipsis !important;
            -webkit-line-clamp: 3;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            white-space: normal;
            height: 75px;
            margin-bottom: 10px;
        }
        .article-box-content .article-box-title h4{
            font-family: 'Noto-Sans-Tamil' !important;
            font-size: 16px;
            line-height: 22px;
        }
        .article-like svg {
            position: absolute;
            margin-top: 8px;
        }
        .article-like a{
            margin-left: 17px;
        }
        .article-comment svg{
            position: absolute;
            margin-top: 8px;
        }
        .article-comment a {
            margin-left: 17px;
        }
        .article-later svg{
            position: absolute;
            margin-top: 8px;
        }
        .article-shadow{
            margin: 10px 0px 10px 0px;
        }
        .article-box-content h6{
            margin-top: 10px;
            margin-bottom: 15px;
        }
        .article-later a {
            margin-left: 17px;
        }
        .home-content{
            margin-left: 5%;
        }
        .h-manual-login-circle{
            border-radius: 50%;
            background-color: blue;
            height: 35px;
            width: 35px;
            vertical-align: middle;
            color: white !important;
            padding: 7px 0px 0px 11px;
            font-size: 16px;
            margin-right: 0px;
        }


        @media(min-width: 360px) and (max-width: 600px){
            .android-download-img{
                width:100px !important;
            }
            .permalink-box{
                width: 95% !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
            .permalink-box .permalink-box-header .social-icons{
                /*float: left !important;
                margin-left: 30% !important;*/
                margin-top: 10px !important;
            }
            .permalink-box .permalink-box-content img{
                height: 250px !important;
            }
            .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-category{
               position: absolute !important;
                margin-left: -192px !important;
                margin-top: 50px !important;
            }
            .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-cate{
                margin-top: 50px !important;
                margin-left: -157px !important;
            }

        }
        @media(min-width: 600px) and (max-width: 700px){
            .android-download-img{
                width:150px !important;
            }
            .permalink-box{
                width: 90% !important;
            }
            .article-box-content{
                padding: 0px 20px 20px 20px !important;
            }
            .m-user-pic{
                display:none !important;
            }
            .home-content {
                margin-left: 0px !important;
            }
            .permalink-box .permalink-box-header .social-icons{
                /*float: left !important;
                margin-left: 30% !important;*/
                margin-top: 10px !important;
            }
            .permalink-box .permalink-box-content img{
                height: 250px !important;
            }
            .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-category{
                position: absolute !important;
                margin-left: -192px !important;
                margin-top: 50px !important;
            }
            .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-cate{
                margin-top: 50px !important;
                margin-left: -157px !important;
            }
        }
        /* Permalink Page */
        .permalink-container{
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 30px 10px 15px 10px;
        }
        .permalink-box{
            width: 65%;
            padding: 30px 20px;
            background-color: white;
        }
        .permalink-box .permalink-box-header a h4{
            color: black;
            font-size: 30px;
            font-weight: 400;
            line-height: 37px;
        }
        .permalink-box .permalink-box-header a:hover{
            color: black;
            text-decoration: none !important;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon{
            margin-top: 30px;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news{
            background-color: #e7e7e7;
            border-radius: 50% !important;
            height: 35px !important;
            width: 35px !important;
            display: inline-block;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news svg{
            position: absolute;
            margin-top: 10px;
            margin-left: 8px;
            display: inline-block;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon  .icon-news-title{
            position: absolute;
            padding-left: 10px;
            padding-top: 7px;
            color:#444;
            font-size: 13px;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 130px;
            white-space: nowrap;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-date{
            background-color: #e7e7e7;
            border-radius: 50% !important;
            height: 35px !important;
            width: 35px !important;
            display: inline-block;
            margin-left: 120px;
        }

        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-date svg{
            position: absolute;
            margin-top: 9px;
            margin-left: 8px;
            display: inline-block;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon  .icon-news-time{
            position: absolute;
            padding-left: 10px;
            padding-top: 8px;
            color: #444;
            font-size: 13px;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-category{
            background-color: #e7e7e7;
            border-radius: 50% !important;
            height: 35px !important;
            width: 35px !important;
            display: inline-block;
            margin-left: 100px;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-category svg{
            position: absolute;
            margin-top: 10px;
            margin-left: 9px;
            display: inline-block;
        }
        .permalink-box .permalink-box-header .permalink-box-header-icon .icon-news-cate{
            position: absolute;
            padding-left: 10px;
            padding-top: 8px;
            color: #444;
            font-size: 13px;
        }
        .permalink-box .permalink-box-header .social-icons{
            float: right;
            display:inline-block;
            padding-top: 0px;
        }
        .permalink-box .permalink-box-header .social-icons .icon-email{
            background-color: #DB4437;
            height: 30px;
            width: 30px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 7px;
            cursor: pointer;
        }
        .permalink-box .permalink-box-header .social-icons .icon-email i{
            color: white;
            font-size: 15px;
            margin: 7px 8px;
        }
        .permalink-box .permalink-box-header .social-icons .icon-facebook{
            background-color: #4267B2;
            height: 30px;
            width: 30px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 7px;
            cursor: pointer;
        }
        .permalink-box .permalink-box-header .social-icons .icon-facebook i{
            color: white;
            font-size: 15px;
            margin: 8px 11px;
        }
        .permalink-box .permalink-box-header .social-icons .icon-twitter{
            background-color: #00acee;
            height: 31px;
            width: 31px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 7px;
            cursor: pointer;
        }
        .permalink-box .permalink-box-header .social-icons .icon-twitter i{
            color: white;
            font-size: 15px;
            margin: 9px 10px;
        }
        .permalink-box .permalink-box-header .social-icons .icon-whatsapp{
            background-color: #25D366;
            height: 32px;
            width: 32px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 7px;
            cursor: pointer;
        }
        .permalink-box .permalink-box-header .social-icons .icon-whatsapp i{
            color: white;
            font-size: 18px;
            margin: 8px 9px;
        }
        .permalink-box .permalink-box-content{
        }
        .permalink-box .permalink-box-content img{
            width: 100%;
            margin: 30px 0px;
            height: 400px;
        }
        .permalink-box .permalink-box-content p{
            font-size: 18px;
            line-height: 28px;
        }
        .permalink-box .permalink-box-content p span:first-child{
            padding-left: 5px;
        }
        .permalink-box .permalink-box-content p span:nth-child(2){
            margin-left:10px;
            color: blue;
            cursor: pointer;
            font-size: 15px;
        }
        .permalink-box .permalink-box-content p span:nth-child(2) a{
            text-decoration: none;
            color:#007ec7;
        }
        .permalink-box .permalink-box-content p span:nth-child(2) a:hover{
            color:#007ec7;
        }
        .email-notif{
            margin-left: 60px!important;
        }
        .permalink-box .permalink-footer{
            border-top: 1px solid #ddd;
            padding-top: 15px;
        }
        .permalink-box .permalink-footer .like{
            margin-right: 25px;
            display: inline-block;
        }
        .permalink-box .permalink-footer .like .footer-like-icon{
            border-radius: 50%;
            height: 35px;
            width: 36px;
            cursor: pointer;
            display: inline-block;
        }
        .permalink-box .permalink-footer .like .footer-like-icon:hover{
            background-color: #e67e22;
            transition: 0.4s linear;
        }
        .permalink-box .permalink-footer .like .footer-like-icon svg{
            margin: 9px 9px;
        }
        .permalink-box .permalink-footer .like .footer-like-icon:hover svg path{
            stroke: #ffffff;
        }
        .permalink-box .permalink-footer .like .footer-like-count{
            display: inline-block;
            position: absolute;
            margin-top: 8px;
            margin-left: 7px;
            font-size: 15px;
        }
        .permalink-box .permalink-footer .dislike{
            margin-right: 10px;
            display: inline-block;
            padding-top: 2px;
        }
        .permalink-box .permalink-footer .dislike .footer-like-icon{
            border-radius: 50%;
            height: 35px;
            width: 36px;
            cursor: pointer;
            display: inline-block;
        }
        .permalink-box .permalink-footer .dislike .footer-like-icon:hover{
            background-color: #e67e22;
            transition: 0.4s linear;
        }
        .permalink-box .permalink-footer .dislike .footer-like-icon svg{
            margin: 9px 9px;
        }
        .permalink-box .permalink-footer .dislike .footer-like-icon:hover svg path{
            stroke: #ffffff;
        }
        .permalink-box .permalink-footer .dislike .footer-like-count{
            display: inline-block;
            position: absolute;
            margin-top: 8px;
            margin-left: 7px;
            font-size: 15px;
        }
    </style>
    <%--<link rel="stylesheet" href="../compiled/flipclock.css">--%>

    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>

    <%--<script src="../compiled/flipclock.js"></script>--%>
    <%--Specfic for tool tip--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--%>
    <%--   <style type="text/css">
          .fb_iframe_widget iframe {
             opacity: 0;
          }

          .fb_iframe_widget {
             background-image: url("<%=request.getContextPath()%>/resources/images/fb-login.png");
             background-repeat: no-repeat;
          } <script type="text/javascript">
                var clock;

                $(document).ready(function () {

                    clock = $('.clock').FlipClock(10, {
                        countdown: true,
                        clockFace: 'MinuteCounter'
                    });
                });
            </script>


       </style>--%>
    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/27dc580797c012bd9e9cde849449a638.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>
</head>
<body>
<%
    String tab = (String) request.getAttribute("tab");
    if (tab == null) tab = "current";
    if (request.getSession().getAttribute("userInfo") != null) {
        User user = (User) session.getAttribute("userInfo");
        String name=((User) session.getAttribute("userInfo")).getUserName();
        String email=((User) session.getAttribute("userInfo")).getEmail();

        System.out.println("jsp page"+ ((User) session.getAttribute("userInfo")).getUserName());
    }
%>
<%--
 <script type="text/javascript">
                var clock;

                $(document).ready(function () {

                    clock = $('.clock').FlipClock(10, {
                        countdown: true,
                        clockFace: 'MinuteCounter'
                    });
                });
            </script>
--%>

<div class="home-container">
    <div class="home-sidebar">
        <div class="logo"></div>
        <div class="left-arrow side-menu"></div>
        <div class="s-cat-box">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg width="20" height="20" style="margin-top: -13px; position: absolute;" stroke="#ffffff" fill="#ffffff" viewBox="0 0 36 36" version="1.1"  preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>new-line</title>
                                <path class="clr-i-outline clr-i-outline-path-1" d="M34.59,23l-4.08-5,4-4.9a1.82,1.82,0,0,0,.23-1.94A1.93,1.93,0,0,0,32.94,10h-31A1.91,1.91,0,0,0,0,11.88V24.13A1.91,1.91,0,0,0,1.94,26H33.05a1.93,1.93,0,0,0,1.77-1.09A1.82,1.82,0,0,0,34.59,23ZM2,24V12H32.78l-4.84,5.93L32.85,24Z"></path><polygon class="clr-i-outline clr-i-outline-path-2" points="9.39 19.35 6.13 15 5 15 5 21.18 6.13 21.18 6.13 16.84 9.39 21.18 10.51 21.18 10.51 15 9.39 15 9.39 19.35"></polygon><polygon class="clr-i-outline clr-i-outline-path-3" points="12.18 21.18 16.84 21.18 16.84 20.16 13.31 20.16 13.31 18.55 16.5 18.55 16.5 17.52 13.31 17.52 13.31 16.03 16.84 16.03 16.84 15 12.18 15 12.18 21.18"></polygon><polygon class="clr-i-outline clr-i-outline-path-4" points="24.52 19.43 23.06 15 21.84 15 20.37 19.43 19.05 15 17.82 15 19.78 21.18 20.89 21.18 22.45 16.59 24 21.18 25.13 21.18 27.08 15 25.85 15 24.52 19.43"></polygon>
                                <rect x="0" y="0" width="36" height="36" stroke="none" fill-opacity="0"/>
                            </svg>

                        </div>
                        <h5>முகப்பு</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=trending" style="margin-top: 10px;">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("Trending")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg style="position: absolute; margin-top: -15px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.2 7.8l-7.7 7.7-4-4-5.7 5.7"/><path d="M15 7h6v6"/></svg>
                        </div>
                        <h5>டிரெண்டிங்</h5>
                    </div>
                </a>


                <% if (session.getAttribute("userInfo") != null) {
                    String save = (String) session.getAttribute("save");
                    save = (save == null ? "All" : save);
                %>


                <a href="<%=request.getContextPath()%>/Recommendation">
                    <div class='s-topic <%=(save.toLowerCase().contains("recommendation".toLowerCase())?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <img src="<%=request.getContextPath()%>/resources/images/recomendation.png" alt="recomendation-img" style="height: 22px; width: 22px; position: absolute; margin-top: -12px;" />
                        </div>
                        <h5>பரிந்துரைகள்</h5>
                        <span style="display: block; font-size: 11px; color: white; margin-left: 40px; margin-top: -5px; text-decoration: none">Experimental</span>
                    </div>
                </a>
                <%
                    }
                %>
                <!--  <a href="<%=request.getContextPath()%>/GetLinks?tab=current">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="trending-icon"></span>
                        </div>
                        <h5>TRENDING</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="expired-icon"></span>
                        </div>
                        <h5>EXPIRED</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="mylinks-icon"></span>
                        </div>
                        <h5>Likes</h5>
                    </div>
                </a>
                <a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="later-filled-icon"></span>
                        </div>
                        <h5>Bookmark</h5>
                    </div>
                </a>-->
            </div>
        </div>

        <div class="s-cat" style="margin-top: -40px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>ஊடகங்கள்</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String cat = (String) session.getAttribute("category");
                System.out.println("Domain like"+cat);
                cat = (cat == null ? "All" : cat); %>
            <div class="cat-items hidden" style="margin-top: -20px">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinakaran">
                        <li <%=(cat.toLowerCase().contains("Dinakaran".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinakaran-rounded.png" alt="img-newspapers" class="news-paper-img">தினகரன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=hindu">
                        <li <%=(cat.toLowerCase().contains("Hindu".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/tamilthehindu.png" alt="img-newspapers" class="news-paper-img">தமிழ் தி இந்து</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamani">
                        <li <%=(cat.toLowerCase().contains("dinamani".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamani-1.jpeg" alt="img-newspapers" class="news-paper-img">தினமணி</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamalar">
                        <li <%=(cat.toLowerCase().contains("Dinamalar".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamalar.png" alt="img-newspapers" class="news-paper-img">தினமலர்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=indianexpress">
                        <li <%=(cat.toLowerCase().contains("indianexpress".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/indian-express.jpeg" alt="img-newspapers" class="news-paper-img">இந்தியன் எக்ஸ்பிரஸ்</li>
                    </a>

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=bbc">
                        <li <%=(cat.toLowerCase().contains("bbc".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/bbc-tamil.jpg" alt="img-newspapers" class="news-paper-img">பிபிசி தமிழ் </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=oneindia">
                        <li <%=(cat.toLowerCase().contains("oneindia".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/oneindia-tamil.png" alt="img-newspapers" class="news-paper-img">ஒன் இந்தியா தமிழ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=zeenews">
                        <li <%=(cat.toLowerCase().contains("zeenews".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/zeenews.jpeg" alt="img-newspapers" class="news-paper-img">ஜீ நியூஸ்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=vikatan">
                        <li <%=(cat.toLowerCase().contains("vikatan".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/vikatan.png" alt="img-newspapers" class="news-paper-img">விகடன்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=filmibeat">
                        <li <%=(cat.toLowerCase().contains("filmibeat".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/filmibeat.jpeg" alt="img-newspapers" class="news-paper-img">பில்மிபீட் தமிழ்</li>
                    </a>


                </ul>
            </div>
        </div>
        <div class="s-cat" style="margin-top: -15px;">
            <div class="s-cat-heading">
                <svg  xmlns="http://www.w3.org/2000/svg" stroke-width="2" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="17" height="17" viewBox="0 0 256 256" xml:space="preserve">
                <desc>Created with Fabric.js 1.7.22</desc>
                    <defs>
                    </defs>
                    <g transform="translate(128 128) scale(0.72 0.72)" style="">
                        <g style="stroke: none; stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: none; fill-rule: nonzero; opacity: 1;" transform="translate(-175.05 -175.05000000000004) scale(3.89 3.89)" >
                            <path d="M 84.563 16.063 h -4.426 v -2.891 c 0 -2.634 -2.143 -4.776 -4.775 -4.776 H 71.53 v -3.62 C 71.53 2.143 69.388 0 66.754 0 H 5.437 C 2.803 0 0.661 2.143 0.661 4.776 v 73.637 C 0.661 84.802 5.859 90 12.248 90 h 67.187 c 5.461 0 9.904 -5.495 9.904 -12.249 V 20.84 C 89.339 18.206 87.196 16.063 84.563 16.063 z M 12.248 88 c -5.287 0 -9.587 -4.301 -9.587 -9.587 V 4.776 C 2.661 3.245 3.906 2 5.437 2 h 61.317 c 1.531 0 2.776 1.245 2.776 2.776 v 72.975 c 0 4.282 1.786 8.059 4.485 10.249 H 12.248 z M 87.339 77.751 c 0 5.651 -3.546 10.249 -7.904 10.249 s -7.904 -4.598 -7.904 -10.249 V 10.396 h 3.832 c 1.53 0 2.775 1.246 2.775 2.776 v 65.307 c 0 0.553 0.447 1 1 1 s 1 -0.447 1 -1 V 18.063 h 4.426 c 1.53 0 2.775 1.246 2.775 2.776 V 77.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 35.662 61.153 h -23.69 c -1.554 0 -2.818 -1.265 -2.818 -2.818 V 34.368 c 0 -1.554 1.264 -2.818 2.818 -2.818 h 23.69 c 1.554 0 2.818 1.264 2.818 2.818 v 23.967 C 38.48 59.889 37.216 61.153 35.662 61.153 z M 11.972 33.55 c -0.451 0 -0.818 0.367 -0.818 0.818 v 23.967 c 0 0.451 0.367 0.818 0.818 0.818 h 23.69 c 0.451 0 0.818 -0.367 0.818 -0.818 V 34.368 c 0 -0.451 -0.367 -0.818 -0.818 -0.818 H 11.972 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 33.55 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 33.55 61.756 33.55 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 42.751 H 46.347 c -0.552 0 -1 -0.448 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.448 1 1 S 62.309 42.751 61.756 42.751 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 51.952 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 51.952 61.756 51.952 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 61.153 H 46.347 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 15.409 c 0.553 0 1 0.447 1 1 S 62.309 61.153 61.756 61.153 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 70.354 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 70.354 61.756 70.354 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 61.756 79.556 H 10.154 c -0.552 0 -1 -0.447 -1 -1 s 0.448 -1 1 -1 h 51.602 c 0.553 0 1 0.447 1 1 S 62.309 79.556 61.756 79.556 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 19.513 24.604 c -0.311 0 -0.61 -0.145 -0.803 -0.404 l -7.557 -10.176 v 9.58 c 0 0.552 -0.448 1 -1 1 s -1 -0.448 -1 -1 V 11 c 0 -0.431 0.276 -0.814 0.686 -0.949 c 0.407 -0.134 0.859 0.007 1.117 0.353 l 7.557 10.176 V 11 c 0 -0.552 0.448 -1 1 -1 s 1 0.448 1 1 v 12.604 c 0 0.431 -0.276 0.814 -0.686 0.949 C 19.724 24.587 19.618 24.604 19.513 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 31.344 18.302 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 V 12 h 5.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -6.357 c -0.552 0 -1 0.448 -1 1 v 12.604 c 0 0.552 0.448 1 1 1 h 6.357 c 0.552 0 1 -0.448 1 -1 s -0.448 -1 -1 -1 h -5.357 v -4.302 H 31.344 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 38.981 24.604 c -0.015 0 -0.03 0 -0.045 -0.001 c -0.448 -0.02 -0.828 -0.335 -0.929 -0.772 l -2.939 -12.604 c -0.125 -0.538 0.208 -1.076 0.747 -1.201 c 0.535 -0.125 1.075 0.208 1.201 0.747 l 2.131 9.136 l 3.041 -9.222 c 0.135 -0.41 0.518 -0.687 0.95 -0.687 l 0 0 c 0.432 0 0.814 0.277 0.95 0.687 l 3.04 9.221 l 2.131 -9.135 c 0.125 -0.538 0.658 -0.873 1.201 -0.747 c 0.537 0.125 0.872 0.663 0.746 1.201 l -2.939 12.604 c -0.102 0.437 -0.481 0.752 -0.929 0.772 c -0.477 0.009 -0.854 -0.26 -0.995 -0.686 l -3.204 -9.723 l -3.206 9.723 C 39.794 24.329 39.411 24.604 38.981 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                            <path d="M 58.605 24.604 h -4.702 c -0.553 0 -1 -0.448 -1 -1 s 0.447 -1 1 -1 h 4.702 c 1.186 0 2.15 -0.965 2.15 -2.151 s -0.965 -2.151 -2.15 -2.151 h -1.552 c -2.288 0 -4.15 -1.862 -4.15 -4.151 S 54.766 10 57.054 10 h 3.632 c 0.553 0 1 0.448 1 1 s -0.447 1 -1 1 h -3.632 c -1.186 0 -2.15 0.965 -2.15 2.151 s 0.965 2.151 2.15 2.151 h 1.552 c 2.288 0 4.15 1.862 4.15 4.151 S 60.894 24.604 58.605 24.604 z" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round" />
                        </g>
                    </g>
                </svg>
                <h5>மற்ற ஊடகங்கள் </h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String cat1 = (String) session.getAttribute("category");
                System.out.println("Domain like"+cat1);
                cat1 = (cat1 == null ? "All" : cat1); %>
            <div class="cat-items hidden" style="margin-top: -20px">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=samayam">
                        <li <%=(cat.toLowerCase().contains("Samayam".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/samayam-rounded.png" alt="img-newspapers" class="news-paper-img">சமயம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=puthiyathalaimurai">
                        <li <%=(cat.toLowerCase().contains("puthiyathalaimurai".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/puthiyathalaimurai.jpg" alt="img-newspapers" class="news-paper-img">புதிய தலைமுறை</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=ibctamil">
                        <li <%=(cat.toLowerCase().contains("puthiyathalaimurai".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/ibc-tamil.png" alt="img-newspapers" class="news-paper-img">ஐபிசி தமிழ் </li>
                    </a>
                </ul>
            </div>
        </div>

        <div class="s-cat" style="margin-top: -5px;">
            <div class="s-cat-heading">
                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                <h5>தொகுப்பு</h5><a href="#"><span class="down-icon"></span></a>
            </div>
            <% String domain = (String) session.getAttribute("domain");
                System.out.println("category like------>"+domain);
                domain = (domain == null ? "All" : domain); %>
            <div class="cat-items hidden" style="margin-top: -20px">
                <ul class="cat-list" style="display: block;">

                    <a href="<%=request.getContextPath()%>/GetNews?category=india">
                        <li <%=(domain.equalsIgnoreCase("india") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/india-2-darkblue.png" alt="img-newspapers" class="news-paper-img">இந்தியா</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=world">
                        <li <%=(domain.equalsIgnoreCase("world") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/world-orange.png" alt="img-newspapers" class="news-paper-img">உலகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=tamilnadu">
                        <li <%=(domain.equalsIgnoreCase("tamilnadu") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/tamilnadu-3-darkblue.png" alt="img-newspapers" class="news-paper-img">தமிழகம்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=arasiyal">
                        <li <%=(domain.equalsIgnoreCase("arasiyal") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/politics-2-yellow.png" alt="img-newspapers" class="news-paper-img">அரசியல்</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=sports">
                        <li <%=(domain.equalsIgnoreCase("sports") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/sports-1-blue.png" alt="img-newspapers" class="news-paper-img">விளையாட்டு</li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=cinema">
                        <li <%=(domain.equalsIgnoreCase("cinema") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/cinema-rounded-yellow.png" alt="img-newspapers" class="news-paper-img">சினிமா </li>
                    </a>
                    <a href="<%=request.getContextPath()%>/GetNews?category=general">
                        <li <%=(domain.equalsIgnoreCase("general") ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/category/news-darkblue.png" alt="img-newspapers" class="news-paper-img">பொது செய்திகள்</li>
                    </a>

                </ul>
            </div>
        </div>
        <% if (session.getAttribute("userInfo") != null) {
            String save = (String) session.getAttribute("save");
            save = (save == null ? "All" : save);
        %>

        <div class="s-cat-box" style="margin-top: -45px">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/ReadLaterArticle?userId=<%=((User) session.getAttribute("userInfo")).getUserId()%>">
                    <div class='s-topic <%=(save.toLowerCase().contains("READ".toLowerCase())?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; margin-top: -27px;"  width="23" height="23" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>

                        </div>
                        <h5>சேமிக்கப்பட்ட செய்திகள்</h5>
                    </div>
                </a>
            </div>
        </div>
        <%
            }
        %>
        <!--  <div class="s-cat">
                <div class="s-cat-heading">
                    <span class="country-icon"></span>
                    <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <% String country = (String) session.getAttribute("country");
                    if (country == null) country = "all";%>
                <div class="cat-items hidden">
                    <ul class="cat-list">
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'>
                            <li <%=(country.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'>
                            <li <%=(country.equalsIgnoreCase("India") ? "class='cat-list-active'" : "")%>>INDIA</li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'>
                            <li <%=(country.equalsIgnoreCase("United States") ? "class='cat-list-active'" : "")%>>USA
                            </li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'>
                            <li <%=(country.equalsIgnoreCase("Australia") ? "class='cat-list-active'" : "")%>>
                                AUSTRALIA
                            </li>
                        </a>
                        <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'>
                            <li <%=(country.equalsIgnoreCase("United Kingdom") ? "class='cat-list-active'" : "")%>>
                                UNITED KINGDOM
                            </li>
                        </a>
                    </ul>
                </div>
            </div>-->
        <!--  <div class="s-cat">
                <div class="s-cat-heading">
                    <span class="lang-icon"></span>
                    <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <% String language = (String) session.getAttribute("language");
                    if (language == null) language = "All";%>

                <div class="cat-items hidden">
                    <ul class="cat-list">
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en">
                            <li <%=(language.equalsIgnoreCase("All") ? "class='cat-list-active'" : "")%>>ALL</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en">
                            <li <%=(language.equalsIgnoreCase("English") ? "class='cat-list-active'" : "")%>>ENGLISH
                            </li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta">
                            <li <%=(language.equalsIgnoreCase("Tamil") ? "class='cat-list-active'" : "")%>>தமிழ்</li>
                        </a>
                        <%--<a href="#"><li>HINDI</li></a>--%>
                        <%--<a href="#"><li>TELUGU</li></a>--%>
                        <%--<a href="#"><li>MALAYALAM</li></a>--%>
                    </ul>
                </div>
            </div>-->
        <!--  <div class="s-cat">
                <div class="s-cat-heading">
                    <span class="country-icon"></span>
                    <h5>ANALYTICS</h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <div class="cat-items hidden">
                    <ul class="cat-list">
                        <a href='<%=request.getContextPath()%>/Analytics?type=1'>
                            <li class='cat-list'>Source</li>
                        </a>
                        <a href='<%=request.getContextPath()%>/Analytics'>
                            <li class='cat-list'>Category</li>
                        </a>
                    </ul>
                </div>
            </div>-->
        <br>

        <!--  <div class="clock"></div>-->
    </div>
</div>
<div class="home-main" id="page-content-wrapper">
    <div class="mobile-header">
        <a href="#">
            <div class="mobile-menu side-menu"></div>
        </a>
        <a href="#">
            <div class="mobile-logo"></div>
        </a>

        <div class="mobile-user">
            <div class="m-user-pic">
                <%-- <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
                  --%>
            </div>
            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="m-user-name"><%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserName() : "")%></span>
                <span class="down-icon-grey"></span>
            </a>
            <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
                <%--<a href="#"><li>Edit Profile</li></a>
                  <a href="#"><li>Change Password</li></a>--%>
                <a href="<%=request.getContextPath()%>/SignOut">
                    <li>Logout</li>
                </a>
            </ul>
        </div>
        <div class="mobile-login hide">
            <p><a href="#" id="user-signin" data-toggle="modal" data-target="#myModal">SignIn</a></p>

            <p><a href="#" id="user-signup" data-toggle="modal" data-target="#myModal">SignUp</a></p>
        </div>
    </div>
    <div class="home-header">
        <h4 class="google-play-download">சுடசுட செயலியை பெற </h4>
        <a href="http://bit.ly/2R8xz1e" class="android-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/Google-Play-Store-Logo.png"></a>
        <a href="https://apple.co/2T5zqtC" class="ios-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/app-store.png"></a>
        <div class="input-group home-search">
            <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
            <span class="input-group-btn">
                  <button class="btn home-search-btn" type="button"></button>

               </span>
        </div>

        <% if (session.getAttribute("userInfo") == null) {%>
        <div class="home-login">
            <%--   <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                  <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>--%>
            <p><a href="<%=request.getContextPath()%>/SignIn#login" id="user-signin">LOGIN</a></p>

            <p><a href="<%=request.getContextPath()%>/SignIn#signup" id="user-signup">SIGNUP</a></p>
        </div>
        <%}%>

        <% if (session.getAttribute("userInfo") != null) {
            System.out.println("picture"+((User) session.getAttribute("userInfo")).getPicture());
        %>

        <div class="home-user dropdown">
            <div class="h-user-pic">
                <% if (!((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its google"); %>
                <img src="<%=((User) session.getAttribute("userInfo")).getPicture()%>" alt="User Picture">
                <%
                    }
                %>
                <% if (((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its cc"); %>
                <span class="h-manual-login-circle"><%=((User) session.getAttribute("userInfo")).getUserName().toUpperCase().charAt(0)%></span>
                <%
                    }
                %>
            </div>
            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span id="uname" class="h-user-name"><%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserName() : "")%></span>
                <span class="down-icon-grey"></span>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <%-- <a href="#"><li>Edit Profile</li></a>
                  <a href="#"><li>Change Password</li></a>
                  --%>
                <a href="<%=request.getContextPath()%>/SignOut">
                    <li>Logout</li>
                </a>
            </ul>
        </div>

        <!--    <div class="submit-post">
                <a href="/AddLink"><span class="submit-text">Submit an item</span></a>
            </div>-->
        <%}%>
        <!--      <div class="submit-post" id="admin">
                <a href="<%=request.getContextPath()%>/NewGetLinks"><span class="submit-text">Admin</span></a>
            </div>-->

        <!--    <div class="submit-post" id="admin" style="display: none">
                <a href="<%=request.getContextPath()%>/NewGetLinks"><span>Admin</span></a>
            </div>-->

        <div class="clearfix"></div>
    </div>

    <div id="status"></div>
    <div class="row">
        <div class="col-md-12">
            <%
                List<Link>links=(List<Link>) request.getAttribute("links");
               for(int i=0;i<links.size();i++)
               {

               Link link=links.get(i);
            %>
            <div class="permalink-container">
                <div class="permalink-box">
                    <div class="permalink-box-header">
                        <a href="#" id="article_title_<%=link.getLinkId()%>"><h4 id="article_title_text"><%=link.getTitle()%></h4></a>
                        <div class="permalink-box-header-icon">
                            <div class="icon-news">
                                <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                                     width="18" height="16" viewBox="0 0 950.000000 1280.000000"
                                     preserveAspectRatio="xMidYMid meet">
                                    <g transform="translate(0.000000,1280.000000) scale(0.100000,-0.100000)"
                                       fill="#000000" stroke="none">
                                        <path d="M0 6400 l0 -6400 4750 0 4750 0 0 6400 0 6400 -4750 0 -4750 0 0
                                       -6400z m8800 0 l0 -5700 -4050 0 -4050 0 0 5700 0 5700 4050 0 4050 0 0 -5700z"/>
                                        <path d="M7680 11565 c-8 -2 -48 -9 -89 -15 -286 -47 -489 -212 -536 -437 -43
                                       -201 8 -386 144 -521 78 -77 159 -126 368 -220 81 -37 174 -82 207 -100 201
                                       -114 220 -283 41 -372 l-70 -35 -145 0 c-120 0 -159 4 -223 23 -91 26 -214 86
                                       -280 136 l-47 36 0 -224 0 -225 78 -29 c200 -77 487 -99 743 -56 170 28 293
                                       86 390 183 111 112 155 238 146 421 -8 162 -57 272 -165 372 -75 70 -148 113
                                       -357 210 -228 107 -258 124 -313 179 -71 73 -77 156 -16 227 109 128 427 132
                                       677 9 l87 -43 0 206 0 206 -55 18 c-112 35 -202 47 -385 51 -102 2 -192 2
                                       -200 0z"/>
                                        <path d="M1170 10540 l0 -1000 215 0 215 0 -1 528 c-1 290 -4 608 -8 707 -3
                                       99 -6 181 -5 183 1 2 26 -37 56 -85 30 -48 238 -368 462 -710 l409 -623 228 0
                                       229 0 0 1000 0 1000 -215 0 -215 0 1 -597 c0 -329 4 -640 7 -691 4 -50 5 -92
                                       2 -92 -3 0 -27 38 -55 84 -27 45 -228 356 -445 689 l-395 607 -242 0 -243 0 0
                                       -1000z"/>
                                        <path d="M3180 10540 l0 -1000 600 0 600 0 0 180 0 180 -375 0 -375 0 0 230 0
                                       230 330 0 330 0 -2 183 -3 182 -327 3 -328 2 0 220 0 220 351 2 351 3 0 130
                                       c-1 89 2 124 8 110 9 -18 75 -267 392 -1482 l103 -393 261 0 261 0 26 98 c206
                                       769 321 1216 336 1302 l17 105 12 -90 c7 -49 88 -387 182 -750 93 -363 170
                                       -661 170 -662 0 -2 113 -2 252 -1 l251 3 258 975 c142 536 261 985 264 998 l5
                                       22 -227 -2 -228 -3 -92 -445 c-132 -638 -201 -986 -209 -1055 l-7 -60 -13 90
                                       c-6 50 -88 402 -181 782 l-168 693 -241 0 -242 0 -6 -24 c-3 -14 -89 -328
                                       -190 -698 -101 -370 -190 -711 -197 -758 l-13 -85 -9 75 c-11 91 -64 357 -200
                                       1000 l-103 485 -812 3 -812 2 0 -1000z"/>
                                        <path d="M1110 8548 l0 -353 3605 3 3605 2 0 350 0 350 -3605 0 -3605 0 0
                                       -352z"/>
                                        <path d="M1110 7190 l0 -350 3605 0 3605 0 0 350 0 350 -3605 0 -3605 0 0
                                       -350z"/>
                                        <path d="M1110 5875 l0 -355 3605 0 3605 0 0 355 0 355 -3605 0 -3605 0 0
                                       -355z"/>
                                        <path d="M1110 4520 l0 -350 3605 0 3605 0 0 350 0 350 -3605 0 -3605 0 0
                                       -350z"/>
                                        <path d="M1110 3178 l0 -353 3605 3 3605 2 0 350 0 350 -3605 0 -3605 0 0
                                       -352z"/>
                                        <path d="M1110 1820 l0 -350 3605 0 3605 0 0 350 0 350 -3605 0 -3605 0 0
                                       -350z"/>
                                    </g>
                                </svg>
                            </div>
                            <span class="icon-news-title" id="article_domain"><%=link.getDomain()%></span>
                            <div class="icon-news-date">
                                <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                                     width="19" height="19" viewBox="0 0 1280.000000 1280.000000"
                                     preserveAspectRatio="xMidYMid meet">
                                    <g transform="translate(0.000000,1280.000000) scale(0.100000,-0.100000)"
                                       fill="#000000" stroke="none">
                                        <path d="M6015 12413 c-462 -30 -960 -122 -1404 -259 -1166 -362 -2215 -1090
                                    -2969 -2061 -655 -845 -1073 -1843 -1211 -2893 -41 -316 -46 -398 -46 -805 0
                                    -416 7 -524 56 -870 128 -902 487 -1811 1014 -2565 390 -557 888 -1062 1430
                                    -1451 867 -622 1824 -985 2915 -1105 197 -21 784 -30 1006 -15 2081 145 3911
                                    1329 4906 3174 615 1139 842 2500 632 3796 -257 1592 -1134 3005 -2450 3947
                                    -898 643 -1953 1022 -3069 1104 -126 9 -684 11 -810 3z m710 -793 c1866 -117
                                    3514 -1210 4356 -2892 613 -1223 714 -2670 279 -3978 -211 -634 -525 -1195
                                    -967 -1725 -132 -159 -426 -456 -588 -594 -1297 -1108 -3014 -1514 -4672
                                    -1106 -1356 334 -2548 1230 -3253 2445 -390 674 -617 1392 -691 2190 -16 174
                                    -16 706 0 880 74 799 300 1516 691 2190 704 1214 1894 2109 3250 2444 525 130
                                    1063 179 1595 146z"/>
                                        <path d="M6298 11522 l-98 -3 0 -319 0 -320 200 0 200 0 0 320 0 320 -89 0
                                    c-49 0 -95 1 -103 3 -7 1 -57 1 -110 -1z"/>
                                        <path d="M3875 10857 c-172 -98 -225 -131 -225 -140 0 -14 311 -547 319 -547
                                    4 0 57 30 117 66 60 36 135 80 167 97 31 17 57 35 57 39 0 9 -299 519 -314
                                    536 -9 10 -35 -1 -121 -51z"/>
                                        <path d="M8787 10883 c-61 -98 -297 -503 -297 -510 0 -5 39 -31 88 -58 48 -26
                                    122 -70 165 -97 43 -26 82 -48 86 -48 8 0 321 534 321 548 0 7 -163 106 -282
                                    172 l-57 31 -24 -38z"/>
                                        <path d="M2002 9028 c-87 -145 -145 -259 -136 -268 13 -12 534 -310 542 -310
                                    7 0 204 342 200 345 -2 1 -113 64 -248 140 -135 75 -259 146 -277 156 l-31 20
                                    -50 -83z"/>
                                        <path d="M10475 8955 c-148 -85 -274 -156 -279 -158 -5 -2 13 -41 41 -88 28
                                    -46 72 -123 99 -171 26 -49 51 -88 54 -88 8 0 531 299 544 310 5 5 -10 43 -34
                                    87 -42 76 -151 263 -154 263 0 -1 -122 -70 -271 -155z"/>
                                        <path d="M6271 6884 c-179 -48 -326 -206 -363 -390 -7 -32 -12 -60 -13 -61 -1
                                    -2 -460 -172 -1021 -379 -1018 -375 -2863 -1057 -2896 -1069 -10 -4 -18 -16
                                    -18 -27 0 -17 4 -19 28 -13 29 7 952 267 2127 600 402 114 991 280 1309 370
                                    l579 164 51 -45 c63 -55 167 -109 243 -125 78 -16 204 -6 275 21 122 46 218
                                    131 278 248 19 36 26 42 55 42 18 0 147 7 286 15 140 8 733 42 1319 75 1289
                                    72 1220 67 1220 90 0 23 69 18 -1220 90 -586 33 -1179 67 -1319 75 -139 8
                                    -268 15 -287 15 -31 0 -36 5 -60 52 -36 71 -132 169 -203 207 -112 59 -253 77
                                    -370 45z"/>
                                        <path d="M1280 6400 l0 -200 320 0 320 0 0 200 0 200 -320 0 -320 0 0 -200z"/>
                                        <path d="M10880 6400 l0 -200 320 0 320 0 0 200 0 200 -320 0 -320 0 0 -200z"/>
                                        <path d="M2133 4197 c-144 -84 -265 -155 -268 -159 -8 -8 56 -131 137 -266
                                    l50 -83 31 20 c18 10 142 81 277 156 135 76 246 139 248 140 6 5 -194 344
                                    -203 344 -5 0 -128 -69 -272 -152z"/>
                                        <path d="M10336 4263 c-27 -49 -71 -125 -98 -171 -28 -46 -48 -85 -47 -86 2
                                    -2 110 -63 239 -136 129 -72 254 -143 276 -156 l42 -24 55 93 c96 161 140 248
                                    131 256 -14 13 -535 311 -543 311 -4 0 -29 -39 -55 -87z"/>
                                        <path d="M3806 2361 c-86 -149 -156 -273 -156 -278 0 -8 148 -99 282 -173 l57
                                    -31 15 23 c42 66 306 520 306 526 0 4 -26 22 -57 39 -32 17 -107 61 -167 97
                                    -60 36 -113 66 -117 66 -4 0 -77 -121 -163 -269z"/>
                                        <path d="M8783 2606 c-18 -12 -91 -56 -163 -97 -71 -40 -130 -77 -130 -82 0
                                    -8 237 -415 297 -510 l24 -38 57 31 c120 66 282 165 282 173 0 17 -314 547
                                    -324 547 -6 -1 -26 -11 -43 -24z"/>
                                        <path d="M6200 1600 l0 -320 200 0 200 0 0 320 0 320 -200 0 -200 0 0 -320z"/>
                                    </g>
                                </svg>
                            </div>
                            <span class="icon-news-time"> <%=(link.getHoursElapsed() <= 24 ? link.getHoursElapsed() + " hours ago " : link.getHoursElapsed() / 24 + " days ago ") %></span>
                            <div class="icon-news-category">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                            </div>
                            <span class="icon-news-cate"><%=link.getCategory()%></span>
                            <div class="social-icons">
                               <div class="icon-email" title="Share on Email" onclick="email()" id="icon-email-share"><i class="fa fa-envelope" ></i></div>
                               <div class="icon-facebook" title="Share on Facebook" onclick="shareFacebook('<%=link.getLinkId()%>')"><i class="fa fa-facebook"></i></div>
                                <div class="icon-twitter" title="Share on Twitter" onclick="shareTwitter('<%=link.getLinkId()%>')"><i class="fa fa-twitter"></i></div>
                                <div class="icon-whatsapp" title="Share on Whatsapp" onclick="shareWhatsapp('<%=link.getLinkId()%>')"><i class="fa fa-whatsapp"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="permalink-box-content">
                        <img src="<%=link.getImageUrl()%>" alt="Permalink-img" id="article_image_url">
                        <p id="article_desc" cus_desc=<%=link.getDesc()%>"> <%=link.getDesc()%><span>...</span><span><a id="article_url" href="<%=link.getUrl()%>" target="_blank">மேலும்</a></span></p>
                    </div>
                    <div class="permalink-footer">
                        <div class="like" title="Like" >
                            <div class="footer-like-icon" onclick="like(<%=link.getLinkId()%>)" id=like_<%=link.getLinkId()%>>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
                            </div>
                            <span class="footer-like-count"><%=link.getVote_up()%></span>
                        </div>

                        <%
                            String t =link.getUserVoted();
                            System.out.println("t---->"+t);
                            if(t.equals("1"))
                            {
                        %>
                        <script>
                            var  idLink='<%=link.getLinkId()%>';
                            document.getElementById("like_"+idLink).firstElementChild.setAttribute("stroke","#e67e22");
                            //document.getElementById("like_"+idLink).style="background-color:#e67e22";
                        </script>
                        <%
                            }
                        %>
                        <div class="dislike" title="Dislike" >
                            <div class="footer-like-icon" onclick="dislike(<%=link.getLinkId()%>)" id="dislike_<%=link.getLinkId()%>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>
                            </div>
                            <span class="footer-like-count"><%=link.getVote_down()%></span>
                        </div>

                        <%
                            String t1 =link.getUserVoted();
                            System.out.println("t---->"+t);
                            if(t1.equals("-1"))
                            {
                        %>
                        <script>
                            var  idLink='<%=link.getLinkId()%>';
                            document.getElementById("dislike_"+idLink).firstElementChild.setAttribute("stroke","#e67e22");
                            //document.getElementById("dislike_"+idLink).style="background-color:#e67e22";
                        </script>
                        <!--<div class="map" title="maps" style="display:inline-block;  margin-left:25px; position: absolute; margin-top: 7px;" onclick="showMap(<%=link.getLinkId()%>)">
                            <div class="footer-like-icon">
                                <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="22px" height="22px"><path d="M 35.5 3 C 32.316385 3 29.491595 4.5852966 27.765625 7 L 11.5 7 C 8.4802259 7 6 9.4802259 6 12.5 L 6 37.5 C 6 40.519774 8.4802259 43 11.5 43 L 36.5 43 C 39.519774 43 42 40.519774 42 37.5 L 42 19.693359 C 43.52453 17.664135 45 15.506079 45 12.476562 C 45 7.2583961 40.726284 3 35.5 3 z M 35.5 6 C 39.109716 6 42 8.8847299 42 12.476562 C 42 14.69209 40.93429 16.141555 39.326172 18.265625 C 38.057178 19.941766 36.628032 22.186244 35.5 25 C 34.371968 22.186244 32.942822 19.941766 31.673828 18.265625 C 30.06571 16.141555 29 14.69209 29 12.476562 C 29 8.8847299 31.890284 6 35.5 6 z M 11.5 10 L 26.341797 10 C 26.125724 10.791015 26 11.619076 26 12.476562 C 26 14.991244 27.017951 16.904478 28.234375 18.644531 L 9.0351562 37.84375 C 9.0199745 37.730558 9 37.617889 9 37.5 L 9 12.5 C 9 11.101774 10.101774 10 11.5 10 z M 35.5 10 C 34.119 10 33 11.119 33 12.5 C 33 13.881 34.119 15 35.5 15 C 36.881 15 38 13.881 38 12.5 C 38 11.119 36.881 10 35.5 10 z M 16.515625 12 C 13.490048 12 11.015625 14.474423 11.015625 17.5 C 11.015625 20.525577 13.490048 23 16.515625 23 C 19.333765 23 21.667751 20.852007 21.980469 18.113281 A 1.0001 1.0001 0 0 0 20.986328 17 L 17.015625 17 A 1.0001 1.0001 0 1 0 17.015625 19 L 19.419922 19 C 18.835281 20.116815 17.865351 21 16.515625 21 C 14.571202 21 13.015625 19.444423 13.015625 17.5 C 13.015625 15.555577 14.571202 14 16.515625 14 C 17.355012 14 18.115 14.291164 18.716797 14.779297 A 1.0001 1.0001 0 1 0 19.976562 13.226562 C 19.032359 12.460695 17.822238 12 16.515625 12 z M 30.033203 21.087891 C 31.402376 22.933637 32.725514 24.943963 33.513672 27.962891 L 33.515625 27.962891 C 33.552145 28.102405 33.535375 28.288143 33.714844 28.767578 C 33.804574 29.007296 33.993117 29.366839 34.357422 29.636719 C 34.72168 29.906599 35.17174 30 35.5 30 C 35.828826 30 36.280435 29.90692 36.644531 29.636719 C 37.008628 29.366518 37.19556 29.005162 37.285156 28.765625 C 37.464349 28.286551 37.448068 28.10328 37.484375 27.964844 L 37.484375 27.962891 L 37.486328 27.962891 C 37.877609 26.464145 38.405158 25.220399 39 24.113281 L 39 37.5 C 39 37.617889 38.980025 37.730558 38.964844 37.84375 L 26.121094 25 L 30.033203 21.087891 z M 24 27.121094 L 36.84375 39.964844 C 36.730558 39.980025 36.617889 40 36.5 40 L 11.5 40 C 11.382111 40 11.269442 39.980025 11.15625 39.964844 L 24 27.121094 z"/></svg>
                            </div>
                        </div>-->
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</div>

</div>
</div>
<!-- Share Box -->
<div class="share-container" id="share-container">
    <div class="share-box">
        <div class="share-box-header">
            <h4>Share</h4>
            <span class="close-share-box" id="share-close" title="close"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
        </div>
        <div class="share-box-content">
            <div class="share-icon" title="Share on Email" id="share-email" onclick="email()"><i class="fa fa-envelope"></i></div>
            <div class="share-icon" id="share-whatsapp" title="Share on Whatsapp" onclick="shareWhatsapp()"><i class="fa fa-whatsapp"></i></div>
            <div class="share-icon" id="share-twitter" title="Share on Twitter" onclick="shareTwitter()"><i class="fa fa-twitter"></i></div>
            <div class="share-icon" id="share-facebook" title="Share on Facebook" onclick="shareFacebook()"><i class="fa fa-facebook"></i></div>
        </div>
    </div>
</div>
<!-- End of Share Box-->


<!-- Email Share -->
<div class="email-share-container" id="email-share-container">
    <div class="email-share-box">
        <!--<div class="email-share-header">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
        </div>-->
        <div class="email-share-content">
            <form id="signin-form">
                <div class="input-group">
                    <span class="input-group-addon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                    </span>
                    <input type="email" class="form-control" placeholder="Email Id" id="email-share">
                </div>
                <div class="submit-btn" id="email-submit-btn">
                    <p id="email-title"></p>
                    <p id="email-url"></p>
                    <p id="email-image"></p>
                    <p id="email-domain"></p>
                    <button type="button" onclick="emailSubmit()">Submit</button>
                </div>
                <span class="email-notif" id="email-notif">Email Sent Successfully</span>
            </form>
        </div>
    </div>
</div>
<!-- End of Email Share-->



<!-- Login Box -->
<div class="login-box-container" id="login-box">
    <div class="login-box">
        <div class="login-box-header">
            <h4>You are not Logged In Please</h4>
            <span class="close-login-box" title="close"><svg xmlns="http://www.w3.org/2000/svg" id="login-box-close" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
        </div>
        <div class="login-box-content">
            <a href="<%=request.getContextPath()%>/SignIn#login" id="login" target="_blank">
                <div class="login-btn">
                    <div><i class="fa fa-lock"></i></div>
                    <span class="login-btn-txt">Login</span>
                    <p>Already a member?</p>
                </div>
            </a>
            <a href="<%=request.getContextPath()%>/SignIn#signup" id="signup" target="_blank">
                <div class="signup-btn">
                    <div><i class="fa fa-user"></i></div>
                    <span class="signup-btn-txt">Signup</span>
                    <p>Create new account</p>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- End of Login Box-->


<%--<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>--%>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
<script>

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-61533941-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<!--<script type="text/javascript">
    //<![CDATA[
    var sc_project = 9681219;
    var sc_invisible = 1;
    var sc_security = "9ea9e173";
    var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
    document.write("<sc" + "ript type='text/javascript' src='" +
    scJsHost +
    "statcounter.com/counter/counter_xhtml.js'></" + "script>");
    //]]>
</script>
<noscript>
    <div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"  class="statcounter"><img class="statcounter"  src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                         alt="shopify analytics"/></a></div>
</noscript>-->
<!-- End of StatCounter Code for Default Guide -->
<script type="text/javascript">
    var infolinks_pid = 2467450;
    var infolinks_wsid = 0;
</script>
<script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script>

<script>
    var email_submit_btn = document.getElementById('email-submit-btn');
    var email_share = document.getElementById('email-share');
    var signin_form = document.getElementById('signin-form');
    var icon_email_share = document.getElementById('icon-email-share');
    email_submit_btn.addEventListener('click', function(e){
        e.stopPropagation();
    })
    email_share.addEventListener('click', function(e){
        e.stopPropagation();
    })
    signin_form.addEventListener('click', function(e){
        e.stopPropagation();
    })
    icon_email_share.addEventListener('click', function(e){
        e.stopPropagation();
    })
</script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : <%=facebookId%>,
            xfbml      : true,
            version    : 'v2.9'
        });
    };
   (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function share(id){
        document.getElementById('share-container').style.display="block";
        var share_whatsapp = document.getElementById('share-whatsapp');
        var share_twit = document.getElementById('share-twitter');
        var share_facebook = document.getElementById('share-facebook');
        var share_email = document.getElementById('share-email');
        var article_title = document.getElementById("article_title_text"+id).innerHTML;
        var article_header = document.getElementById("article_title_text"+id);
        var article_image_url = document.getElementById("article_image_url_"+id).src;
        var article_url = document.getElementById("article_url_"+id).href;
        //var article_domain=document.getElementById("article_domain_"+id).innerHTML;
        share_whatsapp.setAttribute('share-title', article_title);
        share_whatsapp.setAttribute('share-image-url', article_image_url);
        share_whatsapp.setAttribute('share-url', article_url);
        share_twit.setAttribute('share-title', article_title);
        share_twit.setAttribute('share-image-url', article_image_url);
        share_twit.setAttribute('share-url', article_url);
        share_facebook.setAttribute('share-title', article_title);
        share_facebook.setAttribute('share-image-url', article_image_url);
        share_facebook.setAttribute('share-url', article_url);
        article_header.setAttribute('share-title', article_title_text);


        share_email.setAttribute('share-title', article_title);
        share_email.setAttribute('share-image-url', article_image_url);
        share_email.setAttribute('share-url', article_url);
        //share_email.setAttribute('share-domain',article_domain);
    }
    function shareWhatsapp(id){
        var title = document.getElementById("article_title_text").innerHTML;
        var image_url = document.getElementById("article_image_url").src;
        //var url = document.getElementById("article_url").href;
        var url = window.location.href;
        var message = "சுடசுட(chudachuda.com)" + "%0a" + "%0a" + encodeURIComponent(title) + " - " + "%0a" + "%0a" +encodeURIComponent(url);
        //window.open('https://api.whatsapp.com/send?text=' + message);

        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            window.open('https://api.whatsapp.com/send?text=' + message);
        }
        else{
            window.open('https://web.whatsapp.com/send?text=' + message);
        }
        document.getElementById('share-container').style.display="none";
    }
    function shareTwitter(id){
        var title = document.getElementById("article_title_text").innerHTML;
        var image_url = document.getElementById("article_image_url").src;
        //var url = document.getElementById("article_url").href;
        var url = window.location.href;
        //var message_twit = encodeURIComponent(title)+encodeURIComponent(url)+encodeURIComponent(image_url);
        window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'\n&text='+'சுடசுட(chudachuda.com)'+'%0a'+encodeURIComponent(title), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
    }

    function shareFacebook(id){
        var title = document.getElementById("article_title_text").innerHTML;
        var image_url = document.getElementById("article_image_url").src;
        var url = "<%=url%>/News_Detail?id="+id;
        var desc = document.getElementById("article_desc").innerHTML;
        var desc_new = desc.replace(/<span.?>.?<\/span>/ig,'');
        var desc_new_1 = desc_new.replace(/<a.?>.?<\/a>/ig,'');


        FB.init({
            appId      : <%=facebookId%>,
            xfbml      : true,
            version    : 'v2.9'
        });

        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            action_properties: JSON.stringify({
                object : {
                    'og:url':url,
                    'og:title': title,
                    'og:image':image_url,
                    'og:description':desc_new_1

                }
            })

        });
    }

    function email(e){
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);
        // emailId="sathiyak@upspring.it"

            document.getElementById('email-share-container').style = "display: block";
            var article_title = document.getElementById("article_title_text").innerHTML;
            var article_image_url = document.getElementById("article_image_url").src;
            var article_url = window.location.href;
            var article_domain=document.getElementById("article_domain").innerHTML;
            document.getElementById("email-title").setAttribute('email-title', article_title);
            document.getElementById("email-image").setAttribute('email-image', article_image_url);
            document.getElementById("email-url").setAttribute('email-url', article_url);
            document.getElementById("email-domain").setAttribute('email-domain', article_domain);
            e.stopPropagation();

    }
    function  emailSubmit()
    {
        console.log(document.getElementById("email-share").value);
        var emailId=document.getElementById("email-share").value;
        var title = document.getElementById('email-title').getAttribute('email-title');
        var image_url = document.getElementById('email-image').getAttribute('email-image');
        var url = document.getElementById('email-url').getAttribute('email-url');
        var domain=document.getElementById('email-domain').getAttribute("email-domain");


        $.ajax({
            type: "GET",
            url: "/ShareEmail?emailId=" + emailId+"&title="+encodeURIComponent(title)+"&image_url="+encodeURIComponent(image_url)+"&url="+encodeURIComponent(url)+"&domain="+encodeURIComponent(domain),
            success: function (result) {
                document.getElementById("share-container").style.display="none";
                //document.getElementById('email-share-container').style = "display: none";
                document.getElementById('email-notif').style="display:block";
                const myTimeout = setTimeout(resetHeader, 6000);
                function resetHeader(){
                    document.getElementById('email-share-container').style="display: none";
                    document.getElementById('email-notif').style="display:none";
                    document.getElementById('email-share').value="";
                }
            }
        });

    }



    function  like(id) {
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);
        //  emailId="sathiyak@upspring.it"
        if(emailId=="")
        {
            document.getElementById('login-box').style="display: block";

        }
        else
        {
            document.getElementById("like_"+id).setAttribute("style","pointer-events:none")
            document.getElementById("dislike_"+id).setAttribute("style","pointer-events:none;")


            $.ajax({
                type: "GET",
                url: "/LikeArticles?id=" + id + "&emailId=" + emailId,
                success: function (result) {

                    console.log(result);
                    console.log(result.vote_up);
                    console.log(result.vote_down);
                    document.getElementById("like_"+id).nextElementSibling.innerHTML=result.vote_up;
                    document.getElementById("dislike_"+id).nextElementSibling.innerHTML=result.vote_down;
                    document.getElementById("dislike_"+id).style.marginLeft="30px";

                    console.log("its set ah...")

                    var likeFill=document.getElementById("like_"+id).firstElementChild.getAttribute("stroke");
                    var dislikeFill=document.getElementById("dislike_"+id).firstElementChild.getAttribute("stroke");
                    console.log(likeFill);
                    if(likeFill=="#000000")
                    {
                        if(dislikeFill=="#e67e22")
                        {
                            document.getElementById("dislike_"+id).firstElementChild.setAttribute("stroke","#000000");
                            //document.getElementById("dislike_"+id).style="background: #ffffff";
                        }
                        document.getElementById("like_"+id).firstElementChild.setAttribute("stroke","#e67e22");
                        //document.getElementById("like_"+id).style="background: #e67e22";

                    }
                    if(likeFill=="#e67e22")
                    {
                        document.getElementById("like_"+id).firstElementChild.setAttribute("stroke","#000000");
                        //document.getElementById("like_"+id).style="background: #ffffff";
                    }
                    document.getElementById("like_"+id).setAttribute("style","pointer-events:cursor");
                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor;")



                }
            });

        }

    }

    function dislike(id)
    {
        var emailId = '<%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getEmail() : "")%>';
        console.log(emailId);
        //emailId="sathiyak@upspring.it"
        if(emailId=="")
        {
            document.getElementById('login-box').style="display: block";
        }
        else
        {
            document.getElementById("like_"+id).setAttribute("style","pointer-events:none")
            document.getElementById("dislike_"+id).setAttribute("style","pointer-events:none;");



            $.ajax({
                type: "GET",
                url: "/DisLikeArticles?id=" + id + "&emailId=" + emailId,
                success: function (result) {

                    console.log(result);
                    console.log(result.vote_up);
                    console.log(result.vote_down);
                    document.getElementById("like_"+id).nextElementSibling.innerHTML=result.vote_up;
                    document.getElementById("dislike_"+id).nextElementSibling.innerHTML=result.vote_down;


                    var likeFill=document.getElementById("like_"+id).firstElementChild.getAttribute("stroke");
                    var dislikeFill=document.getElementById("dislike_"+id).firstElementChild.getAttribute("stroke");
                    console.log(likeFill);
                    if(dislikeFill=="#000000")
                    {
                        if(likeFill=="#e67e22")
                        {
                            document.getElementById("like_"+id).firstElementChild.setAttribute("stroke","#000000");
                            //document.getElementById("like_"+id).style="background: #ffffff";
                        }
                        document.getElementById("dislike_"+id).firstElementChild.setAttribute("stroke","#e67e22");
                        //document.getElementById("dislike_"+id).style="background: #e67e22";

                    }
                    if(dislikeFill=="#e67e22")
                    {
                        document.getElementById("dislike_"+id).firstElementChild.setAttribute("stroke","#000000");
                        //document.getElementById("dislike_"+id).style="background: #ffffff";
                    }
                    document.getElementById("like_"+id).setAttribute("style","pointer-events:cursor")
                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor")

                    document.getElementById("dislike_"+id).setAttribute("style","pointer-events:cursor;")




                }
            });

        }
    }


</script>

<script>
    var login_box = document.getElementById('login-box');
    var login_box_close = document.getElementById('login-box-close');
    login_box_close.addEventListener('click', function(){
        document.getElementById('login-box').style="display: none";
    })
</script>

<script>
    $("body").on('click',function(){
        $('#email-share-container').fadeOut(300);
    });
    function showMap(linkId)
    {

        console.log("---inside of save"+linkId);
        window.open("/chudachudaMap?id="+linkId)

    }
</script>

</body>
</html>
