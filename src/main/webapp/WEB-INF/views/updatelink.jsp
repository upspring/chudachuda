<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page
        import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User, java.net.URLDecoder"%>
<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Chuda Chuda.com">
  <meta name="keywords" content="Chuda Chuda.com">
  <title>ChudaChuda.com</title>
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript">
    var timer = <%=CountdownTimerService.initialTimerSeconds%>;
    var clock;
    $(document).ready(function () {
      clock = $('.clock').FlipClock(timer, {
        countdown: true,
        clockFace: 'MinuteCounter',
        autoStart: true,
        callbacks: {
          stop: function() {
            window.location.reload();
            clock.start();
          }
        }
      });
    });
  </script>

</head>
<body>
<%String tab = (String) request.getAttribute("tab");
  if (tab==null) tab="current"; %>
<div class="home-container">
  <div class="home-sidebar">
    <div class="logo"></div>
    <div class="left-arrow side-menu"></div>
    <div class="s-cat-box">
      <div class="s-topics">
        <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
          <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="new-icon"></span>
            </div>
            <h5>NEW</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=current">
          <div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="trending-icon"></span>
            </div>
            <h5>TRENDING</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
          <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="expired-icon"></span>
            </div>
            <h5>EXPIRED</h5>
          </div>
        </a>

        <a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="mylinks-icon"></span>
            </div>
            <h5>Likes</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="later-filled-icon"></span>
            </div>
            <h5>Bookmark</h5>
          </div>
        </a>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="cat-icon"></span>
          <h5>CATEGORIES</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String cat = (String)request.getAttribute("category");
          cat = (cat==null?"All":cat); %>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=All"><li <%=(cat.equalsIgnoreCase("all")?"class='cat-list-active'":"")%>>All</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Health"><li <%=(cat.equalsIgnoreCase("health")?"class='cat-list-active'":"")%>>HEALTH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Society"><li <%=(cat.equalsIgnoreCase("society")?"class='cat-list-active'":"")%>>SOCIETY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=World"><li <%=(cat.equalsIgnoreCase("world")?"class='cat-list-active'":"")%>>WORLD</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Politics"><li <%=(cat.equalsIgnoreCase("politics")?"class='cat-list-active'":"")%>>POLITICS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Sports"><li <%=(cat.equalsIgnoreCase("sports")?"class='cat-list-active'":"")%>>SPORTS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Spirituality"><li <%=(cat.equalsIgnoreCase("spirituality")?"class='cat-list-active'":"")%>>SPIRITUALITY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Technology"><li <%=(cat.equalsIgnoreCase("technology")?"class='cat-list-active'":"")%>>TECHNOLOGY</li></a>
          </ul>
        </div>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="country-icon"></span>
          <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String country=request.getParameter("country");
          if ( country == null ) country="all";%>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'><li <%=(country.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'><li <%=(country.equalsIgnoreCase("India")?"class='cat-list-active'":"")%>>INDIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'><li <%=(country.equalsIgnoreCase("United States")?"class='cat-list-active'":"")%>>USA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'><li <%=(country.equalsIgnoreCase("Australia")?"class='cat-list-active'":"")%>>AUSTRALIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'><li <%=(country.equalsIgnoreCase("United Kingdom")?"class='cat-list-active'":"")%>>UNITED KINGDOM</li></a>
          </ul>
        </div>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="lang-icon"></span>
          <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String language=request.getParameter("ln");
          if ( language == null ) language="all";%>

        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en"><li <%=(language.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Emglish&ln=en"><li <%=(language.equalsIgnoreCase("en")?"class='cat-list-active'":"")%>>ENGLISH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta"><li <%=(language.equalsIgnoreCase("ta")?"class='cat-list-active'":"")%>>தமிழ்</li></a>
            <%--<a href="#"><li>HINDI</li></a>--%>
            <%--<a href="#"><li>TELUGU</li></a>--%>
            <%--<a href="#"><li>MALAYALAM</li></a>--%>
          </ul>
        </div>
      </div>
      <div class="clock"></div>
    </div>
  </div>
</div>
<div class="home-main" id="page-content-wrapper">

  <%--<div class="home-header">
      <div class="input-group home-search">
          <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
              <span class="input-group-btn">
                  <button class="btn home-search-btn" type="button"></button>
              </span>
      </div>
      <div class="home-login">
          <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
          <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
      </div>
      <div class="home-user dropdown">
          <div class="h-user-pic">
              <img src="images/user-avatar.png" alt="User Picture">
          </div>
          <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="h-user-name">Hi Venkk</span>
              <span class="down-icon-grey"></span>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
              <a href="#"><li>Edit Profile</li></a>
              <a href="#"><li>Change Password</li></a>
              <a href="#"><li>Logout</li></a>
          </ul>
      </div>
      <div class="clearfix"></div>
  </div>--%>
  <div class="submit-content">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog signup-dialog">
        <div class="modal-content signup-content">
          <div class="modal-body signup-body">

            <div class="submit-content pad0">
              <div class="submit-item">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="sign-tab">
                  <ul>
                    <li class="active" id="signin">SIGNIN</li>
                    <li id="signup">SIGNUP</li>
                  </ul>
                </div>
                <div class="sign-body">
                  <div class="login-left">
                    <form id="signin-form">
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                        <input type="email" class="form-control" placeholder="Email Id">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                        <input type="password" class="form-control" placeholder="Password">
                      </div>
                      <div class="pwd-options">
                        <div class="input-group remember-pwd">
                          <label>
                            <input type="checkbox"> Remember Password
                          </label>
                        </div>
                        <div class="input-group fgt-pwd">
                          <a href="#"><span>Forgot Password?</span></a>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="signupBtn">
                        <button class="btn btn-signup">SIGN IN</button>
                      </div>
                    </form>
                    <form id="signup-form" class="hide">
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                        <input type="email" class="form-control" placeholder="Email Id">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/fn-icon.png" alt="N">
														</span>
                        <input type="text" class="form-control" placeholder="User Name">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                        <input type="password" class="form-control" placeholder="Password">
                      </div>

                      <div class="signupBtn">
                        <button class="btn btn-signup">SIGN UP</button>
                      </div>
                    </form>
                  </div>
                  <div class="login-center hr-signin">
                    <hr>
                    <span>OR</span>
                  </div>
                  <div class="login-right">
                    <a href="">
                      <img src="<%=request.getContextPath()%>/resources/images/fb-btn-signin.png" alt="facebook">
                    </a>
                    <a href="">
                      <img src="<%=request.getContextPath()%>/resources/images/gp-btn-signin.png" alt="Google+">
                    </a>
                  </div>
                </div>
              </div>
              <!-- <div class="submit-item-shadow"></div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="submit-item">
      <h4>Edit  Item</h4>
      <div class="s-i-form">
        <form action="<%=request.getContextPath()%>/UpdateLink" method="POST">

          <%
            Link link = (Link) request.getAttribute("link");

            if ( request.getAttribute("error") != null ) { %>
          <p style="color: red; font-size: small;"><%=request.getAttribute("error") %></p>
          <%} %>


          <ul class="list-unstyled">
            <li>
              <input id="linkId" name="linkId" value="<%=link.getLinkId()%>" type="hidden" />
              <label>TITLE</label>
              <input type="text" name="title" id="title" class="form-control submit-input" placeholder="Enter Title"  value='<%=link.getTitle()%>' required >
            </li>
            <li>
              <label>URL</label>
              <input type="url" name="url" id="url" class="form-control submit-input" placeholder="Enter URL" value='<%=link.getUrl()%>' required>
            </li>
            <li>
              <label>Image URL</label>
              <input type="url" name="image_url" id="image_url" class="form-control submit-input" placeholder="Enter Image URL" value='<%=link.getImageUrl()%>' required>
            </li>
            <li>
              <label>Image</label>
              <div align="center">
                <img id="image_url1"style="width:310px; height:208px;">
              </div>
            </li>
            <li>
              <label>Description</label>
              <textarea class="form-control submit-input" name="desc" id="desc" rows="3" placeholder='Enter Description'><%=link.getDesc()==null?"Enter Description":link.getDesc()%></textarea>
            </li>
            <li class="submit-form-select">
              <label>Category</label>
              <select class="form-control submit-input" id="category" name="category">
                <option value="Health">Health</option>
                <option value="Society">Society</option>
                <option value="World">World</option>
                <option value="Politics">Politics</option>
                <option value="Sports">Sports</option>
                <option value="Spirituality">Spirituality</option>
                <option value="Recipie">Recipie</option>
                <option value="Technology">Technology</option>
              </select>
            </li>
            <li class="submit-form-select" >
              <label class="pl10">MEDIA</label>
              <select class="form-control submit-input" id="media" name="media">
                <option value="Text/HTML">Text/HTML</option>
                <option value="Video">Video</option>
                <option value="Others">Other</option>
              </select>
            </li>

            <li class="submit-form-select">
              <label>Language</label>
              <select id="language" name="language"class="form-control submit-input">
                <option value="English">English</option>
                <option value="Tamil">தமிழ்</option>
                <option value="Hindi">Hindi</option>
              </select>
            </li>
            <li class="submit-form-select" >
              <label for="country">Country</label>
              <select  id="country" name="country"class="form-control submit-input">
                <option value="India">India</option>
                <option value="Singapore">Singapore</option>
                <option value="UAE">UAE</option>
                <option value="United States">United States</option>
                <option value="United Kingdom">United Kingdom</option>
              </select>
            </li>

            <li class="m-pad-0">
              <label for="tags">Tags</label>
              <input type="text" id="tags" name="tags"class="form-control submit-input" placeholder="Enter TAGS" value='<%=link.getTags()%>' id="Tags">
            </li>
            <li class="pad-0">

              <ul class="tagEditor">
              </ul>
            </li>
            <li class="text-right m-text-center">
              <button type="submit" class="btn btn-post">
                SUBMIT
              </button>
            </li>
          </ul>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="submit-item-shadow"></div>
  </div>
</div>
</div>

<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>

<%--
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>

<script src="<%=request.getContextPath()%>/js/form-elements.js"></script>
<script src="<%=request.getContextPath()%>/js/settings.js"></script>
--%>


<script type="text/javascript">
  $(document).ready(function() {
    $("#Tags").tagEditor(
            {
              items: ["First tag", "Second tag"],
              confirmRemoval: false
            });
  });
</script>
<%--this for url image jquery--%>
<script>
  $(document).ready(function(){
    $("#image_url").change(function(){
      var sourceUrl = $("#image_url").val();

      $("[id$='image_url1']").attr("src",sourceUrl);
    });
  });
</script>
<!-- Start of StatCounter Code for Default Guide -->

<!-- End of StatCounter Code for Default Guide -->
</body>
</html>