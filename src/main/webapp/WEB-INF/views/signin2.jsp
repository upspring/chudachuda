<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.vo.NameCountVO" %>
<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Chuda Chuda.com">
	<meta name="keywords" content="ChudaChuda.com,chuda,chuda,tamil,news,hot news,chuda chuda news,thamizh,suda,suda suda,india, tamil nadu,kumbakonam,chennai,times,dinamalar,dinakaran,malaimalar">
	<title>ChudaChuda.com</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


	<style type="text/css">
		.fb_iframe_widget iframe {
			opacity: 0;
		}

		.fb_iframe_widget {
			background-image: url("<%=request.getContextPath()%>/resources/images/fb-login.png");
			background-repeat: no-repeat;
		}

		.login-button{
			border:1px solid #e8e8e8;
			/*background-color: #fff;*/
			background-color: #ffcf00;

			color: #000;
			fill: #000;
			width: 200px;
			margin: 0 auto;
			height: auto;
			margin-top: 30px;
			border-radius: 5px;
			cursor: pointer;
			margin-bottom: 20px;
		}
		.login-button:hover{
			background-color: #f7f7ff;
		}
		.login-icon{
			background-color: transparent !important;
			position: absolute;
			padding: 8px 8px 4px 8px;
			border-right: 1px solid #666;
		}
		.login-iconsvg{
			position: absolute;
			margin-top: 5px;
		}
		.login-button span{
			font-size: 11px;
			text-transform: uppercase;
			align-items: center;
			display: inline-flex;
			flex-wrap: nowrap;
			justify-content: center;
			width: inherit;
			flex: 1;
			font-family: sans-serif;
			letter-spacing: 1px;
			font-weight: 600;
			margin-left: 30px;
			padding: 10px;
			color: #222;
			text-decoration: none;
			margin-left: 40px;
		}
		.login-button span:hover{
			text-decoration: none;
		}
	</style>

</head>	
<body>
<%String tab = (String) request.getAttribute("tab");
	if (tab==null) tab="current";
	if ( request.getSession().getAttribute("userInfo") != null ) { User user = (User) session.getAttribute("userInfo"); }%>

<script>
	// This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
		console.log('statusChangeCallback');
		console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
			// Logged into your app and Facebook.
			// testAPI();
			var access_token = response.authResponse.accessToken;

			FB.api('/me', function(response) {
				console.log('Successful login for: ' + response.name);
				document.getElementById("loginTab").style.display="none";
				document.getElementById('status').innerHTML =
						'Loading.... Please wait!';
				/// alert(response.authResponse.accessToken);
				// window.location.href = '<%--=request.getContextPath()--%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token=";
				<% if (request.getParameter("next") != null) {%>
				 window.location.href = '<%=request.getContextPath()%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token="+access_token+"&next=<%=request.getParameter("next")%>";
				<%} else {%>
				 window.location.href = '<%=request.getContextPath()%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token="+access_token;
				<%}%>
			});
			// alert(response.name+","+response.email);
			// alert(response.authResponse.accessToken);

				// .href = '<%-- =request.getContextPath() --%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token="+response.authResponse.accessToken;

		} else if (response.status === 'not_authorized') {
			// The person is logged into Facebook, but not your app.
			/*document.getElementById('status').innerHTML = 'Please log ' +
					'into this app.';*/
		} else {
			// The person is not logged into Facebook, so we're not sure if
			// they are logged into this app or not.
			/*document.getElementById('status').innerHTML = 'Please log ' +
					'into Facebook.';*/
		}
	}

	// This function is called when someone finishes with the Login
	// Button.  See the onlogin handler attached to it in the sample
	// code below.
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '909964385712734',
			cookie     : true,  // enable cookies to allow the server to access
								// the session
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.2' // use version 2.2
		});

		// Now that we've initialized the JavaScript SDK, we call
		// FB.getLoginStatus().  This function gets the state of the
		// person visiting this page and can return one of three states to
		// the callback you provide.  They can be:
		//
		// 1. Logged into your app ('connected')
		// 2. Logged into Facebook, but not your app ('not_authorized')
		// 3. Not logged into Facebook and can't tell if they are logged into
		//    your app or not.
		//
		// These three cases are handled in the callback function.

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function testAPI() {
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
			console.log('Successful login for: ' + response.name);
			document.getElementById('status').innerHTML =
					'Thanks for logging in, ' + response.name + '!';
			// alert('name signin :'+response.name);
			// alert('token :'+response.authResponse.accessToken);
			/// alert(response.authResponse.accessToken);
			// window.location.href = '<%--=request.getContextPath()--%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token=";
			// window.location.href = '<%--=request.getContextPath()--%>/SocialLogin?email='+response.email+"&user="+response.name+"&network=FB&access_token="+response.authResponse.accessToken;
		});
	}

	 function fb_login(){
	 FB.login(function(response) {
	 if (response.authResponse) {
	 console.log('Welcome!  Fetching your information.... ');
	 console.log(response); // dump complete info
	 access_token = response.authResponse.accessToken; //get access token
	 user_id = response.authResponse.userID; //get FB UID
	// alert('username='+response.authResponse.userID);
	// alert('access token='+access_token);
	 console.log('access token='+access_token);
	 FB.api('/me', function(response) {
	 user_email = response.email; //get user email
	 // you can store this data into your database
	 });

	 } else {
	 //user hit cancel button
	 // alert('cancel button');
	 console.log('User cancelled login or did not fully authorize.');

	 }
	 }, {
	 scope: 'public_profile,email,user_friends'
	 });
	}

	(function() {
		var e = document.createElement('script');
		e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
		e.async = true;
		document.getElementById('fb-root').appendChild(e);
	}());



</script>

<div class="home-container">
	<div class="home-sidebar">
		<div class="logo"></div>
		<div class="left-arrow side-menu"></div>
		<div class="s-cat-box">
			<div class="s-topics">
				<a href="<%=request.getContextPath()%>/GetLinks?tab=new">
					<div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="new-icon"></span>
						</div>
						<h5>NEW</h5>
					</div>
				</a>
				<!--<a href="<%=request.getContextPath()%>/GetLinks?tab=current">
					<div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="trending-icon"></span>
						</div>
						<h5>TRENDING</h5>
					</div>
				</a>
				<a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
					<div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="expired-icon"></span>
						</div>
						<h5>EXPIRED</h5>
					</div>
				</a>
				<a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
					<div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="mylinks-icon"></span>
						</div>
						<h5>Likes</h5>
					</div>
				</a>
				<a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
					<div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="later-filled-icon"></span>
						</div>
						<h5>Bookmark</h5>
					</div>
				</a>-->

			<%--	<a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
					<div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
						<div class="s-topic-icon">
							<span class="mylinks-icon"></span>
						</div>
						<h5>Likes</h5>
					</div>
				</a>--%>
			</div>



		</div>
	</div>
	<div class="home-main" id="page-content-wrapper">
		<div class="mobile-header">
			<a href="#"><div class="mobile-menu side-menu"></div></a>
			<a href="#">
				<div class="mobile-logo"></div>
			</a>
			<div class="mobile-user hide">
				<div class="m-user-pic">
					<img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
				</div>
				<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="m-user-name">Hi Venkk</span>
					<span class="down-icon-grey"></span>
				</a>
				<ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
					<a href="#"><li>Edit Profile</li></a>
					<a href="#"><li>Change Password</li></a>
					<a href="#"><li>Logout</li></a>
				</ul>
			</div>
			<div class="mobile-login hide">
				<p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
				<p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
			</div>
		</div>
		<div class="home-header signin-header">
			<div class="input-group home-search">
				<input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
					<span class="input-group-btn">
						<button class="btn home-search-btn" type="button"></button>
					</span>
			</div>
			<div class="home-login hide">
				<p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
				<p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
			</div>
			<div class="home-user dropdown hide">
				<div class="h-user-pic">
					<img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
				</div>
				<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="h-user-name">Hi</span>
					<span class="down-icon-grey"></span>
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					<a href="#"><li>Edit Profile</li></a>
					<a href="#"><li>Change Password</li></a>
					<a href="#"><li>Logout</li></a>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="submit-content">

			<div class="clearfix"></div>
			<div id="status" style="align-content: center; text-align: center; font-weight: bolder; text-decoration-color: #0000FF">
			</div>








			<div id="loginTab" class="submit-item">
				<div class="s-i-form">
					<div class="sign-tab">
						<ul>
							<li class="active" id="signin">SIGNIN</li>
							<li id="signup">SIGNUP</li>
						</ul>
					</div>
					<div class="sign-body">
						<div class="login-left">
							<% if ( request.getAttribute("error" ) !=null ){ %>
							<p color="red"><%=request.getAttribute("error")%></p>
							<%}%>

							<form id="signin-form" action="<%=request.getContextPath()%>/SignIn?action=login"
								  method="post">
								<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/fn-icon.png" alt="M">
														</span>
									<input type="text" id="username_signin" name="email" class="form-control" placeholder="Email" value="<%=request.getAttribute("email")%>">
								</div>
								<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/lock-icon.png" alt="M">
														</span>
									<input type="password" id="password_signin" name="password" class="form-control" placeholder="Password" value="<%=request.getAttribute("password")%>">
								</div>
								<div class="pwd-options">
									<div class="input-group remember-pwd">
										<label>
											<input type="checkbox" name="remember" checked> Stay Signed In
										</label>
									</div>
									<div class="input-group fgt-pwd">
										<a href="#"><span>Forgot Password?</span></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="signupBtn">
									<button id="singninBtn1" class="btn btn-signup">SIGN IN</button>
								</div>
								<input type="hidden" name="next" id="next" value="<%=(request.getParameter("next")!=null?request.getParameter("next").toString():"")%>" />
							</form>
							<form id="signup-form" class="hide" action="<%=request.getContextPath()%>/SignIn?action=signup"
								  method="post">
								<input type="hidden" name="next" id="next1" value="<%=(request.getParameter("next")!=null?request.getParameter("next").toString():"")%>" />
								<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/mail-icon.png" alt="M">
														</span>
									<input type="email" name="email" id="email" class="form-control" placeholder="Email Id">
								</div>
								<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/fn-icon.png" alt="N">
														</span>
									<input type="text" name="username" id="username" class="form-control" placeholder="User Name">
								</div>
								<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/lock-icon.png" alt="M">
														</span>
									<input type="password" class="form-control" id="password" name="password" placeholder="Password">
								</div>

								<div class="signupBtn">
									<button class="btn btn-signup">SIGN UP</button>
								</div>
							</form>
						</div>
						<div class="login-center hr-signin">
							<hr>
							<span>OR</span>
						</div>

						<div class="login-right signup-social hide" id="signup-social">

							<div class="login-button">
								<div class="login-icon">
									<svg width="18" height="18" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M17.64 9.205c0-.639-.057-1.252-.164-1.841H9v3.481h4.844a4.14 4.14 0 01-1.796 2.716v2.259h2.908c1.702-1.567 2.684-3.875 2.684-6.615z" fill="#4285F4" fill-rule="nonzero"></path><path d="M9 18c2.43 0 4.467-.806 5.956-2.18l-2.908-2.259c-.806.54-1.837.86-3.048.86-2.344 0-4.328-1.584-5.036-3.711H.957v2.332A8.997 8.997 0 009 18z" fill="#34A853" fill-rule="nonzero"></path><path d="M3.964 10.71A5.41 5.41 0 013.682 9c0-.593.102-1.17.282-1.71V4.958H.957A8.996 8.996 0 000 9c0 1.452.348 2.827.957 4.042l3.007-2.332z" fill="#FBBC05" fill-rule="nonzero"></path><path d="M9 3.58c1.321 0 2.508.454 3.44 1.345l2.582-2.58C13.463.891 11.426 0 9 0A8.997 8.997 0 00.957 4.958L3.964 7.29C4.672 5.163 6.656 3.58 9 3.58z" fill="#EA4335" fill-rule="nonzero"></path><path d="M0 0h18v18H0z"></path></g></svg>
								</div>
								<a href="http://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://localhost:8080/oauth2callback&response_type=code&client_id=281124818594-hmeukt2p7p4hlno58n75nddrqq0ll8no.apps.googleusercontent.com&approval_prompt=force">
									<!--   <a href="http://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&redirect_uri=http://localhost:8080/LoginAPI&response_type=code&client_id=725860114805-kusk69852lhsqeftumivggpojptb0bu9.apps.googleusercontent.com&approval_prompt=force">-->

									<span>Login With Google</span>
								</a>
							</div>



						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="sigin-item-shadow"></div>
		</div>
	</div>
</div>

<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
		<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#Tags").tagEditor(
		{
			items: ["First tag", "Second tag"],
			confirmRemoval: false
		});
	});
	</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-61533941-1', 'auto');
	ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
	//<![CDATA[
	var sc_project=9681219;
	var sc_invisible=1;
	var sc_security="9ea9e173";
	var scJsHost = (("https:" == document.location.protocol) ?
			"https://secure." : "http://www.");
	document.write("<sc"+"ript type='text/javascript' src='" +
			scJsHost+
			"statcounter.com/counter/counter_xhtml.js'></"+"script>");
	//]]>
</script>
<noscript><div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
									  class="statcounter"><img class="statcounter"
															   src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
															   alt="shopify analytics" /></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>
</html>