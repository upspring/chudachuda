<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.domain.FollowUser"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="ChudaChuda - News for your use">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title><spring:message code='label.title' /></title>

<!-- Bootstrap core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<%=request.getContextPath()%>/resources/css/navbar.css"
	rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
.auto-style1 {
	font-family: "Arial Black";
	font-size: xx-large;
}

.auto-style2 {
	text-align: center;
	color: #6868F8;
	border-left-style: none;
	border-left-width: medium;
	border-right-style: solid;
	border-right-width: 0px;
	border-top-style: solid;
	border-top-width: 0px;
	border-bottom-style: solid;
	border-bottom-width: 0px;
}

.auto-style3 {
	font-family: "Arial Black";
	font-weight: bold;
	font-size: xx-large;
	color: #F43402;
}

.auto-style4 {
	border-width: 0px;
}

.auto-style5 {
	border-left-style: solid;
	border-left-width: 0px;
	border-right-style: none;
	border-right-width: medium;
	border-top-style: none;
	border-top-width: medium;
	border-bottom-style: solid;
	border-bottom-width: 0px;
	font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS",
		sans-serif;
	font-size: medium;
}

.auto-style6 {
	border-left-style: solid;
	border-left-width: 0px;
	border-right-style: none;
	border-right-width: medium;
	border-top-style: solid;
	border-top-width: 0px;
	border-bottom-style: solid;
	border-bottom-width: 0px;
}

.auto-style7 {
	border-left-style: solid;
	border-left-width: 0px;
	border-right-style: none;
	border-right-width: medium;
	border-top-style: none;
	border-top-width: medium;
	border-bottom-style: solid;
	border-bottom-width: 0px;
	font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS",
		sans-serif;
	font-size: small;
}

.auto-style8 {
	border-left-style: solid;
	border-left-width: 0px;
	border-right-style: none;
	border-right-width: medium;
	border-top-style: solid;
	border-top-width: 0px;
	border-bottom-style: none;
	border-bottom-width: medium;
}

.auto-style9 {
	text-align: center;
	color: #6868F8;
	border-left-style: none;
	border-left-width: medium;
	border-right-style: solid;
	border-right-width: 0px;
	border-top-style: none;
	border-top-width: medium;
	border-bottom-style: solid;
	border-bottom-width: 0px;
	font-size: x-large;
}

.auto-style-links {
	A: link{color:#FF0000;
}

A:visited {
	color: #0000FF;
}

}
.auto-style14 {
	font-size: x-small;
}

.auto-style16 {
	font-size: x-small;
	color: black;
}
</style>

</head>

 <body style="background-color: <spring:message code='background.color' />">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=4467235982";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script><table style="width: 1352px; height: 20px">
		<tr>
			<td class="auto-style3" style="width: 1100px"></td>
			<td class="auto-style7">
				<%
					if (request.getSession().getAttribute("userInfo") != null) {
						User user = (User) session.getAttribute("userInfo");
				%>
				<%=user.getUserName()%> <%
 	} else {
 %> &nbsp; <%
 	}
 %>
			</td>
		</tr>
		<tr>
			<td class="auto-style3" style="width: 1066px"></td>
			<td><br /></td>
		</tr>
	</table>

	<div class="container">

		<!-- Static navbar -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid" style="background-color: <spring:message code='background.color' />;">
				<div class="navbar-header" style="background-color: <spring:message code='background.color' />;">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" style="color: #FF8000" href="#"><spring:message code='label.title' /></a>
				</div>
				<div class="navbar-collapse collapse"
					style="background-color: <spring:message code='background.color' />;">
					<ul class="nav navbar-nav" style="background-color: <spring:message code='background.color' />;">
						<li><a style="color: #01B6AD;"
							href="<%=request.getContextPath()%>/GetLinks?tab=Current"><spring:message code='label.trending' /></a></li>
						<li><a style="color: #01B6AD;"
							href="<%=request.getContextPath()%>/GetLinks?tab=New"><spring:message code='label.new' /></a></li>
						<li><a style="color: #01B6AD;"
							href="<%=request.getContextPath()%>/GetLinks?tab=Expired"><spring:message code='label.expired' /></a></li>

						<li class="dropdown"><a style="color: #01B6AD;" href="#"
							class="dropdown-toggle" data-toggle="dropdown"><spring:message code='label.more' /> <b
								class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<%=request.getContextPath()%>/bookmarklet">Bookmarklet</a></li>
								<li><a href="<%=request.getContextPath()%>/about">About
										Us</a></li>
								<%--   <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>  --%>
							</ul></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<%
							if (request.getSession().getAttribute("userInfo") == null) {
						%>
						<li><a style="color: #01B6AD;"
							href="<%=request.getContextPath()%>/SignIn?page=signin">Sign
								In</a></li>
						<%
							} else {
								User user = (User) request.getSession()
										.getAttribute("userInfo");
						%>
						<li><a style="color: #01B6AD;"
							href="<%=request.getContextPath()%>/SignOut">Sign Out (<%=user.getUserName()%>)
						</a></li>
						<%
							}
						%>
						<%-- <li class="active"><a href="./">Default</a></li>
              <li><a href="../navbar-static-top/">Static top</a></li>
              <li><a href="../navbar-fixed-top/">Fixed top</a></li>   --%>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</div>

		<div align="center" style="font-size: large; color: white">
			Welcome to <br><spring:message code="label.title" /></b> <br />&nbsp; <br /> <img
				src="<%=request.getContextPath()%>/resources/img/upspring.jpg"
				height="100" width="100" class="img-rounded" /> <br />&nbsp;
			<p align="center" style="font-size: medium">
				ChudaChuda is a social site with news and views for your use
				<br>&nbsp;<br /> ChudaChuda.com is in <b>beta</b>
				and your feedback is very important to us. <br />Please send your thoughts
				to <a href="mailto:seydhigal@gmail.com?Subject=Feedback" target="_top">seydhigal@gmail.com</a><br />&nbsp;<br />
				<br /> <br /> <br />
				<a class="btn btn-lg btn-primary"
					href="<%=request.getContextPath()%>/GetLinks?tab=current"
					role="button">continue to <spring:message code="label.title" /></a>
				
			</p>
<div class="fb-like" style="" data-href="https://www.facebook.com/pages/%E0%AE%9A%E0%AF%81%E0%AE%9F%E0%AE%9A%E0%AF%81%E0%AE%9Fcom-sudasudacom/609162835843961" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

		</div>

	</div>
	
	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60919802-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
