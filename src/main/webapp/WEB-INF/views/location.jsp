<%--
  Created by IntelliJ IDEA.
  User: Upspring
  Date: 27-01-2022
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Display a web map using an alternate projection</title>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
    <link href="https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.css" rel="stylesheet">
    <script src="https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.js"></script>
    <style>
        body { margin: 0; padding: 0; }
        #map { position: absolute; top: 0; bottom: 0; width: 100%; }
    </style>
</head>
<body>
<div id="map"></div>
<script>
    var geoLat=0;
    var geoLon=0;
    var mapDetails;
    mapDetails='<%=request.getAttribute("mapDetails")%>'   ;
    console.log(mapDetails);
    console.log("mapDetails",JSON.parse(mapDetails));
    //console.log("mapDetails",JSON.parse((JSON.stringify(mapDetails))).size());

    var t=JSON.parse(mapDetails);

    mapboxgl.accessToken = 'pk.eyJ1Ijoic2F0aGl5YWtlbm5hZHkiLCJhIjoiY2t5emVnczFjMDg4MDJvbzB4NzQ3Z241ZCJ9.lJf5IxiZ9Sg2rXb0l9URDg';




    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [0, 0],
        zoom: 1.5,
        projection: 'naturalEarth' // starting projection
    });

    // Create a default Marker and add it to the map.

    // Create a default Marker, colored black, rotated 45 degrees.
    console.log("map",t.length);
    if(t.length>0)
    {
        for(var i=0;i<t.length;i++)
        {

            if(t[i].voted==1 ) {
                const marker1 = new mapboxgl.Marker({color: 'green', rotation: 0})
                    .setLngLat([t[i].longitude, t[i].latitude])
                    .addTo(map);
            }
            if(t[i].voted==-1)
            {
                const marker2 = new mapboxgl.Marker({color: 'red', rotation: 0})
                    .setLngLat([t[i].longitude, t[i].latitude])
                    .addTo(map);
            }
        }

    }

</script>

</body>
</html>