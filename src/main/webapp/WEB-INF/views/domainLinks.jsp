<%--
  Created by IntelliJ IDEA.
  User: venkksastry
  Date: 22/05/15
  Time: 3:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@page import="org.springframework.web.servlet.i18n.SessionLocaleResolver"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.vo.NameCountVO,com.sudasuda.app.domain.FollowDomain" %>
<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="ChudaChuda.com">
  <meta name="keywords" content="ChudaChuda.com">
  <title>சுடசுட.com - ChudaChuda.com</title>
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript">
    var timer = <%=CountdownTimerService.initialTimerSeconds%>;
    var clock;
    $(document).ready(function () {
      clock = $('.clock').FlipClock(timer, {
        countdown: true,
        clockFace: 'MinuteCounter',
        autoStart: true,
        callbacks: {
          stop: function() {
            window.location.reload();
            clock.start();
          }
        }
      });
    });
  </script>

</head>
<body>
<%String tab = (String) request.getAttribute("tab");
  if (tab==null) tab="current";
  if ( request.getSession().getAttribute("userInfo") != null ) { User user = (User) session.getAttribute("userInfo"); }%>


<div class="home-container">
  <div class="home-sidebar">
    <div class="logo"></div>
    <div class="left-arrow side-menu"></div>
    <div class="s-cat-box">
      <div class="s-topics">
        <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
          <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="new-icon"></span>
            </div>
            <h5>NEW</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=current">
          <div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="trending-icon"></span>
            </div>
            <h5>TRENDING</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
          <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="expired-icon"></span>
            </div>
            <h5>EXPIRED</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="mylinks-icon"></span>
            </div>
            <h5>Likes</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=mybookmarks">
          <div class='s-topic <%=(tab.equalsIgnoreCase("mybookmarks")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="later-filled-icon"></span>
            </div>
            <h5>Bookmark</h5>
          </div>
        </a>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="cat-icon"></span>
          <h5>CATEGORIES</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String cat = (String)session.getAttribute("category");
          cat = (cat==null?"All":cat); %>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=All"><li <%=(cat.equalsIgnoreCase("all")?"class='cat-list-active'":"")%>>All</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Health"><li <%=(cat.equalsIgnoreCase("health")?"class='cat-list-active'":"")%>>HEALTH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Society"><li <%=(cat.equalsIgnoreCase("society")?"class='cat-list-active'":"")%>>SOCIETY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=World"><li <%=(cat.equalsIgnoreCase("world")?"class='cat-list-active'":"")%>>WORLD</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Politics"><li <%=(cat.equalsIgnoreCase("politics")?"class='cat-list-active'":"")%>>POLITICS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Sports"><li <%=(cat.equalsIgnoreCase("sports")?"class='cat-list-active'":"")%>>SPORTS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Spirituality"><li <%=(cat.equalsIgnoreCase("spirituality")?"class='cat-list-active'":"")%>>SPIRITUALITY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Recipie"><li <%=(cat.equalsIgnoreCase("recipie")?"class='cat-list-active'":"")%>>RECIPIE</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Technology"><li <%=(cat.equalsIgnoreCase("technology")?"class='cat-list-active'":"")%>>TECHNOLOGY</li></a>
          </ul>
        </div>
      </div>
      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="country-icon"></span>
          <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String country=(String) session.getAttribute("country");
          if ( country == null ) country="all";%>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'><li <%=(country.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'><li <%=(country.equalsIgnoreCase("India")?"class='cat-list-active'":"")%>>INDIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'><li <%=(country.equalsIgnoreCase("United States")?"class='cat-list-active'":"")%>>USA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'><li <%=(country.equalsIgnoreCase("Australia")?"class='cat-list-active'":"")%>>AUSTRALIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'><li <%=(country.equalsIgnoreCase("United Kingdom")?"class='cat-list-active'":"")%>>UNITED KINGDOM</li></a>
          </ul>
        </div>
      </div>
      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="lang-icon"></span>
          <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String language=(String) session.getAttribute("language");
          if ( language == null ) language="All";%>

        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en"><li <%=(language.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en"><li <%=(language.equalsIgnoreCase("English")?"class='cat-list-active'":"")%>>ENGLISH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta"><li <%=(language.equalsIgnoreCase("Tamil")?"class='cat-list-active'":"")%>>தமிழ்</li></a>
            <%--<a href="#"><li>HINDI</li></a>--%>
            <%--<a href="#"><li>TELUGU</li></a>--%>
            <%--<a href="#"><li>MALAYALAM</li></a>--%>
          </ul>
        </div>

      </div>
      <div class="clock"></div>
    </div>
  </div>
  <div class="home-main" id="page-content-wrapper">
    <div class="mobile-header">
      <a href="#"><div class="mobile-menu side-menu"></div></a>
      <a href="#">
        <div class="mobile-logo"></div>
      </a>
      <div class="mobile-user">
        <div class="m-user-pic">
          <%-- <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
          --%>
        </div>
        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="m-user-name"><%=(session.getAttribute("userInfo")!=null? ((User) session.getAttribute("userInfo")).getUserName():"")%></span>
          <span class="down-icon-grey"></span>
        </a>
        <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
          <%--<a href="#"><li>Edit Profile</li></a>
          <a href="#"><li>Change Password</li></a>--%>
          <a href="<%=request.getContextPath()%>/SignOut"><li>Logout</li></a>
        </ul>
      </div>
      <div class="mobile-login hide">
        <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">SignIn</a></p>
        <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SignUp</a></p>
      </div>
    </div>
    <div class="home-header">
      <div class="input-group home-search">
        <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
					<span class="input-group-btn">
						<button class="btn home-search-btn" type="button"></button>
					</span>
      </div>

      <% if ( session.getAttribute("userInfo") == null ) {%>
      <div class="home-login">
        <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
        <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
      </div>
      <%}%>

      <% if (session.getAttribute("userInfo") != null) { %>

      <div class="home-user dropdown">
        <div class="h-user-pic">
          <img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
        </div>
        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="h-user-name"><%=(session.getAttribute("userInfo")!=null? ((User) session.getAttribute("userInfo")).getUserName():"")%></span>
          <span class="down-icon-grey"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <%-- <a href="#"><li>Edit Profile</li></a>
          <a href="#"><li>Change Password</li></a>
          --%>
          <a href="<%=request.getContextPath()%>/SignOut"><li>Logout</li></a>
        </ul>
      </div>
      <%}%>
      <div class="submit-post">
        <a href="<%=request.getContextPath()%>/AddLink"><span class="submit-text">Submit an item</span></a>
      </div>
      <div class="clearfix"></div>
    </div>


    <div class="home-content">

      <!-- Modal -->
      <div class="article-source">
        <span class="source-title">Items from source :&nbsp;</span>
        <span class="articles-domain">
          <% List<FollowDomain> domains = (List<FollowDomain>)request.getAttribute("domains"); if ( domains != null && domains.size()>0) { %>
          <select class="form-control" name="follow" style="width: 200px" onchange="javascript: changePage(this.value);">
            <%--	<option value="Please select">Please select....</option> --%>
            <%  String currentDomain = request.getParameter("domain");
              for ( int i =0; domains != null && i<domains.size(); i++) {
                FollowDomain domain = domains.get(i);
            %>
            <option value="<%=domain.getDomain()%>"  <%=(currentDomain.equalsIgnoreCase(domain.getDomain())?" selected":"")%>><%=domain.getDomain()%></option>
            <%}%>

          </select>   <%} %> <span class="domain-color"> <%=request.getParameter("domain")%> </span>
      <% User user = (User) session.getAttribute("userInfo"); String domain=request.getParameter("domain"); Boolean isFollow=(request.getAttribute("isFollowAllowed") != null? (Boolean) request.getAttribute("isFollowAllowed"):false); if (isFollow && user != null) {%>
      (<a href='<%=request.getContextPath()%>/FollowManager?op=add&domain=<%=request.getParameter("domain")%>'>Follow</a>)</span>
       <%} if (!isFollow && user != null){ %><span class="auto-style15">(<a href="<%=request.getContextPath()%>/FollowManager?op=remove&domain=<%=request.getParameter("domain")%>">Unfollow</a>)</span><%} %>
        <span class="articles-found"></span>
      </div>

      <div class="home-articles">

        <%
          List <Link> links = (List<Link>) request.getAttribute("links");
          if ( links == null || links.size() == 0) { %>
        No articles found

        <%}

          for (int i = 0; links != null && i < links.size(); i++) {

            Link link = links.get(i);
        %>
        <div class="article-box">
          <div class="articles-img">
            <img src="<%=link.getImageUrl() %>" alt="Image">
          </div>
          <div class="article-box-content">
            <a href="<%=link.getUrl()%>" target="_blank"><h5><%=link.getTitle()%></h5></a>
            <h6>By <%=link.getSubmitedBy() %> <%=( link.getHoursElapsed()<=24?link.getHoursElapsed()+" hours ago ":link.getHoursElapsed()/24+" days ago ") %></h6>
            <%
              if ( link.getTags() != null) {
                String tags[] = link.getTags().split(",");
                int j =0;
                for (String tag : tags)
                {
                  if ( j > 1) break;
                  j++;
            %>
            <a href="#"><span>#<%=tag%></span></a>
            <%}

            }else if (link.getTags() == null || link.getTags().length() > 0) {%>
            <br /> <%}%>

            <div class="article-shadow"></div>
            <div class="article-action">
              <div class="article-like">
                <%-- <span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span> --%>
                <% if ( tab != null && !tab.equalsIgnoreCase("mylinks")) {%>
                <% if ( !link.isVoted() ) {%>
                <a href="<%=request.getContextPath()%>/VoteUp?linkId=<%=link.getLinkId()%>&tab=<%=request.getAttribute("tab")%>"><span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span><h6>LIKE(<%=link.getVotes()%>)</h6></a>
                <%} else { %>
                <span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span><h6>LIKE(<%=link.getVotes()%>)</h6>
                <%} }%>

              </div>
              <div class="article-comment">
                <span class="comment-icon"></span>
                <a href="<%=request.getContextPath()%>/AddComment?linkId=<%=link.getLinkId()%>"><h6>COMMENT(<%=link.getNoOfComments()%>)</h6></a>
              </div>
              <%-- <div class="article-later">
                  <span class="later-icon"></span>
                  <a href="#"><h6>READ LATER</h6></a>
              </div> --%>
            </div>
          </div>
          <div class="box-shadow"></div>
        </div>
        <%} %>

      </div>
    </div>
  </div>
</div>
<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61533941-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
  //<![CDATA[
  var sc_project=9681219;
  var sc_invisible=1;
  var sc_security="9ea9e173";
  var scJsHost = (("https:" == document.location.protocol) ?
          "https://secure." : "http://www.");
  document.write("<sc"+"ript type='text/javascript' src='" +
          scJsHost+
          "statcounter.com/counter/counter_xhtml.js'></"+"script>");
  //]]>
</script>
<noscript><div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
                                      class="statcounter"><img class="statcounter"
                                                               src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                               alt="shopify analytics" /></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
<script type="text/javascript">
  function changePage(a)
  {
    if ( a != "Please select...." )
      location.href='<%=request.getContextPath()%>/GetLinks?domain='+a;
  }
</script>
<script type="text/javascript">
  var infolinks_pid = 2467450;
  var infolinks_wsid = 0;
</script>
<script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script>
</body>
</html>