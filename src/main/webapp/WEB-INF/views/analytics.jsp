<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page
        import="java.util.List,com.sudasuda.app.domain.User,com.sudasuda.app.vo.NameCountVO"%>
<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Chuda Chuda.com">
  <meta name="keywords" content="Chuda Chuda.com">
  <title>ChudaChuda.com</title>
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript">
    var timer = <%=CountdownTimerService.initialTimerSeconds%>;
    var clock;
    $(document).ready(function () {
      clock = $('.clock').FlipClock(timer, {
        countdown: true,
        clockFace: 'MinuteCounter',
        autoStart: true,
        callbacks: {
          stop: function() {
            window.location.reload();
            clock.start();
          }
        }
      });
    });
  </script>

  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Category', 'Submissions'],
        <%User user = (User) request.getSession().getAttribute("userInfo");
             List<NameCountVO> counts = (List<NameCountVO>) request.getAttribute("counts");
             for (int i = 0; i < counts.size(); i++) {
                 NameCountVO ccVO = counts.get(i);%>
        ['<%=ccVO.getCategory()%>',<%=ccVO.getCount()%>]<%=(i==(counts.size()-1)?"":",")%>
        <%}%>
      ]);

      var options = {
        title: "",
        is3D: true,
        backgroundColor: "<spring:message code='background.color.jumbotron' />",
        legendTextStyle: {color: '<spring:message code="googlechart.text.color" />'},
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));

      function selectHandler()
      {
        var selection = chart.getSelection()[0];

        if ( selection ) {
          var value = data.getValue(selection.row, 0);
          var loc = value.substring(0,value.indexOf("("));
          <% if ( request.getAttribute("type") != null && request.getAttribute("type").toString().equalsIgnoreCase("category")) { %>
          location.href='<%=request.getContextPath()%>/GetLinks?tab=mylinks&category='+loc;
          <%} else {%>
          location.href='<%=request.getContextPath()%>/Analytics?type=3&source='+loc;
          <%}%>
        }
      }
      google.visualization.events.addListener(chart,'select',selectHandler);
      chart.draw(data, options);

    }


  </script>

</head>
<body>
<%String tab = (String) request.getAttribute("tab");
  if (tab==null) tab="current"; %>
<div class="home-container">
  <div class="home-sidebar">
    <div class="logo"></div>
    <div class="left-arrow side-menu"></div>
    <div class="s-cat-box">
      <div class="s-topics">
        <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
          <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="new-icon"></span>
            </div>
            <h5>NEW</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=current">
          <div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="trending-icon"></span>
            </div>
            <h5>TRENDING</h5>
          </div>
        </a>
        <a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
          <div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
            <div class="s-topic-icon">
              <span class="expired-icon"></span>
            </div>
            <h5>EXPIRED</h5>
          </div>
        </a>
        <a href="#">
          <div class="s-topic">
            <div class="s-topic-icon">
              <span class="mylinks-icon"></span>
            </div>
            <h5>MY LINKS</h5>
          </div>
        </a>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="cat-icon"></span>
          <h5>CATEGORIES</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String cat = (String)request.getAttribute("category");
          cat = (cat==null?"All":cat); %>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=All"><li <%=(cat.equalsIgnoreCase("all")?"class='cat-list-active'":"")%>>All</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Health"><li <%=(cat.equalsIgnoreCase("health")?"class='cat-list-active'":"")%>>HEALTH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Society"><li <%=(cat.equalsIgnoreCase("society")?"class='cat-list-active'":"")%>>SOCIETY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=World"><li <%=(cat.equalsIgnoreCase("world")?"class='cat-list-active'":"")%>>WORLD</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Politics"><li <%=(cat.equalsIgnoreCase("politics")?"class='cat-list-active'":"")%>>POLITICS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Sports"><li <%=(cat.equalsIgnoreCase("sports")?"class='cat-list-active'":"")%>>SPORTS</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Spirituality"><li <%=(cat.equalsIgnoreCase("spirituality")?"class='cat-list-active'":"")%>>SPIRITUALITY</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Technology"><li <%=(cat.equalsIgnoreCase("technology")?"class='cat-list-active'":"")%>>TECHNOLOGY</li></a>
          </ul>
        </div>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="country-icon"></span>
          <h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String country=request.getParameter("country");
          if ( country == null ) country="all";%>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'><li <%=(country.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'><li <%=(country.equalsIgnoreCase("India")?"class='cat-list-active'":"")%>>INDIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'><li <%=(country.equalsIgnoreCase("United States")?"class='cat-list-active'":"")%>>USA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'><li <%=(country.equalsIgnoreCase("Australia")?"class='cat-list-active'":"")%>>AUSTRALIA</li></a>
            <a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'><li <%=(country.equalsIgnoreCase("United Kingdom")?"class='cat-list-active'":"")%>>UNITED KINGDOM</li></a>
          </ul>
        </div>
      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="lang-icon"></span>
          <h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <% String language=request.getParameter("ln");
          if ( language == null ) language="all";%>

        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en"><li <%=(language.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Emglish&ln=en"><li <%=(language.equalsIgnoreCase("en")?"class='cat-list-active'":"")%>>ENGLISH</li></a>
            <a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta"><li <%=(language.equalsIgnoreCase("ta")?"class='cat-list-active'":"")%>>தமிழ்</li></a>
            <%--<a href="#"><li>HINDI</li></a>--%>
            <%--<a href="#"><li>TELUGU</li></a>--%>
            <%--<a href="#"><li>MALAYALAM</li></a>--%>
          </ul>
        </div>

      </div>

      <div class="s-cat">
        <div class="s-cat-heading">
          <span class="country-icon"></span>
          <h5>ANALYTICS</h5><a href="#"><span class="down-icon"></span></a>
        </div>
        <div class="cat-items hidden">
          <ul class="cat-list">
            <a href='<%=request.getContextPath()%>/Analytics?type=1'><li class='cat-list-active'>Source</li></a>
          </ul>
        </div>
      </div>
      <div class="clock"></div>
    </div>
  </div>
</div>
<div class="home-main" id="page-content-wrapper">
  <div class="mobile-header">
    <a href="#"><div class="mobile-menu side-menu"></div></a>
    <a href="#">
      <div class="mobile-logo"></div>
    </a>
    <div class="mobile-user">
      <div class="m-user-pic">
        <img src="<%=request.getContextPath()%>/images/user-avatar.png" alt="User Picture">
      </div>
      <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="m-user-name">Hi Hari</span>
        <span class="down-icon-grey"></span>
      </a>
      <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
        <a href="#"><li>Edit Profile</li></a>
        <a href="#"><li>Change Password</li></a>
        <a href="#"><li>Logout</li></a>
      </ul>
    </div>
    <div class="mobile-login hide">
      <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
      <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
    </div>
  </div>
  <%--<div class="home-header">
      <div class="input-group home-search">
          <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
              <span class="input-group-btn">
                  <button class="btn home-search-btn" type="button"></button>
              </span>
      </div>
      <div class="home-login">
          <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
          <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
      </div>
      <div class="home-user dropdown">
          <div class="h-user-pic">
              <img src="images/user-avatar.png" alt="User Picture">
          </div>
          <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="h-user-name">Hi Venkk</span>
              <span class="down-icon-grey"></span>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
              <a href="#"><li>Edit Profile</li></a>
              <a href="#"><li>Change Password</li></a>
              <a href="#"><li>Logout</li></a>
          </ul>
      </div>
      <div class="clearfix"></div>
  </div>--%>
  <div class="submit-content">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog signup-dialog">
        <div class="modal-content signup-content">
          <div class="modal-body signup-body">

            <div class="submit-content pad0">
              <div class="submit-item">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="sign-tab">
                  <ul>
                    <li class="active" id="signin">SIGNIN</li>
                    <li id="signup">SIGNUP</li>
                  </ul>
                </div>
                <div class="sign-body">
                  <div class="login-left">
                    <form id="signin-form">
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                        <input type="email" class="form-control" placeholder="Email Id">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                        <input type="password" class="form-control" placeholder="Password">
                      </div>
                      <div class="pwd-options">
                        <div class="input-group remember-pwd">
                          <label>
                            <input type="checkbox"> Remember Password
                          </label>
                        </div>
                        <div class="input-group fgt-pwd">
                          <a href="#"><span>Forgot Password?</span></a>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="signupBtn">
                        <button class="btn btn-signup">SIGN IN</button>
                      </div>
                    </form>
                    <form id="signup-form" class="hide">
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                        <input type="email" class="form-control" placeholder="Email Id">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/fn-icon.png" alt="N">
														</span>
                        <input type="text" class="form-control" placeholder="User Name">
                      </div>
                      <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                        <input type="password" class="form-control" placeholder="Password">
                      </div>

                      <div class="signupBtn">
                        <button class="btn btn-signup">SIGN UP</button>
                      </div>
                    </form>
                  </div>
                  <div class="login-center hr-signin">
                    <hr>
                    <span>OR</span>
                  </div>
                  <div class="login-right">
                    <a href="">
                      <img src="<%=request.getContextPath()%>/resources/images/fb-btn-signin.png" alt="facebook">
                    </a>
                    <a href="">
                      <img src="<%=request.getContextPath()%>/resources/images/gp-btn-signin.png" alt="Google+">
                    </a>
                  </div>
                </div>
              </div>
              <!-- <div class="submit-item-shadow"></div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="submit-item">
      <div id="piechart_3d"
           style="width: 1000px; height: 500px;"></div>

    </div>
    <div class="clearfix"></div>
    <div class="submit-item-shadow"></div>
  </div>
</div>
</div>

<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>

<%--
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>

<script src="<%=request.getContextPath()%>/js/form-elements.js"></script>
<script src="<%=request.getContextPath()%>/js/settings.js"></script>
--%>


<script type="text/javascript">
  $(document).ready(function() {
    $("#Tags").tagEditor(
            {
              items: ["First tag", "Second tag"],
              confirmRemoval: false
            });
  });
</script>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
  //<![CDATA[
  var sc_project=9681219;
  var sc_invisible=1;
  var sc_security="9ea9e173";
  var scJsHost = (("https:" == document.location.protocol) ?
          "https://secure." : "http://www.");
  document.write("<sc"+"ript type='text/javascript' src='" +
          scJsHost+
          "statcounter.com/counter/counter_xhtml.js'></"+"script>");
  //]]>
</script>
<noscript><div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
                                      class="statcounter"><img class="statcounter"
                                                               src="http://c.statcounter.com/9681219/0/9ea9e173/1/"
                                                               alt="shopify analytics" /></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>
</html>