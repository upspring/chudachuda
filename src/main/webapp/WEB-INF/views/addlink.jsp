<%@ page import="com.sudasuda.app.service.CountdownTimerService" %>
<%@ page import="com.sudasuda.app.domain.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Chuda Chuda.com">
    <meta name="keywords" content="Chuda Chuda.com">
    <title>ChudaChuda.com</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/<%=session.getAttribute("stylefile")%>">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/flipclock.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style>
    .android-download {
        margin-left: 10px;
        margin-top: -30px;
    }
    ios-download {
        margin-left: 0px;
    }

    .android-download-img {
        width: 200px;
    }
    .google-play-download {
        display: inline-block;
        padding-left: 10px;
        color: black;
    }
    .home-login{
        display: inline-block;
        margin: 30px 9% 20px 20px;
        float: right;
    }
    .cat-list li{
        padding: 15px 0 15px 50px;
        line-height: 11px;
    }
    .s-cat-box{
        padding: 15px 0px 10px 0px;
    }
    .s-cat-box-heading{
        padding: 10px 0px 18px 10px;
    }
    .news-paper-img{
        display: inline-block;
        height: 30px;
        width: 30px;
        border-radius: 50%;
        margin-right: 10px;
    }
    .h-user-pic{
        display: inline-block;
        vertical-align: middle;
        margin-right: 18px;
        border: 2px solid #666;
        border-radius: 50%;
    }
    .h-user-pic img
    {
        border-radius: 50%;
        width: 35px;
        height: 35px;
    }
    .home-user{
        margin: 20px 30px 10px 10px;
    }
    .m-user-pic{
        border-radius: 50%;
        border: 2px solid #fff;
    }
    .mobile-user a {
        color: white;
        text-align: center;
    }
    .article-box:hover{
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        transition: 0.3s linear;
    }
    .article-box img{
        height: 170px;
    }
    .articles-img{
        margin-bottom: -10px;
    }
    .article-box-content{
        padding: 0px 20px 15px 20px;
    }
    .article-box-content .article-box-title{
        overflow: hidden;
        text-overflow: ellipsis !important;
        -webkit-line-clamp: 3;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        white-space: normal;
        height: 75px;
        margin-bottom: 10px;
    }
    .article-box-content .article-box-title h4{
        font-family: 'Noto-Sans-Tamil' !important;
        font-size: 16px;
        line-height: 22px;
    }
    .article-like svg {
        position: absolute;
        margin-top: 8px;
    }
    .article-like a{
        margin-left: 17px;
    }
    .article-comment svg{
        position: absolute;
        margin-top: 8px;
    }
    .article-comment a {
        margin-left: 17px;
    }
    .article-later svg{
        position: absolute;
        margin-top: 8px;
    }
    .article-shadow{
        margin: 10px 0px 10px 0px;
    }
    .article-box-content h6{
        margin-top: 10px;
        margin-bottom: 15px;
    }
    .article-later a {
        margin-left: 17px;
    }
    .home-content{
        margin-left: 5%;
    }
    .h-manual-login-circle{
        border-radius: 50%;
        background-color: blue;
        height: 35px;
        width: 35px;
        vertical-align: middle;
        color: white !important;
        padding: 7px 0px 0px 14px;
        font-weight: bold;
        font-size: 16px;
        margin-right: 0px;
    }


    @media(min-width: 360px) and (max-width: 600px){
        .android-download-img{
            width:100px !important;
        }
        .article-box-content{
            padding: 0px 20px 20px 20px !important;
        }
        .m-user-pic{
            display:none !important;
        }
        .home-content {
            margin-left: 0px !important;
        }
    }
    @media(min-width: 600px) and (max-width: 700px){
        .android-download-img{
            width:150px !important;
        }
        .article-box-content{
            padding: 0px 20px 20px 20px !important;
        }
        .m-user-pic{
            display:none !important;
        }
        .home-content {
            margin-left: 0px !important;
        }
    }
</style>

</head>
<body>
<%String tab = (String) request.getAttribute("tab");
    if (tab==null) tab="current"; %>
<div class="home-container">
    <div class="home-sidebar">
        <div class="logo"></div>
        <div class="left-arrow side-menu"></div>
        <div class="s-cat-box">
            <div class="s-topics">
                <a href="<%=request.getContextPath()%>/GetLinks?tab=new">
                    <div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
                        <div class="s-topic-icon">
                            <span class="new-icon"></span>
                        </div>
                        <h5>முகப்பு</h5>
                    </div>
                </a>

            </div>



            <div class="s-cat" style="margin-top: -25px;">
                <div class="s-cat-heading">
                    <span class="cat-icon"></span>
                    <h5>செய்தித்தாள்கள் </h5><a href="#"><span class="down-icon"></span></a>
                </div>
                <% String cat = (String) session.getAttribute("category");
                    System.out.println("Domain like"+cat);
                    cat = (cat == null ? "All" : cat); %>
                <div class="cat-items" style="margin-top: -20px">
                    <ul class="cat-list" style="display: block;">

                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinakaran">
                            <li <%=(cat.toLowerCase().contains("Dinakaran".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinakaran-rounded.png" alt="img-newspapers" class="news-paper-img">தினகரன்</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=hindu">
                            <li <%=(cat.toLowerCase().contains("Hindu".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/tamilthehindu.png" alt="img-newspapers" class="news-paper-img">தமிழ் தி இந்து</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=samayam">
                            <li <%=(cat.toLowerCase().contains("Samayam".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/samayam-rounded.png" alt="img-newspapers" class="news-paper-img">சமயம்</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=dinamalar">
                            <li <%=(cat.toLowerCase().contains("Dinamalar".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/dinamalar.png" alt="img-newspapers" class="news-paper-img">தினமலர்</li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=puthiyathalaimurai">
                            <li <%=(cat.toLowerCase().contains("puthiyathalaimurai".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/puthiyathalaimurai.jpg" alt="img-newspapers" class="news-paper-img">புதிய தலைமுறை </li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=bbc">
                            <li <%=(cat.toLowerCase().contains("bbc".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/bbc-tamil.jpg" alt="img-newspapers" class="news-paper-img">பிபிசி தமிழ் </li>
                        </a>
                        <a href="<%=request.getContextPath()%>/GetCategoryNewsForWeb?category=oneindia">
                            <li <%=(cat.toLowerCase().contains("oneindia".toLowerCase()) ? "class='cat-list-active'" : "")%>><img src="<%=request.getContextPath()%>/resources/images/news-papers/oneindia-tamil.png" alt="img-newspapers" class="news-paper-img">ஒன் இந்தியா தமிழ்</li>
                        </a>

                    </ul>
                </div>
            </div>
            <div class="clock"></div>
        </div>
    </div>
    </div>
    <div class="home-main" id="page-content-wrapper">
        <div class="mobile-header">
            <a href="#"><div class="mobile-menu side-menu"></div></a>
            <a href="#">
                <div class="mobile-logo"></div>
            </a>
            <div class="mobile-user">
                <div class="m-user-pic">
                    <img src="<%=request.getContextPath()%>/images/user-avatar.png" alt="User Picture">
                </div>
                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="m-user-name">Hi Hari</span>
                    <span class="down-icon-grey"></span>
                </a>
                <ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <a href="#"><li>Edit Profile</li></a>
                    <a href="#"><li>Change Password</li></a>
                    <a href="#"><li>Logout</li></a>
                </ul>
            </div>
            <div class="mobile-login hide">
                <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
            </div>
        </div>
        <div class="home-header">
            <h4 class="google-play-download">சுடசுட செயலியை பெற </h4>
            <a href="http://bit.ly/2R8xz1e" class="android-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/Google-Play-Store-Logo.png"></a>
            <a href="https://apple.co/2T5zqtC" class="ios-download" target="_blank"><img class="android-download-img" alt="chudchuda-playstore" src="<%=request.getContextPath()%>/resources/img/app-store.png"></a>

            <div class="input-group home-search">
                <input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
					<span class="input-group-btn">
						<button class="btn home-search-btn" type="button"></button>
					</span>
            </div>

            <% if (session.getAttribute("userInfo") == null) {%>
            <div class="home-login">
                <%--   <p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
                      <p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>--%>
                <p><a href='<%=request.getContextPath()+"/SignIn"%>' id="user-signin">LOGIN</a></p>

                <p><a href="<%=request.getContextPath()+"/SignIn"%>" id="user-signup">SIGNUP</a></p>
            </div>
            <%}%>

            <% if (session.getAttribute("userInfo") != null) {
                System.out.println("picture"+((User) session.getAttribute("userInfo")).getPicture());
            %>

            <div class="home-user dropdown">
                <div class="h-user-pic">
                    <% if (!((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its google"); %>
                    <img src="<%=((User) session.getAttribute("userInfo")).getPicture()%>" alt="User Picture">
                    <%
                        }
                    %>
                    <% if (((User) session.getAttribute("userInfo")).getPicture().equals("")) {System.out.println("its cc"); %>
                    <span class="h-manual-login-circle"><%=((User) session.getAttribute("userInfo")).getUserName().toUpperCase().charAt(0)%></span>
                    <%
                        }
                    %>
                </div>
                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span id="uname" class="h-user-name"><%=(session.getAttribute("userInfo") != null ? ((User) session.getAttribute("userInfo")).getUserName() : "")%></span>
                    <span class="down-icon-grey"></span>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <%-- <a href="#"><li>Edit Profile</li></a>
                      <a href="#"><li>Change Password</li></a>
                      --%>
                    <a href="<%=request.getContextPath()%>/SignOut">
                        <li>Logout</li>
                    </a>
                </ul>
            </div>


            <%}%>
            <div class="clearfix"></div>
        </div>
        <div class="submit-content">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog signup-dialog">
                    <div class="modal-content signup-content">
                        <div class="modal-body signup-body">

                            <div class="submit-content pad0">
                                <div class="submit-item">
                                    <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <div class="sign-tab">
                                        <ul>
                                            <li class="active" id="signin">SIGNIN</li>
                                            <li id="signup">SIGNUP</li>
                                        </ul>
                                    </div>
                                    <div class="sign-body">
                                        <div class="login-left">
                                            <form id="signin-form">
                                                <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                                                    <input type="email" class="form-control" placeholder="Email Id">
                                                </div>
                                                <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                                                    <input type="password" class="form-control" placeholder="Password">
                                                </div>
                                                <div class="pwd-options">
                                                    <div class="input-group remember-pwd">
                                                        <label>
                                                            <input type="checkbox"> Remember Password
                                                        </label>
                                                    </div>
                                                    <div class="input-group fgt-pwd">
                                                        <a href="#"><span>Forgot Password?</span></a>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="signupBtn">
                                                    <button class="btn btn-signup">SIGN IN</button>
                                                </div>
                                            </form>
                                            <form id="signup-form" class="hide">
                                                <div class="input-group">
														<span class="input-group-addon">
															<img src="images/mail-icon.png" alt="M">
														</span>
                                                    <input type="email" class="form-control" placeholder="Email Id">
                                                </div>
                                                <div class="input-group">
														<span class="input-group-addon">
															<img src="images/fn-icon.png" alt="N">
														</span>
                                                    <input type="text" class="form-control" placeholder="User Name">
                                                </div>
                                                <div class="input-group">
														<span class="input-group-addon">
															<img src="images/lock-icon.png" alt="M">
														</span>
                                                    <input type="password" class="form-control" placeholder="Password">
                                                </div>

                                                <div class="signupBtn">
                                                    <button class="btn btn-signup">SIGN UP</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!--    <div class="login-center hr-signin">
                                            <hr>
                                            <span>OR</span>
                                        </div>
                                          <div class="login-right">
                                            <a href="">
                                                <img src="<%=request.getContextPath()%>/resources/images/fb-btn-signin.png" alt="facebook">
                                            </a>
                                            <a href="">
                                                <img src="<%=request.getContextPath()%>/resources/images/gp-btn-signin.png" alt="Google+">
                                            </a>
                                        </div>-->
                                    </div>
                                </div>
                                <!-- <div class="submit-item-shadow"></div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="submit-item">
                <h4>Submit a new Item</h4>
                <div class="s-i-form">
                    <form action="<%=request.getContextPath()%>/AddLink" method="POST">
                        <% if (request.getAttribute("error") != null ) {%>
                        <p style="color:red"><%=request.getAttribute("error")%></p>
                        <%}%>
                        <ul class="list-unstyled">
                            <li>
                                <label>TITLE</label>
                                <input type="text" name="title" id="title"  value='<%=(request.getParameter("t") != null? request.getParameter("t"):"")%>'
                                       class="form-control submit-input" placeholder="Enter Title" required>
                            </li>
                            <li>
                                <label>URL</label>
                                <input type="url" name="url" id="url" class="form-control submit-input" placeholder="Enter URL" value='<%=(request.getParameter("u") != null? request.getParameter("u"):"")%>' required>
                            </li>
                            <li>
                                <label>Image URL</label>
                                <input type="url" name="image_url" id="image_url" class="form-control submit-input" placeholder="Enter Image URL" required>
                            </li>
                            <li>
                                <label>Description</label>
                                <textarea class="form-control submit-input" name="desc" id="desc" rows="3" placeholder="Enter Description"></textarea>
                            </li>

                            <li class="submit-form-select">
                                <label>Category</label>
                                <select class="form-control submit-input" id="category" name="category">
                                    <option value="Health">Health</option>
                                    <option value="Society">Society</option>
                                    <option value="World">World</option>
                                    <option value="Politics">Politics</option>
                                    <option value="Sports">Sports</option>
                                    <option value="Spirituality">Spirituality</option>
                                    <option value="Recipie">Recipie</option>
                                    <option value="Technology">Technology</option>
                                </select>
                            </li>

                            <li class="text-right m-text-center" style="text-align: center">
                                <button type="submit" class="btn btn-post">
                                    POST
                                </button>
                            </li>

                        </ul>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="submit-item-shadow"></div>
        </div>
    </div>
</div>

<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jquery.tag.editor.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/flipclock.js"></script>

<%--
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/vendor/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>

<script src="<%=request.getContextPath()%>/js/form-elements.js"></script>
<script src="<%=request.getContextPath()%>/js/settings.js"></script>
--%>


<script type="text/javascript">
    $(document).ready(function() {
        $("#Tags").tagEditor(
                {
                    items: ["First tag", "Second tag"],
                    confirmRemoval: false
                });
    });
</script>
</body>
</html>